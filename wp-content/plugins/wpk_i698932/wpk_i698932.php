<?php


/*

Plugin Name: WP Kraken [#i698932]

Plugin URI: https://wpkraken.io/job/i698932

Description: WP Cost Estimation Form Plugin Addon

Author: WP Kraken [Przemo & Grzesiek]

Author URI: https://wpkraken.io/

Version: 1.1

Text Domain: wpk_i698932

*/

global $WPK_i698932;
require_once 'vendor/html2pdf/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;

if ( ! function_exists( 'wp_get_current_user' ) ) {
	include( ABSPATH . "wp-includes/pluggable.php" );
}

if ( ! class_exists( 'WPK_i698932' ) ) {

	require_once 'includes/wpk_functions.php';
	require_once 'includes/class.Schedule.php';
	require_once 'includes/class.Password.php';


	class WPK_i698932 {

		public static $instance;

		public $css_folder;

		public $js_folder;

		public $mail_text;

		public $reminder_text;

		private $isValidation;

		public $schedule;

		public $password;

		public $domain = 'wpk_i698932';

		public $dir;

		public function __construct() {

			$this->dir = dirname( __FILE__ );

			$this->css_folder = plugin_dir_url( __FILE__ ) . '/assets/css/';

			$this->js_folder = plugin_dir_url( __FILE__ ) . '/assets/js/';

			$this->schedule = new WPKSchedule();

			$this->password = new WPKPassword();

			$this->mail_text = [
				'fr' => get_field( 'field_5950c0c11c06a', 'option' ),
				'en' => get_field( 'field_59748852320c9', 'option' ),
			];

			$this->reminder_text = [
				'fr' => [
					'title'   => get_field( 'field_595d29a2c83c6', 'option' ),
					'content' => get_field( 'field_595d29b1c83c7', 'option' ),
				],
				'en' => [
					'title'   => get_field( 'field_597489ad320cd', 'option' ),
					'content' => get_field( 'field_597489b8320ce', 'option' ),
				]
			];
			$this->isValidation  = false;

			add_action( 'wp_enqueue_scripts', [ $this, 'enqueue' ] );
			add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue' ] );
			/*add_action( 'init', array( $this, 'admin_generate_invoice' ), 1 );*/
			add_action( 'admin_menu', [ $this, 'acf_menu' ] );
			add_action( 'init', [ $this, 'register_invoice' ] );
			add_action( 'init', [ $this, 'create_invoice_id' ] );
			add_action( 'admin_init', [ $this, 'admin_download_pdf' ] );
			add_action( 'add_meta_boxes', [ $this, 'invoice_metabox' ] );
			add_action( 'init', [ $this, 'remove_schedule' ] );
			add_action( 'admin_menu', [ $this, 'schedules_menu' ] );
			add_action( 'save_post', [ $this, 'update_invoice_meta' ] );
			add_action( 'wpk_schedule_hook', [ $this, 'proffesional_schedule' ] );
			add_action( 'wp_footer', [ $this, 'showPopup' ] );
			add_action( 'delete_post', [ $this, 'delete_schedules' ] );

			$this->set_ajax_events();


		}

		public function delete_schedules( $pid ) {

			if ( get_post_type( $pid ) !== 'wpk_invoice' ) {
				return;
			}

			$email = get_post_meta( $pid, 'Email', true );
			$date  = $this->get_meta( $pid, 'Date' );
			$date  = self::translate_months( $date );
			$date  = new DateTime( $date );

			$schedules = $this->schedule->schedules;

			foreach ( $schedules[ 'before_event' ] as $day => $events ) {

				foreach ( $events as $reference => $arr ) {

					$invoice_id = $arr[ 'invoice_id' ];

					if ( $invoice_id == $pid ) {
						unset( $schedules[ 'before_event' ][ $day ][ $reference ] );
					}

				}
			}

			$was_activated = false;
			foreach ( $schedules[ 'after_event' ] as $day => $events ) {

				foreach ( $events as $month_day => $data ) {

					foreach ( $data as $ref => $array ) {

						$invoice_id  = $array[ 'invoice_id' ];
						$event_email = $array[ 'mail' ];
						if ( $event_email === $email && ! $was_activated ) {
							$event_date = get_post_meta( $id, 'Date', true );
							$event_date = self::translate_months( $event_date );
							$event_dt   = new DateTime( $event_date );
							if ( $event_dt->getTimestamp() === $date->getTimestamp() && $pid != $invoice_id ) {
								$was_activated                                                        = true;
								$schedules[ 'after_event' ][ $day ][ $month_day ][ $ref ][ 'status' ] = 'active';
							}
						}
						if ( $invoice_id == $pid ) {
							unset( $schedules[ 'after_event' ][ $day ][ $month_day ][ $ref ] );
						}

					}

				}

			}

			foreach ( $schedules[ 'proffesional' ] as $day => $_schedules ) {

				foreach ( $_schedules as $_ref => $schedule ) {

					$invoice_id = $schedule[ 'invoice_id' ];
					if ( $invoice_id == $pid ) {
						unset( $schedules[ 'proffesional' ][ $day ][ $_ref ] );
					}


				}


			}

			update_option( 'wpk_schedules', $schedules );

		}


		public function set_ajax_events() {

			add_action( 'wp_ajax_wpk_invoice', [ $this, 'get_form_values' ] );
			add_action( 'wp_ajax_nopriv_wpk_invoice', [ $this, 'get_form_values' ] );
			add_action( 'wp_ajax_wpk_send', [ $this, 'generate_invoice_for_email' ] );
			add_action( 'wp_ajax_nopriv_wpk_send', [ $this, 'generate_invoice_for_email' ] );
			add_action( 'wp_ajax_wpk_send_invoice', [ $this, 'send_mail_ajax' ] );
			add_action( 'wp_ajax_wpk_paid_status', [ $this, 'change_paid_status' ] );
			add_action( 'wp_ajax_wpk_cash', [ $this, 'change_cash' ] );
			add_action( 'wp_ajax_wpk_send_reminder', [ $this, 'send_reminder' ] );
			add_action( 'wp_ajax_wpk_validate_reference', [ $this, 'check_ref' ] );
			add_action( 'wp_ajax_nopriv_wpk_validate_reference', [ $this, 'check_ref' ] );

		}


		public function enqueue() {

			$css = $this->css_folder;
			$js  = $this->js_folder;

			wp_enqueue_script( 'wpk_i698932_js', $js . 'wpk_i698932.js', [ 'jquery' ], '1.0' );
			wp_localize_script( 'wpk_i698932_js', 'wpk_i698932', [

				'ajax_url' => admin_url( 'admin-ajax.php' ),

				'confirmation_forms' => explode( ',', get_field( 'field_5950af466f7d6', 'option' ) ),

				'english_forms' => explode( ',', get_field( 'field_597487d796b08', 'option' ) ),

				'customization_forms' => explode( ',', get_field( 'field_5979aff6fbe75', 'option' ) ),

				'debug' => get_field( 'field_5950af466f7d6', 'option' ),

			] );

			wp_enqueue_style( 'wpk_i698932_css', $css . 'wpk_i698932.css', [], '1.0' );

		}

		public function confirm_quote( $data ) {


			$values = [];


			foreach ( $data[ 'items' ] as $item ) {

				$label = $item[ 'label' ];

				$form_val = $item[ 'value' ];

				$label = str_replace( '(où se déroule l\'évenement)', '', $label );

				$label = str_replace( [ '(renseigne lors du devis)', '/', '\'' ], [ '' ], $label );


				$label = str_replace( ' ', '_', $label );

				$label = str_replace( 'é', 'e', $label );

				$label = str_replace( 'ê', 'e', $label );

				$label = str_replace( ',', '', $label );

				$label = str_replace( '\'', '', $label );

				$label = str_replace( '’', '', $label );

				$values[ $label ] = $form_val;

			}


			//$values = $this->get_input_labels( $_POST[ 'values' ] );

			if ( isset( $values[ 'Reference_devis' ] ) ) {

				$ref = $values[ 'Reference_devis' ];

			} else {

				$ref = $values[ 'Quote_reference' ];

			}


			if ( isset( $values[ 'Email_(renseigne_lors_du_devis)' ] ) ) {

				$email = $values[ 'Email_(renseigne_lors_du_devis)' ];

			} else if ( isset( $values[ 'Email_(same_as_in_the_quote)' ] ) ) {

				$email = $values[ 'Email_(same_as_in_the_quote)' ];


			} else if ( isset( $values[ 'Email' ] ) ) {

				$email = $values[ 'Email' ];

			} else {

				$email = $values[ '_email' ];

			}


			if ( isset( $values[ 'Societe__Nom' ] ) ) {

				$societe = $values[ 'Societe__Nom' ];

			} else if ( isset( $values[ 'Nom__Societe' ] ) ) {

				$societe = $values[ 'Nom__Societe' ];

			} else {

				$societe = $values[ 'Company__Name' ];

			}


			if ( isset( $values[ 'Adresse_du_lieu_de_levent_si_non_renseigne_dans_le_devis' ] ) ) {

				$address = $values[ 'Adresse_du_lieu_de_levent_si_non_renseigne_dans_le_devis' ];

			} else {

				$address = $values[ 'Event_site_address_if_not_confirmed_on_the_quote' ];

			}


			if ( isset( $values[ 'En-tete_de_facturation' ] ) ) {

				$invoice_details = $values[ 'En-tete_de_facturation' ];

			} else {

				$invoice_details = $values[ 'Invoice_Header' ];

			}


			if ( isset( $values[ 'Numero_de_natel_du_contact_sur_place' ] ) ) {

				$phone_number = $values[ 'Numero_de_natel_du_contact_sur_place' ];

			} else if ( isset( $values[ 'Cellphone_number_of_the_contact_person_onsite' ] ) ) {

				$phone_number = $values[ 'Cellphone_number_of_the_contact_person_onsite' ];

			} else {

				$phone_number = $values[ '_phone' ];

			}


			$args = [

				'post_type' => 'wpk_invoice',

				'meta_query' => [

					[

						'key' => 'ref',

						'value' => $ref,

						'compare' => '=',

					],

				]

			];

			$query = new WP_Query( $args );

			if ( $query->post_count === 0 ) {

				$args = [

					'post_type' => 'wpk_invoice',

					'meta_query' => [

						[

							'key' => 'ref',

							'value' => ucfirst( $ref ),

							'compare' => '=',

						],

					]

				];

				$query = new WP_Query( $args );

			}

			if ( $query->post_count > 0 ) {

				$post = array_shift( $query->get_posts() );

				$id = $post->ID;

				$proffesional = empty( $this->get_meta( $id, 'Professionnel_de_levenementiel' ) ) ? $this->get_meta( $id, 'Professionnel_de_levènementiel' ) : $this->get_meta( $id, 'Professionnel_de_levenementiel' );

				$event_type = $this->get_meta( $id, 'Type_devenement' );
				$date       = $this->get_meta( $id, 'Date' );
				$mail       = $this->get_meta( $id, 'Email' );


				if ( $this->get_meta( $id, 'Email' ) != $email ) {

					return;

				}

				update_post_meta( $id, 'quote_status', 'Confirmed' );

				if ( ! empty( $societe ) ) {

					update_post_meta( $id, 'invoice_societe', $societe );

				}

				if ( ! empty( $address ) ) {

					update_post_meta( $id, 'Adresse', $address );

				}

				if ( ! empty( $phone_number ) ) {

					update_post_meta( $id, 'Telephone', $phone_number );

				}

				if ( ! empty( $invoice_details ) ) {

					update_post_meta( $id, 'invoice_address', $invoice_details );

				}
				$societe = $this->get_meta( $id, 'Societe' );
				if ( strtolower( $proffesional ) === 'oui' || ! empty( $societe ) ) {

					$this->schedule->add_schedule( new DateTime(), $email, $ref, $post->ID, 'proffesional' );

				}

				if ( $event_type !== 'Mariage' ) {

					$this->schedule->add_schedule( new DateTime(), $email, $ref, $post->ID, 'after_event' );

				}


				foreach ( $this->schedule->schedules[ 'before_event' ] as $day => $events ) {

					foreach ( $events as $reference => $array ) {

						if ( $reference === $ref ) {

							unset( $this->schedule->schedules[ 'before_event' ][ $day ][ $ref ] );

							update_option( 'wpk_schedules', $this->schedule->schedules );

						}

					}

				}
				$this->remove_identical_schedules( $id );
				$this->send_quote_to_workers( $id );


			}


		}


		public function add_new_invoice( $data ) {

			global $wpdb;

			$values      = [];
			$form_id     = $data[ 'formID' ];
			$form_values = $data[ 'items' ];
			$price       = $data[ 'total' ];
			$price       = str_replace( '.', ',', $price );

			$values[ 'price' ]   = $price;
			$english_forms       = explode( ',', get_field( 'field_597487d796b08', 'option' ) );
			$lang                = in_array( $form_id, $english_forms ) ? 'en' : 'fr';
			$values[ 'lang' ]    = $lang;
			$confirmation_forms  = explode( ',', get_field( 'field_5950af466f7d6', 'option' ) );
			$customization_forms = explode( ',', get_field( 'field_5979aff6fbe75', 'option' ) );

			if ( in_array( $form_id, $customization_forms ) ) {
				return;

			} else if ( in_array( $form_id, $confirmation_forms ) ) {
				$this->confirm_quote( $data );
			} else {
				foreach ( $form_values as $value ) {
					$label    = $value[ 'label' ];
					$form_val = $value[ 'value' ];
					$label    = str_replace( '(où se déroule l\'évenement)', '', $label );
					$label    = str_replace( [ '(renseigne lors du devis)', '/', '\'' ], [ '' ], $label );
					$label    = str_replace( ' ', '_', $label );
					$label    = str_replace( 'é', 'e', $label );
					$label    = str_replace( 'ê', 'e', $label );
					$label    = str_replace( ',', '', $label );
					$label    = str_replace( '\'', '', $label );
					$label    = str_replace( '’', '', $label );

					$values[ $label ] = $form_val;


				}
				$table_name                 = $wpdb->prefix . "wpefc_forms";
				$rows                       = $wpdb->get_results( "SELECT * FROM $table_name WHERE id=$form_id LIMIT 1" );
				$form                       = $rows[ 0 ];
				$ref                        = $form->current_ref;
				$ref_name                   = $form->ref_root;
				$date                       = new DateTime();
				$vat                        = get_field( 'field_5959ea8c1d337', 'options' );
				$values[ 'ref' ]            = $ref_name . $ref;
				$values[ 'ref_name' ]       = $ref_name;
				$values[ 'vat' ]            = $vat;
				$values[ 'vat_number' ]     = get_field( 'field_5959f2983df2d', 'options' );
				$values[ 'quote_status' ]   = 'Not confirmed';
				$values[ 'paid' ]           = 'Not paid';
				$values[ 'invoice_status' ] = 'Not sent';
				$price                      = $this->calc_vat( $values[ 'price' ], $vat, true );
				$values[ 'total_price' ]    = $price[ 'total' ];
				$values[ 'vat_price' ]      = $price[ 'vat' ];
				$values[ 'gross_price' ]    = $price[ 'price' ];
				$id                         = $this->add_invoice( $ref_name . $ref );
				$values                     = $this->translate_label( $values );
				//if ( $values[ 'lang' ] === 'en' ) {
				//}
				if ( $id ) {
					$this->insert_invoice_meta( $id, $values );
					update_post_meta( $id, 'wpk_new', '1' );
					$this->schedule->add_schedule( $date, $values[ 'Email' ], $ref_name . $ref, $id );

				}

			}

			/**/


		}


		public function schedules_menu() {

			add_submenu_page( null, 'WPK Invoice download', 'WPK Invoice download', 'manage_options', 'wpk_download_invoice', [
				$this,
				'admin_download_pdf'
			] );

			add_menu_page( 'Invoices WPK', 'Invoices WPK', 'manage_options', 'wpk_schedules', [
				$this,
				'display_invoices'
			], '', '5' );

			$before_event = add_submenu_page( 'wpk_schedules', 'Before event follow-ups', 'Before event follow-ups', 'manage_options', 'before_event', [
				$this,
				'schedule_menu'

			] );

			$after_event = add_submenu_page( 'wpk_schedules', 'After event follow-ups', 'After event follow-ups', 'manage_options', 'after_event', [
				$this,
				'schedule_menu'

			] );

			$proffesional = add_submenu_page( 'wpk_schedules', 'Invoice schedules', 'Invoice schedules', 'manage_options', 'proffesional', [
				$this,
				'schedule_menu'
			] );

			add_submenu_page( 'wpk_schedules', 'All events', 'All events', 'manage_options', 'all_events', [
				$this,
				'schedule_menu'
			] );

			add_submenu_page( 'wpk_schedules', 'Delete invoices by date', 'Delete invoices by date', 'manage_options', 'wpk_delete_invoices_by_date', [
				$this,
				'delete_invoices_by_date'
			] );


		}

		public function delete_invoices_by_date() {

			$option  = get_option( 'wpk_invoice_references' );
			$counter = 0;
			//20180107
			if ( ! empty( $_POST[ 'date' ] ) ) {
				$date = $_POST[ 'date' ];
				if ( isset( $option[ $date ] ) ) {
					$args  = [
						'post_type'  => 'wpk_invoice',
						'meta_query' => [
							[
								'key'     => 'invoice_id',
								'value'   => $date,
								'compare' => 'LIKE'
							]
						]
					];
					$query = new WP_Query( $args );

					if ( $query->have_posts() ) {

						foreach ( $query->get_posts() as $post ) {
							$counter ++;
							update_post_meta( $post->ID, 'invoice_id', '' );
						}
						unset( $option[ $date ] );
						update_option( 'wpk_invoice_references', $option );
					}
				}
			}
			if ( WPKPassword::isValidationRequired() ) {
				?>
				<form action="#" method="POST" class="wpk-password-form">
					<div class="wpk-input">
						<input type="password" name="wpk_password" placeholder="Enter password to proceed.">
					</div>
					<div class="wpk-input">
						<button class="button button-primary" type="submit">Submit</button>
					</div>
				</form>
				<?php
			} else {
				if ( $counter > 0 ):
					?>
					<div class="wpk-success">
						<span><?php printf( __( 'Removed %s invoices.', $this->domain ), $counter ) ?></span>
					</div>
				<?php endif; ?>
				<div class="wpk-date-form-wrap">
					<form action="" class="wpk-date-form" method="post">
						<input type="number" placeholder="Date in format YYYYMMDD" name="date">
						<button class="button button-primary" type="submit">Reset counter and delete invoices generated on this day</button>
					</form>
				</div>

				<?php
			}
		}


		public function admin_download_pdf() {

			if ( isset( $_GET[ 'wpk_invoice' ] ) && ! empty( $_GET[ 'wpk_invoice' ] ) ) {
				$ref  = $this->get_meta( $_GET[ 'wpk_invoice' ], 'ref' );
				$args = $this->generate_invoice_html( $_GET[ 'wpk_invoice' ] );
				$this->generate_invoice_pdf( $args, 'I', "$ref.pdf" );
				die();
			}

		}


		public function send_reminder() {

			$id      = $_POST[ 'id' ];
			$lang    = $this->get_meta( $id, 'lang' );
			$title   = $this->reminder_text[ $lang ][ 'title' ];
			$content = $this->reminder_text[ $lang ][ 'content' ];


			if ( ! empty( $title ) && ! empty( $content ) ) {
				$title   = $this->format_variables( $title, $id );
				$content = $this->format_variables( $content, $id );
				$mail    = $this->get_meta( $id, 'Email' );
				$args    = $this->generate_invoice_html( $id, false );

				$invoice_ref = $this->get_meta( $id, 'invoice_id' );

				$invoice = $this->generate_invoice_pdf( $args, 'F', $invoice_ref . '.pdf' );
				$counter = empty( $this->get_meta( $id, 'reminder_count' ) ) ? 0 : intval( $this->get_meta( $id, 'reminder_count' ) );
				$counter = $counter + 1;
				update_post_meta( $id, 'reminder_count', $counter );
				$result      = wp_mail( $mail, $title, $content, $this->schedule->prepare_email_headers(), [ $invoice[ 'path' ] ] );
				$admin_email = get_field( 'field_599c6deb2bec3', 'option' );
				if ( ! empty( $admin_email ) ) {
					wp_mail( $admin_email, $title, $content, $this->schedule->prepare_email_headers(), [ $invoice[ 'path' ] ] );
				}
				if ( $result ) {
					if ( $result ) {

						update_post_meta( $id, 'invoice_status', 'Sent' );

						wp_update_post( [

							'ID' => $id,

							'post_status' => 'publish'

						] );

					}

					unlink( $this->dir . '/pdf/' . $invoice_ref . '.pdf' );

					$proffesional = empty( $this->get_meta( $id, 'Professionnel_de_levenementiel' ) ) ? $this->get_meta( $id, 'Professionnel_de_levènementiel' ) : $this->get_meta( $id, 'Professionnel_de_levènementiel' );
					if ( strtolower( $proffesional ) === 'oui' ) {

						foreach ( $this->schedule->schedules[ 'proffesional' ] as $day => $schedules ) {

							foreach ( $schedules as $_ref => $schedule ) {

								if ( $schedule[ 'invoice_id' ] == $id ) {

									unset( $this->schedule->schedules[ 'proffesional' ][ $day ][ $_ref ] );

									update_option( 'wpk_schedules', $this->schedule->schedules );

								}

							}

						}

					}

					exit( json_encode( [
						'alert'   => 'Sent',
						'message' => sprintf( __( 'Envoyer un rappel (%s)', $this->domain ), $counter )
					] ) );
				}
			}
			exit( json_encode( 'Error' ) );

		}


		public function proffesional_schedule() {

			$events = $this->schedule->get_today_events();
			$date   = $this->schedule->date;
			$day    = $date[ 'day' ];
			foreach ( $events[ 'proffesional' ] as $ref => $event ) {

				$invoice_id = $event[ 'invoice_id' ];
				$status     = $this->get_meta( $invoice_id, 'invoice_status' );
				//invoice_status Sent

				if ( $status !== 'Sent' ) {
					if ( $event[ 'status' ] === 'active' && get_post_status( $invoice_id ) ) {
						$mail = $this->send_mail_invoice( $invoice_id, $event[ 'mail' ], $ref );
						if ( $mail ) {
							unset( $this->schedule->schedules[ 'proffesional' ][ $day ][ $ref ] );
							update_option( 'wpk_schedules', $this->schedule->schedules );
						}
					}
				} else {
					unset( $this->schedule->schedules[ 'proffesional' ][ $day ][ $ref ] );
					update_option( 'wpk_schedules', $this->schedule->schedules );
				}
			}
		}


		public function change_cash() {

			$id         = $_POST[ 'id' ];
			$status     = $this->get_meta( $id, 'cash_status' );
			$new_status = empty( $status ) || $status === '---' ? 'Cash' : '---';
			update_post_meta( $id, 'cash_status', $new_status );
			exit( json_encode( $new_status ) );

		}


		public function change_paid_status() {

			$id         = $_POST[ 'id' ];
			$status     = $this->get_meta( $id, 'paid' );
			$new_status = empty( $status ) || $status === 'Not paid' ? 'Paid' : 'Not paid';
			update_post_meta( $id, 'paid', $new_status );
			$result = $new_status === 'Paid' ? 'Paye' : 'En attente';
			exit( json_encode( $result ) );

		}


		public function get_form_result_table( $ref ) {

			global $wpdb;
			$table = $wpdb->prefix . 'wpefc_logs';
			$res   = $wpdb->get_results( "SELECT * FROM $table WHERE ref='$ref'" );

			return $res;

		}

		public static function translate_date( $date ) {

			$months = [
				'janvier'   => "January",    //January
				'février'   => "February", //February
				'mars'      => "March",      //March
				'avril'     => "April", //April
				'mai'       => "May", //May
				'juin'      => "Jun",         //Jun
				'juillet'   => "July",      //July
				'août'      => "August", //August
				'septembre' => "September",       //September
				'octobre'   => "October",    //October
				'novembre'  => "November", //November
				'décembre'  => "December"

			];

			foreach ( $months as $french_month => $month ) {
				$date = str_replace( $french_month, $month, $date );
			}

			return $date;

		}


		public function display_invoices() {

			if ( WPKPassword::isValidationRequired() ) {
				?>
				<form action="#" method="POST" class="wpk-password-form">
					<div class="wpk-input">
						<input type="password" name="wpk_password" placeholder="Enter password to proceed.">
					</div>
					<div class="wpk-input">
						<button class="button button-primary" type="submit">Submit</button>
					</div>
				</form>
				<?php
			} else {
				if ( isset( $_POST[ 'send' ] ) && ! empty( $_POST[ 'invoice_id' ] ) ) {
					foreach ( $_POST[ 'invoice_id' ] as $id ) {
						$mail = $this->get_meta( $id, 'Email' );
						$ref  = $this->get_meta( $id, 'ref' );
						$this->send_mail_invoice( $id, $mail, $ref );
					}

				} else if ( isset( $_POST[ 'mark' ] ) && ! empty( $_POST[ 'invoice_id' ] ) ) {
					foreach ( $_POST[ 'invoice_id' ] as $id ) {
						$proffesional = empty( $this->get_meta( $id, 'Professionnel_de_levenementiel' ) ) ? $this->get_meta( $id, 'Professionnel_de_levènementiel' ) : $this->get_meta( $id, 'Professionnel_de_levènementiel' );
						$societe      = $this->get_meta( $id, 'Societe' );
						$event_type   = $this->get_meta( $id, 'Type_devenement' );
						$email        = $this->get_meta( $id, 'Email' );
						$ref          = $this->get_meta( $id, 'ref' );
						update_post_meta( $id, 'quote_status', 'Confirmed' );
						if ( strtolower( $proffesional ) === 'oui' || ! empty( $societe ) ) {
							$this->schedule->add_schedule( new DateTime(), $email, $ref, $id, 'proffesional' );
						}

						if ( $event_type !== 'Mariage' ) {
							$this->schedule->add_schedule( new DateTime(), $email, $ref, $id, 'after_event' );
						}

						foreach ( $this->schedule->schedules[ 'before_event' ] as $day => $events ) {
							foreach ( $events as $reference => $array ) {
								if ( $reference === $ref ) {
									unset( $this->schedule->schedules[ 'before_event' ][ $day ][ $ref ] );
									update_option( 'wpk_schedules', $this->schedule->schedules );
								}
							}
						}
					}
				} else if ( isset( $_POST[ 'empty_trash' ] ) && ! empty( $_POST[ 'invoice_id' ] ) ) {
					foreach ( $_POST[ 'invoice_id' ] as $id ) {
						wp_trash_post( $id );
					}

				} else if ( isset( $_POST[ 'standby' ] ) && ! empty( $_POST[ 'invoice_id' ] ) ) {
					foreach ( $_POST[ 'invoice_id' ] as $id ) {

						foreach ( $this->schedule->schedules[ 'before_event' ] as $day => $events ) {
							foreach ( $events as $ref => $array ) {
								if ( $array[ 'invoice_id' ] == $id ) {
									$id = $array[ 'invoice_id' ];

									update_post_meta( $array[ 'invoice_id' ], 'invoice_status', 'refused' );
									$this->schedule->schedules[ 'before_event' ][ $day ][ $ref ][ 'status' ] = 'disabled';
									update_option( 'wpk_schedules', $this->schedule->schedules );

								}

							}

						}

						update_post_meta( $id, 'invoice_status', 'refused' );
					}
				}
				$query = new WP_Query( [
					'post_type'      => 'wpk_invoice',
					'post_status'    => 'any',
					'posts_per_page' => - 1,
				] );


				$months = [
					'janvier'   => "January",    //January
					'février'   => "February", //February
					'mars'      => "March",      //March
					'avril'     => "April", //April
					'mai'       => "May", //May
					'juin'      => "Jun",         //Jun
					'juillet'   => "July",      //July
					'août'      => "August", //August
					'septembre' => "September",       //September
					'octobre'   => "October",    //October
					'novembre'  => "November", //November
					'décembre'  => "December"

				];      //December

				if ( $query->get_posts() ) {
					?>
					<form method="post">
						<table class="wpk-table wpk-invoice-table">
							<thead>
							<tr>
								<th>Select</th>
								<th># Devis</th>
								<th>Date Evenement</th>
								<th>Entreprise</th>
								<th>Nom</th>
								<th>Montant HT</th>
								<th>TVA</th>
								<th>Montant TTC</th>
								<th>Statut Devis</th>
								<th>Statut Facture</th>
								<th># Facture</th>
								<th>Statut Paiement</th>
								<th>Type Paiement</th>
								<th>Modifier</th>
								<th>Rappel Paiement</th>
							</tr>
							</thead>
							<tbody>
							<?php
							foreach ( $query->get_posts() as $post ) : ?>
								<?php
								$id   = $post->ID;
								$date = $this->get_meta( $id, 'Date' );
								foreach ( $months as $french_month => $month ) {
									$date = str_replace( $french_month, $month, $date );
								}
								$invoice_number = $this->get_meta( $id, 'invoice_id' );
								if ( empty( $invoice_number ) ) {
									$invoice_number = 'Non envoyé';

								}
								$cash           = $this->get_meta( $id, 'cash_status' ) === 'Cash' ? 'Cash' : '---';
								$name           = $this->get_meta( $id, 'Societe' );
								$nom            = $this->get_meta( $id, 'Nom' );
								$status         = $this->get_meta( $id, 'quote_status' );
								$status         = $status === 'Confirmed' ? 'Confirmé' : 'En attente';
								$invoice_status = $this->get_meta( $id, 'invoice_status' );
								if ( $invoice_status === 'refused' ) {
									$status = 'Stand by';
								}
								switch ( $invoice_status ) {
									case 'Sent' :
										$invoice_status = '<a target="_blank" href="' . get_home_url() . '/wp-admin/post.php?page=wpk_download_invoice&wpk_invoice=' . $id . '">Voir la facture</a>';
										break;
									default:
										$invoice_status = 'En attente';
								}

								//$invoice_status = $invoice_status === 'Sent' ? '<a target="_blank" href="' . get_home_url() . '/wp-admin/post.php?page=wpk_download_invoice&wpk_invoice=' . $id . '">Voir la facture</a>' : 'En attente';
								$paid           = $this->get_meta( $id, 'paid' );
								$paid           = strtolower( $paid ) === 'paid' ? 'Paye' : 'En attente';
								$ref            = $this->get_meta( $id, 'ref' );
								$gross_price    = $this->get_meta( $id, 'gross_price' );
								$vat_price      = $this->get_meta( $id, 'vat_price' );
								$total_price    = $this->get_meta( $id, 'total_price' );
								$reminder_count = empty( $this->get_meta( $id, 'reminder_count' ) ) ? 0 : $this->get_meta( $id, 'reminder_count' );
								?>
								<tr>
									<td><input type="checkbox" name="invoice_id[]" value="<?php echo $id ?>"></td>
									<td><?php echo $ref ?></td>
									<td class="event-date"><?php echo date( 'd/m/Y', strtotime( $date ) ) ?></td>
									<td><?php echo $name ?></td>
									<td><?php echo $nom ?></td>
									<td class="wpk-price gross-price"><?php echo $gross_price ?></td>
									<td class="wpk-price vat"><?php echo $vat_price ?></td>
									<td class="wpk-price total-price"><?php echo $total_price ?></td>
									<td><?php echo $status ?></td>
									<td><?php echo $invoice_status ?></td>
									<td>
										<?php echo $invoice_number ?>
									</td>
									<td>
										<button title="Click here to change payment status" class="wpk-paid button button-primary" data-id="<?php echo $id ?>"><?php echo $paid ?></button>
									</td>
									<td>
										<button class="wpk-cash button button-primary" data-id="<?php echo $id ?>"><?php echo $cash ?></button>
									</td>
									<td>
										<a target="_blank" class="wpk-edit-invoice button button-primary" href="<?php echo get_site_url() . '/wp-admin/post.php?post=' . $id . '&action=edit' ?>">
											Modifier
										</a>
									</td>
									<td>
										<button class="button button-primary wpk-send-reminder" data-id="<?php echo $id ?>"><?php printf( __( 'Envoyer un rappel (%s)', $this->domain ), $reminder_count ) ?> </button>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
							<tfoot>
							<tr>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
							</tr>
							</tfoot>
						</table>
						<button type="submit" name="send" class="button button-primary"><?php _e( 'Facturer', $this->domain ) ?></button> &nbsp;
						<button type="submit" name="mark" class="button button-primary"><?php _e( 'Confirmer les devis', $this->domain ) ?></button> &nbsp;
						<button type="submit" name="empty_trash" class="button button-primary"><?php _e( 'Supprimer', $this->domain ) ?></button> &nbsp;
						<button type="submit" name="standby" class="button button-primary"><?php _e( 'Mark as stand by', $this->domain ) ?></button> &nbsp;
					</form>
					<div class="wpk-sum">
						<span class="gross-sum"></span>
						<br>
						<span class="vat-sum"></span>
						<br>
						<span class="total-sum"></span>
					</div>
					<?php
				}
			}
		}


		public function remove_schedules( $POST, $type ) {

			if ( $type === 'after_event' ) {
				foreach ( $POST as $event_type => $data ) {
					if ( ! empty( $data ) && is_array( $data ) ) {
						foreach ( $data as $month => $array ) {
							foreach ( $array as $day => $refs ) {
								foreach ( $refs as $ref => $checked ) {
									if ( isset( $POST[ 'disable' ] ) ) {
										$this->schedule->schedules[ $event_type ][ $month ][ $day ][ $ref ][ 'status' ] = 'disabled';

									} else if ( isset( $POST[ 'remove' ] ) ) {
										unset( $this->schedule->schedules[ $event_type ][ $month ][ $day ][ $ref ] );

									} else if ( isset( $POST[ 'activate' ] ) ) {
										$this->schedule->schedules[ $event_type ][ $month ][ $day ][ $ref ][ 'status' ] = 'active';
									}
								}
							}
							update_option( 'wpk_schedules', $this->schedule->schedules );
						}
					}
				}
			} else {
				foreach ( $POST as $event_type => $data ) {
					if ( ! empty( $data ) && is_array( $data ) ) {
						foreach ( $data as $day => $array ) {
							foreach ( $array as $ref => $checked ) {
								if ( isset( $_POST[ 'disable' ] ) ) {
									$this->schedule->schedules[ $event_type ][ $day ][ $ref ][ 'status' ] = 'disabled';
								} else if ( isset( $_POST[ 'remove' ] ) ) {
									unset( $this->schedule->schedules[ $event_type ][ $day ][ $ref ] );

								} else if ( isset( $_POST[ 'activate' ] ) ) {
									$this->schedule->schedules[ $event_type ][ $day ][ $ref ][ 'status' ] = 'active';
								}
							}
							update_option( 'wpk_schedules', $this->schedule->schedules );
						}

					}

				}

			}

		}

		/**
		 * Translates month from french to english
		 *
		 * @param string $date
		 *
		 * @return mixed
		 */
		public static function translate_months( $date ) {

			$months = [
				'janvier'   => "January",    //January
				'février'   => "February", //February
				'mars'      => "March",      //March
				'avril'     => "April", //April
				'mai'       => "May", //May
				'juin'      => "June",         //Jun
				'juillet'   => "July",      //July
				'août'      => "August", //August
				'septembre' => "September",       //September
				'octobre'   => "October",    //October
				'novembre'  => "November", //November
				'décembre'  => "December"
			];      //December
			foreach ( $months as $french_month => $month ) {
				$date = str_replace( $french_month, $month, $date );
			}

			return $date;

		}

		public function display_all_events() {

			/*	$schedules = $this->schedule->schedules;
				$days      = array(

					'Sunday',

					'Monday',

					'Tuesday',

					'Wednesday',

					'Thursday',

					'Friday',

					'Saturday',

				);

				$months = array(

					"January",

					"February",

					"March",

					"April",

					"May",

					"June",

					"July",

					"August",

					"September",

					"October",

					"November",

					"December"

				);
				*/ ?>
				<!--
			<form method="post">
				<div class="wpk-input-container">
					<label for="show_all"><?php /*_e( 'Show all events', $this->domain ) */ ?></label>
					<input type="checkbox" name="show_all" id="show_all" class="wpk-checkbox">
				</div>
				<table class="wpk-table wpk-all-events-table">
					<thead>
					<tr>
						<th>Select</th>
						<th>Reference</th>
						<th>Email</th>
						<th>Date</th>
						<th>Status</th>
						<th>Event type</th>
						<th>Event date</th>
						<th>Event date (timestamp)</th>
					</tr>
					</thead>
					<tbody>
					<?php /*foreach ( $schedules as $type => $schedule ) : */ ?>
						<?php
			/*						$day_type  = $type === 'after_event' ? 'Month' : 'Day';
									$time_type = $type === 'after_event' ? $months : $days;
									*/ ?>
						<?php /*if ( $type === 'after_event' ) : */ ?>
							<?php /*foreach ( $schedule as $month => $data ) : */ ?>
								<?php /*foreach ( $data as $event_day => $events ) : */ ?>
									<?php /*foreach ( $events as $ref => $array ) : */ ?>
										<?php
			/*										$id   = $array[ 'invoice_id' ];
													$date = $this->get_meta( $id, 'Date' );
													$date = self::translate_months( $date );
													$mail = get_post_meta( $id, 'Email', true );
													*/ ?>
										<tr>
											<td>
												<input type="checkbox" value="checked" name="<?php /*echo $type */ ?>[<?php /*echo $month */ ?>][<?php /*echo $event_day */ ?>][<?php /*echo $ref */ ?>]">
											</td>
											<td>
												<a href="<?php /*echo get_site_url() . '/wp-admin/post.php?post=' . $id . '&action=edit' */ ?>"><?php /*echo $ref */ ?></a>
											</td>
											<td><?php /*echo $mail */ ?></td>
											<?php
			/*											$key = $month - 1;
														if ( $day_type === 'Month' ) {
															$event_date = "$event_day $time_type[$key] " . $array[ 'year' ];
														}
														*/ ?>
											<td><?php /*echo date('d/m/y', strtotime($event_date)) */ ?></td>
											<td><?php /*echo $array[ 'status' ] */ ?></td>
											<td><?php /*echo ucfirst( str_replace( '_', ' ', $type ) ) */ ?></td>
											<td><?php /*echo date('d/m/y', strtotime($date)) */ ?></td>
											<td class="wpk-event-date"><?php /*echo strtotime( $date ) * 1000 */ ?></td>
										</tr>
									<?php /*endforeach; */ ?>
								<?php /*endforeach; */ ?>
							<?php /*endforeach; */ ?>
						<?php /*elseif ( $type === 'before_event' || $type === 'professional' ) : */ ?>
							<?php /*foreach ( $schedule as $day => $data ) : */ ?>
								<?php /*foreach ( $data as $ref => $array ) : */ ?>
									<?php
			/*									$id   = $array[ 'invoice_id' ];
												$date = $this->get_meta( $id, 'Date' );
												$date = self::translate_months( $date );
												$mail = get_post_meta( $id, 'Email', true );
												*/ ?>
									<tr>
										<td>
											<input type="checkbox" value="checked" name="<?php /*echo $type */ ?>[<?php /*echo $day */ ?>][<?php /*echo $ref */ ?>]">
										</td>
										<td>
											<a href="<?php /*echo get_site_url() . '/wp-admin/post.php?post=' . $id . '&action=edit' */ ?>"><?php /*echo $ref */ ?></a>
										</td>
										<td><?php /*echo $mail */ ?></td>
										<?php
			/*										$key = $day_type === 'Month' ? (int) $day - 1 : $day;
													if ( $type === 'before_event' ) {
														echo '<td>' . date( 'd/m/Y', strtotime( strtolower( $time_type[ $key ] ) ) ) . '</td>';
													} else {
														if ( $day_type === 'Month' ) {
															$event_date = "$time_type[$day_type]-$days[1]";
														} */ ?>
											<td><?php /*echo $time_type[ $key ] */ ?></td>
											<?php
			/*										}
													*/ ?>
										<td><?php /*echo $array[ 'status' ] */ ?></td>
										<td><?php /*echo ucfirst( str_replace( '_', ' ', $type ) ) */ ?></td>
										<td><?php /*echo date('d/m/y', strtotime($date)) */ ?></td>
										<td class="wpk-event-date"><?php /*echo strtotime( $date ) * 1000 */ ?></td>
									</tr>
								<?php /*endforeach; */ ?>
							<?php /*endforeach; */ ?>
						<?php /*endif; */ ?>

					<?php /*endforeach; */ ?>
					</tbody>
				</table>
				<button name="disable" class="button button-primary" type="submit">Disable schedules</button>
				<button name="activate" class="button button-primary" type="submit">Activate schedules</button>
				<button name="remove" class="button button-primary" type="submit">Remove schedules</button>
			</form>
			--><?php
			/*			return ob_get_clean();*/
			ob_start();
			if ( WPKPassword::isValidationRequired() ) {
				?>
				<form action="#" method="POST" class="wpk-password-form">
					<div class="wpk-input">
						<input type="password" name="wpk_password" placeholder="Enter password to proceed.">
					</div>
					<div class="wpk-input">
						<button class="button button-primary" type="submit">Submit</button>
					</div>
				</form>
				<?php
			} else {
				if ( isset( $_POST[ 'send' ] ) && ! empty( $_POST[ 'invoice_id' ] ) ) {
					foreach ( $_POST[ 'invoice_id' ] as $id ) {
						$mail = $this->get_meta( $id, 'Email' );
						$ref  = $this->get_meta( $id, 'ref' );
						$this->send_mail_invoice( $id, $mail, $ref );
					}

				} else if ( isset( $_POST[ 'mark' ] ) && ! empty( $_POST[ 'invoice_id' ] ) ) {
					foreach ( $_POST[ 'invoice_id' ] as $id ) {
						$proffesional = empty( $this->get_meta( $id, 'Professionnel_de_levenementiel' ) ) ? $this->get_meta( $id, 'Professionnel_de_levènementiel' ) : $this->get_meta( $id, 'Professionnel_de_levènementiel' );
						$societe      = $this->get_meta( $id, 'Societe' );
						$event_type   = $this->get_meta( $id, 'Type_devenement' );
						$email        = $this->get_meta( $id, 'Email' );
						$ref          = $this->get_meta( $id, 'ref' );
						update_post_meta( $id, 'quote_status', 'Confirmed' );
						if ( strtolower( $proffesional ) === 'oui' || ! empty( $societe ) ) {
							$this->schedule->add_schedule( new DateTime(), $email, $ref, $id, 'proffesional' );
						}

						if ( $event_type !== 'Mariage' ) {
							$this->schedule->add_schedule( new DateTime(), $email, $ref, $id, 'after_event' );
						}

						foreach ( $this->schedule->schedules[ 'before_event' ] as $day => $events ) {
							foreach ( $events as $reference => $array ) {
								if ( $reference === $ref ) {
									unset( $this->schedule->schedules[ 'before_event' ][ $day ][ $ref ] );
									update_option( 'wpk_schedules', $this->schedule->schedules );
								}
							}
						}
					}
				} else if ( isset( $_POST[ 'empty_trash' ] ) && ! empty( $_POST[ 'invoice_id' ] ) ) {
					foreach ( $_POST[ 'invoice_id' ] as $id ) {
						wp_trash_post( $id );
					}
				} else if ( isset( $_POST[ 'standby' ] ) && ! empty( $_POST[ 'invoice_id' ] ) ) {
					foreach ( $_POST[ 'invoice_id' ] as $id ) {

						foreach ( $this->schedule->schedules[ 'before_event' ] as $day => $events ) {
							foreach ( $events as $ref => $array ) {
								if ( $array[ 'invoice_id' ] == $id ) {
									$id = $array[ 'invoice_id' ];

									update_post_meta( $array[ 'invoice_id' ], 'invoice_status', 'refused' );
									$this->schedule->schedules[ 'before_event' ][ $day ][ $ref ][ 'status' ] = 'disabled';
									update_option( 'wpk_schedules', $this->schedule->schedules );

								}

							}

						}

						update_post_meta( $id, 'invoice_status', 'refused' );
					}
				}
				$query = new WP_Query( [
					'post_type'      => 'wpk_invoice',
					'post_status'    => 'any',
					'posts_per_page' => - 1,
				] );


				$months = [
					'janvier'   => "January",    //January
					'février'   => "February", //February
					'mars'      => "March",      //March
					'avril'     => "April", //April
					'mai'       => "May", //May
					'juin'      => "Jun",         //Jun
					'juillet'   => "July",      //July
					'août'      => "August", //August
					'septembre' => "September",       //September
					'octobre'   => "October",    //October
					'novembre'  => "November", //November
					'décembre'  => "December"

				];      //December

				if ( $query->get_posts() ) {
					?>
					<div class="wpk-input-container">
						<label for="show_all"><?php _e( 'Show all events', $this->domain ) ?></label>
						<input type="checkbox" name="show_all" id="show_all" class="wpk-checkbox">
					</div>
					<form method="post">
						<table class="wpk-table wpk-invoice-table wpk-all-events-table">
							<thead>
							<tr>
								<th>Select</th>
								<th># Devis</th>
								<th>Date Evenement</th>
								<th>Entreprise</th>
								<th>Nom</th>
								<th>Montant HT</th>
								<th>TVA</th>
								<th>Montant TTC</th>
								<th>Statut Devis</th>
								<th>Statut Facture</th>
								<th># Facture</th>
								<th>Statut Paiement</th>
								<th>Type Paiement</th>
								<th>Modifier</th>
								<th>Rappel Paiement</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							<?php
							foreach ( $query->get_posts() as $post ) : ?>
								<?php
								$id   = $post->ID;
								$date = $this->get_meta( $id, 'Date' );
								foreach ( $months as $french_month => $month ) {
									$date = str_replace( $french_month, $month, $date );
								}
								$invoice_number = $this->get_meta( $id, 'invoice_id' );
								if ( empty( $invoice_number ) ) {
									$invoice_number = 'Non envoyé';

								}
								$cash           = $this->get_meta( $id, 'cash_status' ) === 'Cash' ? 'Cash' : '---';
								$name           = $this->get_meta( $id, 'Societe' );
								$nom            = $this->get_meta( $id, 'Nom' );
								$status         = $this->get_meta( $id, 'quote_status' );
								$status         = $status === 'Confirmed' ? 'Confirmé' : 'En attente';
								$invoice_status = $this->get_meta( $id, 'invoice_status' );
								if ( $invoice_status === 'refused' ) {
									$status = 'Stand by';
								}
								switch ( $invoice_status ) {
									case 'Sent' :
										$invoice_status = '<a target="_blank" href="' . get_home_url() . '/wp-admin/post.php?page=wpk_download_invoice&wpk_invoice=' . $id . '">Voir la facture</a>';
										break;
									default:
										$invoice_status = 'En attente';
								}

								//$invoice_status = $invoice_status === 'Sent' ? '<a target="_blank" href="' . get_home_url() . '/wp-admin/post.php?page=wpk_download_invoice&wpk_invoice=' . $id . '">Voir la facture</a>' : 'En attente';
								$paid           = $this->get_meta( $id, 'paid' );
								$paid           = strtolower( $paid ) === 'paid' ? 'Paye' : 'En attente';
								$ref            = $this->get_meta( $id, 'ref' );
								$gross_price    = $this->get_meta( $id, 'gross_price' );
								$vat_price      = $this->get_meta( $id, 'vat_price' );
								$total_price    = $this->get_meta( $id, 'total_price' );
								$reminder_count = empty( $this->get_meta( $id, 'reminder_count' ) ) ? 0 : $this->get_meta( $id, 'reminder_count' );
								?>
								<tr>
									<td><input type="checkbox" name="invoice_id[]" value="<?php echo $id ?>"></td>
									<td><?php echo $ref ?></td>
									<td class="event-date"><?php echo date( 'd/m/Y', strtotime( $date ) ) ?></td>
									<td><?php echo $name ?></td>
									<td><?php echo $nom ?></td>
									<td class="wpk-price gross-price"><?php echo $gross_price ?></td>
									<td class="wpk-price vat"><?php echo $vat_price ?></td>
									<td class="wpk-price total-price"><?php echo $total_price ?></td>
									<td><?php echo $status ?></td>
									<td><?php echo $invoice_status ?></td>
									<td>
										<?php echo $invoice_number ?>
									</td>
									<td>
										<button title="Click here to change payment status" class="wpk-paid button button-primary" data-id="<?php echo $id ?>"><?php echo $paid ?></button>
									</td>
									<td>
										<button class="wpk-cash button button-primary" data-id="<?php echo $id ?>"><?php echo $cash ?></button>
									</td>
									<td>
										<a target="_blank" class="wpk-edit-invoice button button-primary" href="<?php echo get_site_url() . '/wp-admin/post.php?post=' . $id . '&action=edit' ?>">
											Modifier
										</a>
									</td>
									<td>
										<button class="button button-primary wpk-send-reminder" data-id="<?php echo $id ?>"><?php printf( __( 'Envoyer un rappel (%s)', $this->domain ), $reminder_count ) ?> </button>
									</td>
									<td>
										<?php echo strtotime( $date ) * 1000 ?>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
							<tfoot>
							<tr>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
								<td>
									<input type="text" placeholder="Search"/>
								</td>
							</tr>
							</tfoot>
						</table>
						<button type="submit" name="send" class="button button-primary"><?php _e( 'Facturer', $this->domain ) ?></button> &nbsp;
						<button type="submit" name="mark" class="button button-primary"><?php _e( 'Confirmer les devis', $this->domain ) ?></button> &nbsp;
						<button type="submit" name="empty_trash" class="button button-primary"><?php _e( 'Supprimer', $this->domain ) ?></button> &nbsp;
						<button type="submit" name="standby" class="button button-primary"><?php _e( 'Mark as stand by', $this->domain ) ?></button> &nbsp;
					</form>
					<div class="wpk-sum">
						<span class="gross-sum"></span>
						<br>
						<span class="vat-sum"></span>
						<br>
						<span class="total-sum"></span>
					</div>
					<?php
					return ob_get_clean();
				}
			}

		}


		public function schedule_menu() {

			if ( WPKPassword::isValidationRequired() ) {
				?>
				<form action="#" method="POST" class="wpk-password-form">
					<div class="wpk-input">
						<input type="password" name="wpk_password" placeholder="Enter password to proceed.">
					</div>
					<div class="wpk-input">
						<button class="button button-primary" type="submit">Submit</button>
					</div>
				</form>
				<?php
			} else {
				$type = $_GET[ 'page' ];
				//$this->schedule->add_schedule( new DateTime(), 'przemek543@gmail.com', 'Flair-B7158', '48', 'before_event' );
				if ( isset( $_POST ) && ! empty( $_POST ) ) {
					$this->remove_schedules( $_POST, $type );
				}
				$days = [

					'Sunday',

					'Monday',

					'Tuesday',

					'Wednesday',

					'Thursday',

					'Friday',

					'Saturday',

				];

				$months = [

					"January",

					"February",

					"March",

					"April",

					"May",

					"June",

					"July",

					"August",

					"September",

					"October",

					"November",

					"December"

				];

				$time_type = $type === 'after_event' ? $months : $days;
				if ( $type === 'all_events' ) {
					echo $this->display_all_events();
				} else {


					$schedules = $this->schedule->schedules[ $type ];
					$day_type  = $type === 'after_event' ? 'Month' : 'Day';
					if ( ! empty( $schedules ) ) {
						?>
						<form method="post">
							<div class="wpk-input-container">
								<label for="show_all"><?php _e( 'Show all events', $this->domain ) ?></label>
								<input type="checkbox" name="show_all" id="show_all" class="wpk-checkbox">
							</div>
							<table class="wpk-table">
								<thead>
								<tr>
									<th>Select</th>
									<th>Reference</th>
									<th>Email</th>
									<?php if ( $type === 'before_event' ) : ?>
										<th>Week (limit <?php echo $this->schedule->email_limit ?>)</th>
									<?php endif ?>
									<th><?php echo $day_type ?></th>
									<th>Status</th>
									<th>Event date</th>
								</tr>
								</thead>
								<tbody>
								<?php if ( $type === 'after_event' ) : ?>
									<?php foreach ( $schedules as $month => $data ) : ?>
										<?php foreach ( $data as $event_day => $events ) : ?>
											<?php foreach ( $events as $ref => $array ) : ?>
												<?php
												$id   = $array[ 'invoice_id' ];
												$date = $this->get_meta( $id, 'Date' );
												$date = self::translate_months( $date );
												$mail = get_post_meta( $id, 'Email', true );
												?>
												<tr data-event_date="<?php echo $date ?>">
													<td>
														<input type="checkbox" value="checked" name="<?php echo $type ?>[<?php echo $month ?>][<?php echo $event_day ?>][<?php echo $ref ?>]">
													</td>
													<td>
														<a href="<?php echo get_site_url() . '/wp-admin/post.php?post=' . $id . '&action=edit' ?>"><?php echo $ref ?></a>
													</td>
													<td><?php echo $mail ?></td>
													<?php
													$key = $month - 1;
													if ( $day_type === 'Month' ) {
														$event_date = "$time_type[$key]-$event_day";
													}
													?>
													<td><?php echo $event_date ?></td>
													<td><?php echo $array[ 'status' ] ?></td>
													<td class="wpk-event-date"><?php echo strtotime( $date ) * 1000 ?></td>
												</tr>
											<?php endforeach; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
								<?php else : ?>
									<?php foreach ( $schedules as $day => $data ) : ?>
										<?php foreach ( $data as $ref => $array ) : ?>
											<?php
											$id   = $array[ 'invoice_id' ];
											$date = $this->get_meta( $id, 'Date' );
											$date = self::translate_months( $date );
											$mail = get_post_meta( $id, 'Email', true );
											?>
											<tr>
												<td>
													<input type="checkbox" value="checked" name="<?php echo $type ?>[<?php echo $day ?>][<?php echo $ref ?>]">
												</td>
												<td>
													<a href="<?php echo get_site_url() . '/wp-admin/post.php?post=' . $id . '&action=edit' ?>"><?php echo $ref ?></a>
												</td>
												<td><?php echo $mail ?></td>
												<?php if ( $type === 'before_event' ) : ?>
													<td><?php echo $array[ 'count' ] ?></td>
												<?php endif;
												$key = $day_type === 'Month' ? (int) $day - 1 : $day;
												if ( $day_type === 'Month' ) {
													$event_date = "$time_type[$day_type]-$days[1]";
												}
												?>
												<td><?php echo $time_type[ $key ] ?></td>
												<td><?php echo $array[ 'status' ] ?></td>
												<td class="wpk-event-date"><?php echo strtotime( $date ) * 1000 ?></td>
											</tr>
										<?php endforeach; ?>
									<?php endforeach; ?>
								<?php endif; ?>
								</tbody>
							</table>
							<button name="disable" class="button button-primary" type="submit">Disable schedules</button>
							<button name="activate" class="button button-primary" type="submit">Activate schedules</button>
							<button name="remove" class="button button-primary" type="submit">Remove schedules</button>
						</form>
						<?php
					}
				}
			}
		}


		public function admin_enqueue() {

			$css = $this->css_folder;

			$js = $this->js_folder;

			wp_enqueue_style( 'wpk-admin-css', $css . 'wpk-admin.css' );

			wp_enqueue_style( 'jq-datatables-css', 'https://cdn.datatables.net/v/dt/dt-1.10.15/r-2.1.1/datatables.min.css"' );

			wp_enqueue_script( 'jq-datatables', 'https://cdn.datatables.net/v/dt/dt-1.10.15/r-2.1.1/datatables.min.js', [ 'jquery' ], 1.0 );

			wp_enqueue_script( 'wpk-admin-js', $js . 'wpk-admin.js', [ 'jquery', 'jq-datatables' ], 1.0 );

			wp_localize_script( 'wpk-admin-js', 'wpk_admin', [

				'ajax_url' => admin_url( 'admin-ajax.php' ),

			] );

		}

		/**
		 * Undocumented function
		 *
		 * @return WPK_i698932
		 */
		public static function get_instance() {

			if ( empty( self::$instance ) && self::get_dependencies() ) {

				self::$instance = new self();

			}


			return self::$instance;

		}


		private static function get_dependencies() {

			if ( ! function_exists( 'get_field' ) ) {

				return false;

			}


			return true;

		}


		public function send_mail_ajax() {

			$post_id = $_POST[ 'id' ];

			$ref = $this->get_meta( $post_id, 'ref' );

			$mail = $this->get_meta( $post_id, 'Email' );

			$result = $this->send_mail_invoice( $post_id, $mail, $ref );

			if ( $result ) {


				exit( json_encode( 'Sent' ) );

			}

			exit( json_encode( 'Error' ) );

		}


		public function create_invoice_id() {

			if ( empty( get_option( 'wpk_invoice_last_date' ) ) ) {


				$date = new DateTime();

				$result = [

					'date' => [

						'day' => $date->format( 'd' ),

						'month' => $date->format( 'm' ),

						'year' => $date->format( 'Y' ),

					],

					'count' => 0

				];

				update_option( 'wpk_invoice_last_date', $result );

			}

		}


		public function update_invoice_meta( $post_id ) {

			$vat = $this->get_meta( $post_id, 'vat' );

			$gross_price = $this->get_meta( $post_id, 'gross_price' );

			$vat_price = $this->get_meta( $post_id, 'vat_price' );

			$total_price = $this->get_meta( $post_id, 'total_price' );

			if ( isset( $_POST[ 'wpk' ] ) ) {

				foreach ( $_POST[ 'wpk' ] as $key => $value ) {

					if ( $key !== 'total_price' and $key !== 'gross_price' ) {

						update_post_meta( $post_id, $key, $value );

					}

				}

				$wpk_total = $_POST[ 'wpk' ][ 'total_price' ];

				$wpk_vat = $_POST[ 'wpk' ][ 'vat' ];

				$wpk_gross = $_POST[ 'wpk' ][ 'gross_price' ];

				if ( ! empty( $wpk_gross ) ) {
					$wpk_gross = str_replace( '\\', '', $wpk_gross );
				}
				if ( ! empty( $wpk_total ) ) {
					$wpk_total = str_replace( '\\', '', $wpk_total );
				}


				if ( $gross_price !== $wpk_gross ) {

					//dump("Gross: $gross_price");
					//dump("WPK : $wpk_gross");


					$price = $this->calc_vat_reverse( $wpk_gross, $wpk_vat );

					update_post_meta( $post_id, 'total_price', $price[ 'total' ] );

					update_post_meta( $post_id, 'vat_price', $price[ 'vat' ] );

					update_post_meta( $post_id, 'gross_price', $price[ 'price' ] );

				} else if ( $wpk_total !== $total_price || $wpk_vat !== $vat ) {


					$price = $this->calc_vat( $wpk_total, $wpk_vat );


					update_post_meta( $post_id, 'total_price', $price[ 'total' ] );

					update_post_meta( $post_id, 'vat_price', $price[ 'vat' ] );

					update_post_meta( $post_id, 'gross_price', $price[ 'price' ] );


				}


			}

		}


		public function format_price( $price ) {

			$replace = $price > 999;


			setlocale( LC_MONETARY, 'fr_FR' );


			$price = (string) money_format( '%.2n', $price );


			$price = trim( $price );


			if ( $replace ) {


				$price = preg_replace( '/\s+/', '.', $price, 1 );

			}

			$price = str_replace( 'EUR', '', $price );


			//$price = str_replace( '.', ',', $price );

			//$price = str_replace( array( '.', ',' ), array( ',', '.' ), $price );

			//$price = str_replace( ' ', '\'', $price );

			$price = str_replace( '.', '\'', $price );
			$price = "$price.-CHF";


			return $price;

		}


		public function calc_vat( $price, $vat, $return_formatted = true ) {

			$price = stripslashes( $price );

			$debug = [];

			$debug[ 'array' ] = str_split( $price );

			$debug[ 'entered_price' ] = $price;

			if ( strpos( $price, '\'' ) !== false ) {

				$price = str_replace( [ '\'', '.' ], [ '', ',' ], $price );

				$price = str_replace( ',-CHF', '.-CHF', $price );


			}

			$fmt = numfmt_create( 'fr_FR', NumberFormatter::DECIMAL );

			$price_parsed = numfmt_parse( $fmt, $price );

			if ( ! $price_parsed ) {

				$price_parsed = (float) str_replace( [ '.' ], [ ',' ], $price );

			}

			$debug[ 'price_after_parse' ] = $price_parsed;

			$price_formatted = floatval( $price_parsed );

			$debug[ 'price_formatted' ] = $price_formatted;

			$calc = ( $vat / 100 );

			$price_calculated = (float) $price_formatted / ( 1 + $calc );

			$debug[ 'price_calculated' ] = $price_calculated;

			if ( $return_formatted ) {

				$vat = $this->format_price( $price_formatted - $price_calculated );


				$price_calculated = $this->format_price( $price_calculated );

				$price_parsed = $this->format_price( $price_parsed );

				$debug[ 'result' ] = [

					'total' => $price_parsed,

					'vat' => $vat,

					'price' => $price_calculated,

				];


				return [

					'total' => $price_parsed,

					'vat' => $vat,

					'price' => $price_calculated,

				];


			}

			$vat = $price_formatted - $price_calculated;


			return [

				'total' => $price_formatted,

				'vat' => round( $vat, 2 ),

				'price' => round( $price_calculated, 2 ),

			];


		}


		public function calc_vat_reverse( $gross, $vat ) {

			if ( strpos( $gross, '\'' ) !== false ) {

				$gross = str_replace( [ '\'', '.' ], [ '', ',' ], $gross );

				$gross = str_replace( ',-CHF', '.-CHF', $gross );


			}


			$fmt = numfmt_create( 'fr_FR', NumberFormatter::DECIMAL );

			$price_formatted = numfmt_parse( $fmt, $gross );

			$price_formatted = floatval( $price_formatted );
			$vat             = floatval( str_replace( ',', '.', $vat ) );

			$calc = ( $vat / 100 );

			$price_calculated = $price_formatted * $calc;

			$price_calculated = $price_formatted + $price_calculated;


			$vat = $this->format_price( $price_calculated - $price_formatted );

			$total = $this->format_price( $price_calculated );

			$gross_price = $this->format_price( $price_formatted );


			return [

				'total' => $total,

				'vat' => $vat,

				'price' => $gross_price

			];


		}


		public function acf_menu() {

			acf_add_options_sub_page( [

				'page_title' => 'Invoice settings',

				'menu_title' => 'Invoice settings',

				'menu_slug' => 'wpk_invoice_settings',

				'capability' => 'manage_options',

				'parent_slug' => 'wpk_schedules'

			] );

		}


		public function generate_invoice_id( $id ) {

			$date_counts = get_option( 'wpk_invoice_references' ) ? get_option( 'wpk_invoice_references' ) : [];

			$site_name = get_field( 'field_596df25a44900', 'option' );

			$event_date = $this->get_meta( $id, 'Date' );

			$months = [

				'janvier' => "January",    //January

				'février' => "February", //February

				'mars' => "March",      //March

				'avril' => "April", //April

				'mai' => "May", //May

				'juin' => "Jun",         //Jun

				'juillet' => "July",      //July

				'août' => "August", //August

				'septembre' => "September",       //September

				'octobre' => "October",    //October

				'novembre' => "November", //November

				'décembre' => "December"

			];      //December

			foreach ( $months as $french_month => $month ) {

				$event_date = str_replace( $french_month, $month, $event_date );

			}

			$date = new DateTime( $event_date );

			$date = $date->format( 'Ymd' );

			if ( isset( $date_counts[ $date ] ) ) {

				$count = $date_counts[ $date ];

			} else {

				$count = 1;

			}


			$id_res = $count < 10 ? '0' . $count : $count;


			$prefix = $site_name === 'Easyflash' || strtotime( $site_name ) === 'easyflash' ? 'F' : '';


			$date_counts[ $date ] = $count + 1;

			update_option( 'wpk_invoice_references', $date_counts );


			return $prefix . $date . '-' . $id_res;

		}


		public function get_invoice_id( $current_day, $last_day, $count ) {

			if ( $current_day !== $last_day ) {

				$count = 1;

			} else {

				$count ++;

			}


			return $count;


		}


		public function send_invoice( $post_id, $post_after, $post_before ) {


			$statuses = $this->get_post_statuses( $post_after, $post_before );


			if ( $statuses[ 'before' ] === 'pending' && $statuses[ 'after' ] === 'publish' ) {

				$ref = $this->get_meta( $post_id, 'ref' );

				$mail = $this->get_meta( $post_id, 'Email' );

				$result = $this->send_mail_invoice( $post_id, $mail, $ref );

			}


		}


		public function get_post_statuses( $after, $before ) {

			return [

				'before' => $before->post_status,

				'after' => $after->post_status

			];

		}


		public function register_invoice() {

			register_post_type( 'wpk_invoice', [

				'labels' => [

					'name' => 'Invoices',

					'singular_name' => 'Invoice',

				],

				'public' => false,

				'show_ui' => true,

				'show_in_menu' => true,

				'show_in_admin_bar' => true,

				'supports' => [

					'custom-fields',

				]

			] );

		}


		public function get_form_values() {

			global $wpdb;

			$values = maybe_unserialize( $_POST[ 'values' ] );

			$formID = maybe_unserialize( $_POST[ 'form_id' ] );

			$details = maybe_unserialize( $_POST[ 'details' ] );

			$values = $this->get_input_labels( $values );

			$values = array_merge( $values, $details );

			$table_name = $wpdb->prefix . "wpefc_forms";

			$rows = $wpdb->get_results( "SELECT * FROM $table_name WHERE id=$formID LIMIT 1" );

			$form = $rows[ 0 ];

			$ref = $form->current_ref;

			$ref_name = $form->ref_root;

			$date = new DateTime();

			$vat = get_field( 'field_5959ea8c1d337', 'options' );

			$values[ 'ref' ] = $ref_name . $ref;

			$values[ 'ref_name' ] = $ref_name;

			$values[ 'vat' ] = $vat;

			$values[ 'vat_number' ] = get_field( 'field_5959f2983df2d', 'options' );

			$values[ 'quote_status' ] = 'Not confirmed';

			$values[ 'paid' ] = 'Not paid';

			$values[ 'invoice_status' ] = 'Not sent';

			$price = $this->calc_vat( $values[ 'price' ], $vat, true );

			$values[ 'total_price' ] = $price[ 'total' ];

			$values[ 'vat_price' ] = $price[ 'vat' ];

			$values[ 'gross_price' ] = $price[ 'price' ];

			$id = $this->add_invoice( $ref_name . $ref );

			if ( $values[ 'lang' ] === 'en' ) {

				$values = $this->translate_label( $values );

			}


			if ( $id ) {


				$this->insert_invoice_meta( $id, $values );


				$this->schedule->add_schedule( $date, $values[ 'Email' ], $ref_name . $ref, $id );

			}

			exit( json_encode( [

				'id' => $id,

				'data' => $values,

				'ref' => $form->current_ref,

				'ref_root' => $form->ref_root,

				'price' => $price,

			] ) );

		}


		public function translate_label( $values ) {

			if ( isset( $values[ 'Company' ] ) ) {

				$values[ 'Societe' ] = $values[ 'Company' ];

				unset( $values[ 'Company' ] );

			}

			if ( isset( $values[ 'Event_Professional' ] ) ) {

				$values[ 'Professionnel_de_levenementiel' ] = $values[ 'Event_Professional' ];

				$values[ 'Professionnel_de_levenementiel' ] = strtolower( $values[ 'Professionnel_de_levenementiel' ] ) === 'yes' ? 'OUI' : 'NON';

				unset( $values[ 'Event_Professional' ] );

			}

			if ( isset( $values[ 'Name' ] ) ) {

				$values[ 'Nom' ] = $values[ 'Name' ];

				unset( $values[ 'Name' ] );

			}

			if ( isset( $values[ 'Date_of_Event' ] ) ) {

				$values[ 'Date' ] = $values[ 'Date_of_Event' ];

				unset( $values[ 'Date_of_Event' ] );

			}

			if ( isset( $values[ 'Address' ] ) ) {

				$values[ 'Adresse' ] = $values[ 'Address' ];

				unset( $values[ 'Address' ] );

			}

			if ( isset( $values[ 'Phone_Number' ] ) ) {

				$values[ 'Telephone' ] = $values[ 'Phone_Number' ];

				unset( $values[ 'Phone_Number' ] );

			}

			if ( isset( $values[ 'Type_of_Event' ] ) ) {

				$values[ 'Type_devenement' ] = $values[ 'Type_of_Event' ];

				unset( $values[ 'Type_of_Event' ] );

				if ( $values[ 'Type_devenement' ] === 'Wedding' ) {

					$values[ 'Type_devenement' ] = 'Mariage';

				}

			}


			return $values;


		}


		public function get_input_label( $input_id ) {

			global $wpdb;

			$table = $wpdb->prefix . 'wpefc_items';

			$rows = $wpdb->get_results( "SELECT title FROM $table WHERE id=$input_id LIMIT 1" );

			$result = $rows[ 0 ];

			$label = $result->title;

			$label = str_replace( '(où se déroule l\'évenement)', '', $label );

			$label = str_replace( [ '(renseigne lors du devis)', '/', '\'' ], [ '' ], $label );


			$label = str_replace( ' ', '_', $label );

			$label = str_replace( 'é', 'e', $label );

			$label = str_replace( 'ê', 'e', $label );

			$label = str_replace( ',', '', $label );

			$label = str_replace( '\'', '', $label );

			$label = str_replace( '’', '', $label );


			return $label;

		}


		public function get_input_labels( $values ) {

			$result = [];

			foreach ( $values as $label => $value ) {

				if ( ! empty( $value ) && ! is_null( $value ) ) {

					if ( $label == 'price' || $label == 'lang' || $label == '_email' || $label == '_phone' ) {

						$result[ $label ] = $value;

					} else {

						$label = $this->get_input_label( $label );

						$result[ $label ] = $value;

					}

				}

			}


			return $result;

		}


		public function add_invoice( $ref ) {

			$args = [

				'post_type' => 'wpk_invoice',

				'meta_query' => [

					[

						'key' => 'ref',

						'value' => $ref,

						'compare' => '=',

					],

				]

			];

			$query = new WP_Query( $args );

			if ( $query->post_count == 0 ) {

				$post = wp_insert_post( [

					'post_type' => 'wpk_invoice',

					'post_author' => get_current_user_id(),

					'post_title' => "$ref invoice",

					'post_status' => 'pending'

				] );


				return $post;

			}


			return false;

		}


		public function invoice_metabox() {

			//add_meta_box( 'wpk_invoice_metabox', 'Invoice preview', array( $this, 'metabox_content' ), 'wpk_invoice' );

			add_meta_box( 'wpk_invoice_inputs', 'Invoice details', [

				$this,

				'invoice_form_fields'

			], 'wpk_invoice' );

			add_meta_box( 'wpk_invoice_pdf', 'Download PDF', [ $this, 'invoice_pdf' ], 'wpk_invoice', 'side' );


			add_meta_box( 'wpk_invoice_table', 'Form content', [ $this, 'display_form_table' ], 'wpk_invoice' );

		}


		public function display_form_table( $post ) {

			$id = $post->ID;

			$table = $this->get_form_result_table( $this->get_meta( $id, 'ref' ) );


			foreach ( $table as $row ) {


				echo $row->content;

			}

		}

		public function send_quote_to_workers( $id ) {

			$emails = get_field( 'wpk_confirmation_emails', 'option' );
			if ( ! empty( $emails ) ) {
				$ref   = get_post_meta( $id, 'ref', true );
				$wpk   = WPK_i698932::get_instance();
				$table = $wpk->get_form_result_table( $ref );

				if ( empty( $table ) ) {
					return false;
				}

				$content = remove_accents( $table[ 0 ]->content );

				// Create a new DOMDocument and load the HTML
				$dom = new DOMDocument( '1.0', 'UTF-8' );
				$dom->loadHTML( $content );

				// Create a new XPath query
				$xpath = new DOMXPath( $dom );

				$result = $xpath->query( '//span' );


				// Loop the results and remove them from the DOM
				/** @var DOMElement $cell */
				foreach ( $result as $cell ) {

					$content = strtolower( $cell->nodeValue );
					if ( strpos( $content, '.-chf' ) !== false ) {
						$cell->nodeValue = '0’000.-CHF';
					}

				}

				// Save back to a string
				$newhtml = $dom->saveHTML();


				$headers = $this->schedule->prepare_email_headers();
				$subject = sprintf( __( 'Event %s', $this->domain ), $ref );


				foreach ( $emails as $email ) {
					return wp_mail( $email[ 'email' ], $subject, ( $newhtml ), $headers );
				}
			}

			return false;

		}


		public function invoice_form_fields( $post ) {

			$id = $post->ID;

			$mail = $this->get_meta( $id, 'Email' );

			$vat = $this->get_meta( $id, 'vat' );

			$gross_price = $this->get_meta( $id, 'gross_price' );

			$vat_price = $this->get_meta( $id, 'vat_price' );

			$total_price = $this->get_meta( $id, 'total_price' );

			$name = $this->get_meta( $id, 'Societe' );

			$invoice_address = trim( $this->get_meta( $id, 'invoice_address' ) );

			$nom = $this->get_meta( $id, 'Nom' );

			$date = $this->get_meta( $id, 'Date' );

			$ref = $this->get_meta( $id, 'ref' );

			$event = empty( $this->get_meta( $id, 'Professionnel_de_levenementiel' ) ) ? $this->get_meta( $id, 'Professionnel_de_levènementiel' ) : $this->get_meta( $id, 'Professionnel_de_levenementiel' );

			$event = $event === 'OUI' ? 'Yes' : 'No';

			$phone = $this->get_meta( $id, 'Telephone' );

			$invoice_id = $this->get_meta( $id, 'invoice_id' );

			$invoice_societe = $this->get_meta( $id, 'invoice_societe' );

			$details = empty( $this->get_meta( $id, 'product_details' ) ) ? get_field( 'field_5974910445ac5', 'option' ) : $this->get_meta( $id, 'product_details' );

			if ( empty( $invoice_id ) ) {

				$invoice_id = 'Not sent yet';

			}


			ob_start();

			?>

			<div class="wpk-input-wrap">

				<div class="wpk-input">

					<span class="wpk-title">Email</span>

					<input type="text" name="wpk[Email]" value="<?php echo $mail ?>">

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Company</span>

					<input type="text" name="wpk[Societe]" value="<?php echo $name ?>">

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Name / company (from confirmation form)</span>

					<input type="text" name="wpk[invoice_societe]" value="<?php echo $invoice_societe ?>">

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Invoice header</span>

					<textarea name="wpk[invoice_address]"><?php echo $invoice_address ?></textarea>

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Name</span>

					<input type="text" name="wpk[Nom]" value="<?php echo $nom ?>">

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Phone number</span>

					<input type="text" name="wpk[Telephone]" value="<?php echo $phone ?>">

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Date</span>

					<input type="text" name="wpk[Date]" value="<?php echo $date ?>">

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Reference nr</span>

					<input type="text" name="wpk[ref]" value="<?php echo $ref ?>">

				</div>


				<div class="wpk-input">

					<span class="wpk-title">Details</span>

					<textarea name="wpk[product_details]"><?php echo $details ?></textarea>

				</div>


				<div class="wpk-input">

					<span class="wpk-title">Montant HT:</span>

					<span><input type="text" name="wpk[gross_price]" value="<?php echo $gross_price ?>"></span>

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Taux de TVA (en %)</span>

					<input type="text" name="wpk[vat]" value="<?php echo $vat ?>">

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Montant TVA</span>

					<input type="text" name="wpk[vat_price]" value="<?php echo $vat_price ?>">

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Montant TTC</span>

					<input type="text" name="wpk[total_price]" value="<?php echo $total_price ?>">


				</div>

				<div class="wpk-input">

					<span class="wpk-title">Invoice ID</span>

					<span><?php echo $invoice_id ?></span>

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Quote status:</span>

					<span><?php echo $this->get_meta( $id, 'quote_status' ) ?></span>

				</div>

				<div class="wpk-input">

					<span class="wpk-title">Proffesional of events:</span>

					<span><?php echo $event ?></span>

				</div>


			</div>

			<?php

			echo ob_get_clean();

		}


		public function admin_generate_invoice() {

			if ( isset( $_GET[ 'wpk_invoice' ] ) && ! empty( $_GET[ 'wpk_invoice' ] ) ) {

				$args = $this->generate_invoice_html( $_GET[ 'wpk_invoice' ] );

				$this->generate_invoice_pdf( $args );

			}


		}


		public function invoice_pdf( $post ) {

			ob_start();

			?>

			<div class="wpk-invoice-form">

				<a target="_blank" href="<?php echo get_site_url() ?>/wp-admin/admin.php?page=wpk_download_invoice&wpk_invoice=<?php echo $post->ID ?>">Download PDF</a>

			</div>

			<br>

			<div class="wpk-invoice-form">


				<button class="button-primary button wpk-send-invoice" data-id="<?php echo $post->ID ?>">Send invoice</button>

				<br>

				<span>Current status: <span class="wpk-invoice-status"><?php echo $this->get_meta( $post->ID, 'invoice_status' ) ?></span></span>

			</div>

			<?php

			echo ob_get_clean();

		}


		public function mpdf_generate_invoice( $html, $output = 'I', $name = 'invoice.pdf' ) {

			$pdf = new \Mpdf\Mpdf( [

				'default_font_size' => 9,

				'default_font' => 'dejavusans'

			] );

			$pdf->writeHTML( '<page>' . $html[ 'html' ] . '' . $html[ 'footer' ] . '</page>' );

			if ( $output !== 'F' ) {

				$pdf->output( $name, $output );

			} else {

				try {

					$pdf->output( $this->dir . '/pdf/' . $name, 'F' );

				} catch ( \Mpdf\MpdfException $e ) {

					echo $e->getMessage();

				}


				return [

					'url' => plugin_dir_url( __FILE__ ) . '/pdf/' . $name,

					'path' => $this->dir . '/pdf/' . $name,

				];

			}

		}


		public function generate_invoice_pdf( $html, $output = 'I', $name = 'invoice.pdf' ) {

			//require_once 'includes/html2pdf/html2pdf.class.php';

			$pdf = new HTML2PDF( 'P', 'A4', 'fr', true, 'UTF-8', [ 20, 10, 20, 10 ] );

			$pdf->setDefaultFont( 'dejavusans' );

			$pdf->pdf->setDisplayMode( 'fullpage' );

			$pdf->writeHTML( '<page>' . $html[ 'html' ] . '<page_footer>' . $html[ 'footer' ] . '</page_footer></page>' );

			if ( $output !== 'F' ) {

				$pdf->Output( 'invoice.pdf', $output, $name );

			} else {

				try {

					$pdf->Output( $this->dir . '/pdf/' . $name, 'F' );

				} catch ( HTML2PDF_exception $e ) {

					$expection = new \Spipu\Html2Pdf\Exception\ExceptionFormatter( $e );

					echo $expection->getHtmlMessage();

				}


				return [

					'url' => plugin_dir_url( __FILE__ ) . '/pdf/' . $name,

					'path' => $this->dir . '/pdf/' . $name,

				];

			}

		}


		public function get_meta( $id, $meta ) {

			return get_post_meta( $id, $meta, true );

		}


		public function format_date( $date ) {

			$_date = new DateTime( $date );

			$formated = $_date->format( 'M d, Y' );


			return $formated;

		}


		public function get_customer_details( $id ) {

			$data = [

				'societe' => $this->get_meta( $id, 'Societe' ),

				'phone' => $this->get_meta( $id, 'Telephone' ),

				'mail' => $this->get_meta( $id, 'Email' ),

				'nom' => $this->get_meta( $id, 'Nom' ),

				'invoice_societe' => $this->get_meta( $id, 'invoice_societe' )

			];


			return $data;


		}


		public function format_variables( $text, $id ) {


			$ref = $this->get_meta( $id, 'ref' );

			$invoice_id = $this->get_meta( $id, 'invoice_id' ) ? $this->get_meta( $id, 'invoice_id' ) : '';

			$date = $this->get_meta( $id, 'Date' );

			$mail = $this->get_meta( $id, 'Email' );

			$name = $this->get_meta( $id, 'Societe' );

			$nom = $this->get_meta( $id, 'Nom' );

			$invoice_date = get_post( $id )->post_date;

			$invoice_date = new DateTime( $invoice_date );

			$months = [

				1 => "janvier",    //January

				2 => "février", //February

				3 => "mars",      //March

				4 => "avril", //April

				5 => "mai", //May

				6 => "juin",         //Jun

				7 => "juillet",      //July

				8 => "août", //August

				9 => "septembre",       //September

				10 => "octobre",    //October

				11 => "novembre", //November

				12 => "décembre"

			];      //December


			$month = $months[ $invoice_date->format( 'n' ) ];

			$invoice_date = $invoice_date->format( 'd' ) . ' ' . $month . ' ' . $invoice_date->format( 'Y' );


			$text = str_replace( '{ref}', $ref, $text );

			$text = str_replace( '{invoice_id}', $invoice_id, $text );

			$text = str_replace( '{invoice_date}', $invoice_date, $text );

			$text = str_replace( '{event_date}', $date, $text );

			$text = str_replace( '{customer_email}', $mail, $text );

			$text = str_replace( '{customer_name}', $nom, $text );

			$text = str_replace( '{company_name}', $name, $text );


			return $text;

		}


		public function check_ref() {

			$result = [

				'error' => false,

				'ignore' => false

			];


			$values = $this->get_input_labels( $_POST[ 'values' ] );

			if ( isset( $values[ 'Reference_devis' ] ) ) {

				$ref = $values[ 'Reference_devis' ];

			} else {

				$ref = $values[ 'Quote_reference' ];

			}

			if ( isset( $values[ 'Email_(renseigne_lors_du_devis)' ] ) ) {

				$email = $values[ 'Email_(renseigne_lors_du_devis)' ];

			} else if ( isset( $values[ 'Email_(same_as_in_the_quote)' ] ) ) {

				$email = $values[ 'Email_(same_as_in_the_quote)' ];


			} else if ( isset( $values[ 'Email' ] ) ) {

				$email = $values[ 'Email' ];

			} else {

				$email = $values[ '_email' ];

			}

			if ( empty( $email ) || empty( $ref ) ) {

				$result[ 'ignore' ] = true;

			}


			$args = [

				'post_type' => 'wpk_invoice',

				'meta_query' => [

					[

						'key' => 'ref',

						'value' => $ref,

						'compare' => '=',

					],

				]

			];

			$query = new WP_Query( $args );


			if ( $query->post_count === 0 ) {


				$result[ 'error' ] = true;


			} else {

				$post = array_shift( $query->get_posts() );

				$id = $post->ID;

				if ( strtolower( $this->get_meta( $id, 'Email' ) ) != strtolower( $email ) ) {

					$result[ 'error' ] = true;

				}

			}


			wp_send_json( $result );

		}


		public function generate_invoice_for_email() {

			$values = $this->get_input_labels( $_POST[ 'values' ] );

			if ( isset( $values[ 'Reference_devis' ] ) ) {

				$ref = $values[ 'Reference_devis' ];

			} else {

				$ref = $values[ 'Quote_reference' ];

			}


			if ( isset( $values[ 'Email_(renseigne_lors_du_devis)' ] ) ) {

				$email = $values[ 'Email_(renseigne_lors_du_devis)' ];

			} else if ( isset( $values[ 'Email_(same_as_in_the_quote)' ] ) ) {

				$email = $values[ 'Email_(same_as_in_the_quote)' ];


			} else if ( isset( $values[ 'Email' ] ) ) {

				$email = $values[ 'Email' ];

			} else {

				$email = $values[ '_email' ];

			}


			if ( isset( $values[ 'Societe__Nom' ] ) ) {

				$societe = $values[ 'Societe__Nom' ];

			} else if ( isset( $values[ 'Nom__Societe' ] ) ) {

				$societe = $values[ 'Nom__Societe' ];

			} else {

				$societe = $values[ 'Company__Name' ];

			}


			if ( isset( $values[ 'Adresse_du_lieu_de_levent_si_non_renseigne_dans_le_devis' ] ) ) {

				$address = $values[ 'Adresse_du_lieu_de_levent_si_non_renseigne_dans_le_devis' ];

			} else {

				$address = $values[ 'Event_site_address_if_not_confirmed_on_the_quote' ];

			}


			if ( isset( $values[ 'En-tete_de_facturation' ] ) ) {

				$invoice_details = $values[ 'En-tete_de_facturation' ];

			} else {

				$invoice_details = $values[ 'Invoice_Header' ];

			}


			if ( isset( $values[ 'Numero_de_natel_du_contact_sur_place' ] ) ) {

				$phone_number = $values[ 'Numero_de_natel_du_contact_sur_place' ];

			} else if ( isset( $values[ 'Cellphone_number_of_the_contact_person_onsite' ] ) ) {

				$phone_number = $values[ 'Cellphone_number_of_the_contact_person_onsite' ];

			} else {

				$phone_number = $values[ '_phone' ];

			}


			$args = [

				'post_type' => 'wpk_invoice',

				'meta_query' => [

					[

						'key' => 'ref',

						'value' => $ref,

						'compare' => '=',

					],

				]

			];

			$query = new WP_Query( $args );

			if ( $query->post_count === 0 ) {

				$args = [

					'post_type' => 'wpk_invoice',

					'meta_query' => [

						[

							'key' => 'ref',

							'value' => ucfirst( $ref ),

							'compare' => '=',

						],

					]

				];

				$query = new WP_Query( $args );

			}

			if ( $query->post_count > 0 ) {

				$post = array_shift( $query->get_posts() );

				$id = $post->ID;

				$proffesional = empty( $this->get_meta( $id, 'Professionnel_de_levenementiel' ) ) ? $this->get_meta( $id, 'Professionnel_de_levènementiel' ) : $this->get_meta( $id, 'Professionnel_de_levenementiel' );

				$event_type = $this->get_meta( $id, 'Type_devenement' );


				if ( $this->get_meta( $id, 'Email' ) != $email ) {

					exit( json_encode( 'Email not correct' ) );

				}

				update_post_meta( $id, 'quote_status', 'Confirmed' );

				if ( ! empty( $societe ) ) {

					update_post_meta( $id, 'invoice_societe', $societe );

				}

				if ( ! empty( $address ) ) {

					update_post_meta( $id, 'Adresse', $address );

				}

				if ( ! empty( $phone_number ) ) {

					update_post_meta( $id, 'Telephone', $phone_number );

				}

				if ( ! empty( $invoice_details ) ) {

					update_post_meta( $id, 'invoice_address', $invoice_details );

				}

				if ( strtolower( $proffesional ) === 'oui' ) {

					$this->schedule->add_schedule( new DateTime(), $email, $ref, $post->ID, 'proffesional' );

				}

				if ( $event_type !== 'Mariage' ) {

					$this->schedule->add_schedule( new DateTime(), $email, $ref, $post->ID, 'after_event' );

				}

				foreach ( $this->schedule->schedules[ 'before_event' ] as $day => $events ) {

					foreach ( $events as $reference => $array ) {

						if ( $reference === $ref ) {

							unset( $this->schedule->schedules[ 'before_event' ][ $day ][ $ref ] );

							update_option( 'wpk_schedules', $this->schedule->schedules );

						}

					}

				}

				exit( json_encode( [

					'count' => $query->post_count,

					'email' => $email,

					'args' => $args,

					'proffesional' => strtolower( $proffesional ),

					'values' => $values,

				] ) );

			}

			exit( json_encode( [

				'count' => $query->post_count,

				'data' => $_POST,

				'values' => $values,

				'args' => $args,

				'query' => $query

			] ) );


		}


		public function get_invoice_reference( $id ) {

			$invoice_date = get_post( $id )->post_date;

			$invoice_date = new DateTime( $invoice_date );

			$months = [

				1 => "janvier",    //January

				2 => "février", //February

				3 => "mars",      //March

				4 => "avril", //April

				5 => "mai", //May

				6 => "juin",         //Jun

				7 => "juillet",      //July

				8 => "août", //August

				9 => "septembre",       //September

				10 => "octobre",    //October

				11 => "novembre", //November

				12 => "décembre"

			];      //December


			$month = $months[ $invoice_date->format( 'n' ) ];


			return $invoice_date->format( 'd' ) . ' ' . $month . ' ' . $invoice_date->format( 'Y' );

		}


		public function send_mail_invoice( $post_id, $mail, $ref ) {

			$args = $this->generate_invoice_html( $post_id, true );

			$invoice_ref = $this->get_meta( $post_id, 'invoice_id' );

			$invoice = $this->generate_invoice_pdf( $args, 'F', $invoice_ref . '.pdf' );

			$lang = $this->get_meta( $post_id, 'lang' );

			$mail_text = $this->mail_text[ $lang ];

			$proffesional = empty( $this->get_meta( $post_id, 'Professionnel_de_levenementiel' ) ) ? $this->get_meta( $post_id, 'Professionnel_de_levènementiel' ) : $this->get_meta( $post_id, 'Professionnel_de_levènementiel' );

			if ( ! empty( $mail_text ) ) {

				$site = get_field( 'field_596df25a44900', 'option' );

				$ref = $this->get_meta( $post_id, 'ref' );

				$mail = $this->get_meta( $post_id, 'Email' );


				$nom = $this->get_meta( $post_id, 'Nom' );

				$societe = $this->get_meta( $post_id, 'Societe' );

				if ( $lang === 'en' ) {


					$title = "$site, Invoice $invoice_ref $nom $societe";

				} else {

					$title = "$site, Facture $invoice_ref $nom $societe";

				}

				$text = $this->format_variables( $mail_text, $post_id );

				$result = wp_mail( $mail, $title, $text, $this->schedule->prepare_email_headers(), [ $invoice[ 'path' ] ] );

				$admin_email = get_field( 'field_596df24a448ff', 'option' );

				if ( ! empty( $admin_email ) ) {

					wp_mail( $admin_email, $title, $text, $this->schedule->prepare_email_headers(), [ $invoice[ 'path' ] ] );

				}

				if ( $result ) {

					update_post_meta( $post_id, 'invoice_status', 'Sent' );

					wp_update_post( [

						'ID' => $post_id,

						'post_status' => 'publish'

					] );

				}

				unlink( $this->dir . '/pdf/' . $invoice_ref . '.pdf' );


				if ( strtolower( $proffesional ) === 'oui' ) {

					foreach ( $this->schedule->schedules[ 'proffesional' ] as $day => $schedules ) {

						foreach ( $schedules as $_ref => $schedule ) {

							if ( $schedule[ 'invoice_id' ] == $post_id ) {

								unset( $this->schedule->schedules[ 'proffesional' ][ $day ][ $_ref ] );

								update_option( 'wpk_schedules', $this->schedule->schedules );

							}

						}

					}

				}


				return $result;

			}


			return false;

		}

		public function remove_identical_schedules( $invoice_id ) {

			$schedules = $this->schedule->schedules;
			$email     = get_post_meta( $invoice_id, 'Email', true );

			$date = $this->get_meta( $invoice_id, 'Date' );
			$date = self::translate_months( $date );
			$date = new DateTime( $date );
			foreach ( $schedules[ 'before_event' ] as $day => $events ) {
				foreach ( $events as $ref => $array ) {
					$event_email = $array[ 'mail' ];
					if ( $event_email === $email ) {
						$id         = $array[ 'invoice_id' ];
						$event_date = get_post_meta( $id, 'Date', true );
						$event_date = self::translate_months( $event_date );
						$event_dt   = new DateTime( $event_date );
						$status     = get_post_meta( $id, 'quote_status', true );
						if ( $event_dt->getTimestamp() === $date->getTimestamp() && $id != $invoice_id && $status !== 'Confirmed' ) {
							update_post_meta( $array[ 'invoice_id' ], 'invoice_status', 'refused' );
							$this->schedule->schedules[ 'before_event' ][ $day ][ $ref ][ 'status' ] = 'disabled';
							update_option( 'wpk_schedules', $this->schedule->schedules );
						}
					}

				}

			}

			foreach ( $schedules[ 'after_event' ] as $day => $events ) {

				foreach ( $events as $month_day => $data ) {

					foreach ( $data as $ref => $array ) {
						$event_email = $array[ 'mail' ];
						if ( $event_email === $email ) {
							$id         = $array[ 'invoice_id' ];
							$event_date = get_post_meta( $id, 'Date', true );
							$event_date = self::translate_months( $event_date );
							$event_dt   = new DateTime( $event_date );
							if ( $event_dt->getTimestamp() === $date->getTimestamp() && $id != $invoice_id ) {
								$schedules[ 'after_event' ][ $day ][ $month_day ][ $ref ][ 'status' ] = 'disabled';
							}
						}

					}

				}

			}

		}


		public function remove_schedule() {


			if ( isset( $_GET[ 'wpk_cancel_email' ] ) && isset( $_GET[ 'wpk_ref' ] ) && isset( $_GET[ 'wpk_mail_id' ] ) && isset( $_GET[ 'wpk_event_type' ] ) ) {


				$id = $_GET[ 'wpk_mail_id' ];

				$event_type = $_GET[ 'wpk_event_type' ];

				if ( $event_type === 'after_event' ) {

					foreach ( $this->schedule->schedules[ $event_type ] as $day => $events ) {

						foreach ( $events as $month_day => $data ) {

							foreach ( $data as $ref => $array ) {

								if ( $array[ 'id' ] == $id ) {

									unset( $this->schedule->schedules[ $event_type ][ $day ][ $month_day ][ $ref ] );

									update_option( 'wpk_schedules', $this->schedule->schedules );

									$this->isValidation = true;

								}

							}

						}

					}

				} else {

					foreach ( $this->schedule->schedules[ $event_type ] as $day => $events ) {

						foreach ( $events as $ref => $data ) {

							if ( $data[ 'id' ] === $id ) {

								unset( $this->schedule->schedules[ $event_type ][ $day ][ $ref ] );

								update_option( 'wpk_schedules', $this->schedule->schedules );

								$this->isValidation = true;

								update_post_meta( $data[ 'invoice_id' ], 'invoice_status', 'refused' );

							}

						}

					}

				}

			}

		}


		public function showPopup() {

			if ( $this->isValidation ) {

				?>

				<div class="wpk-remove-schedule">

					<div>

						<span><strong><?php _e( 'Success!', $this->domain ) ?></strong><?php _e( 'Emails have been canceled.', $this->domain ) ?></span>

						<span class="close">x</span>

					</div>

				</div>

				<?php

			}

		}


		public function mpdf_generate_invoice_html( $id, $generate_id = false ) {


			$customer_details = $this->get_customer_details( $id );

			$vat = $this->get_meta( $id, 'vat' );

			$ref = $this->get_meta( $id, 'ref' );

			$gross_price = $this->get_meta( $id, 'gross_price' );

			$vat_price = $this->get_meta( $id, 'vat_price' );

			$total_price = $this->get_meta( $id, 'total_price' );

			$months = [

				1 => "janvier",    //January

				2 => "février", //February

				3 => "mars",      //March

				4 => "avril", //April

				5 => "mai", //May

				6 => "juin",         //Jun

				7 => "juillet",      //July

				8 => "août", //August

				9 => "septembre",       //September

				10 => "octobre",    //October

				11 => "novembre", //November

				12 => "décembre"

			];      //December


			$invoice_address = trim( $this->get_meta( $id, 'invoice_address' ) );

			$invoice_address = str_replace( '<br />', '', $invoice_address );

			$invoice_address = explode( PHP_EOL, $invoice_address );

			$invoice_date = get_post( $id )->post_date;

			$invoice_date = new DateTime( $invoice_date );

			$month = $months[ $invoice_date->format( 'n' ) ];


			$start_date = $this->get_meta( $id, 'Date' );

			$header_logo = get_field( 'field_594782633cf22', 'option' );

			$invoice_id = get_post_meta( $id, 'invoice_id', true );

			$details = empty( $this->get_meta( $id, 'product_details' ) ) ? get_field( 'field_5974910445ac5', 'option' ) : $this->get_meta( $id, 'product_details' );

			$details = explode( ',', $details );


			if ( $generate_id && empty( $invoice_id ) ) {

				$invoice_id = $this->generate_invoice_id( $id );

				update_post_meta( $id, 'invoice_id', $invoice_id );

			}

			$header_content = $this->format_variables( get_field( 'field_594a6ac58d219', 'option' ), $id );

			$footer = $this->format_variables( get_field( 'field_594782ad3cf23', 'option' ), $id );


			ob_start();

			?>

			<div>

				<div style="width: 100%">

					<div class="header-own" style="float: left; width: 80%">

								<span style="text-align:right; display: inline-block;">

									<img src="<?php echo $header_logo[ 'sizes' ][ 'medium' ] ?>" alt="EasyFlare">

								</span>

						<span style="display: inline-block; text-align: left;">

									<?php echo $header_content ?>

								</span>

					</div>

					<div style="float:right; width: 20%;">

						<br>

						<span class="header-customer" style="text-align: left;">

									<br>

							<?php foreach ( $invoice_address as $addres ) {

								?>

								<span><?php echo $addres ?></span>

								<br>

								<?php

							} ?>



								</span>

					</div>

				</div>

				<div class="details wpk-invoice-list" style="position: relative; left:3px; font-weight: bold; list-style: none; margin: 0; padding: 0;">

					<span style="position: relative; right: 5px">

					<?php _e( 'Facture N° : ', $this->domain ) ?>

						<span style="font-weight: 200;"><?php echo $invoice_id ?></span>

					</span>

					<br>

					<br>

					<span><?php _e( 'Date de la facture: ', $this->domain ) ?>

						<span style="font-weight: 200; text-decoration: none">

							<?php echo $invoice_date->format( 'd' ) . ' ' . $month . ' ' . $invoice_date->format( 'Y' ) ?>

						</span>

					</span>

					<br>

					<br>

					<span style="text-decoration: none;"><?php _e( 'Numéro de TVA: ', $this->domain ) ?>

						<span style="font-weight: 200"><?php echo get_field( 'field_5959f2983df2d', 'option' ) ?></span></span>

					<br>

					<br>

					<span><?php _e( 'Date de la prestation: ', $this->domain ) ?>

						<span style="font-weight: 200; text-decoration: none"><?php echo $start_date ?> </span></span>

					<br>

					<br>

					<br>

					<br>

					<span><span style="text-decoration: underline;"><?php _e( 'DETAILS PRESTATION:', $this->domain ) ?></span>

						<br>

						<?php foreach ( $details as $detail ) : ?>

							<br>

							<span style="font-weight: 100; font-style: italic; text-decoration: none !important;"><?php echo $detail ?></span>

						<?php endforeach; ?>

						<br>

						<br>

					</span>


				</div>

				<br>

				<br>

				<br>

				<br>

				<div style="text-align: right" class="invoice-price">

					<span>

						<span><b>Montant HT:</b></span>

						<span><?php echo str_replace( [ '.', ',', '\'-CHF' ], [

								'\'',

								'.',

								'.-CHF'

							], $gross_price ) ?></span>

					</span>

					<br>

					<br>

					<span>

						<span><b>TVA <?php echo $vat ?>%: </b></span>

						<span><?php echo str_replace( [ '.', ',', '\'-CHF' ], [

								'\'',

								'.',

								'.-CHF'

							], $vat_price ) ?></span>

					</span>

					<br>

					<br>

					<span>

						<span><b>Montant TTC:</b></span>

						<span><?php echo str_replace( [ '.', ',', '\'-CHF' ], [

								'\'',

								'.',

								'.-CHF'

							], $total_price ) ?></span>

					</span>

				</div>


			</div>

			<br>

			<br>

			<br>

			<?php

			$html = ob_get_clean();


			$args = [

				'html' => $html,

				'footer' => $footer

			];

			$invoices_html = empty( get_option( 'wpk_invoice_html' ) ) ? [] : get_option( 'wpk_invoice_html' );

			$invoices_html[ $id ] = $args;

			update_option( 'wpk_invoice_html', $invoices_html );


			return $args;

		}


		public function generate_invoice_html( $id, $generate_id = false ) {


			$customer_details = $this->get_customer_details( $id );

			$vat = $this->get_meta( $id, 'vat' );

			$ref = $this->get_meta( $id, 'ref' );

			$gross_price = $this->get_meta( $id, 'gross_price' );

			$vat_price = $this->get_meta( $id, 'vat_price' );

			$total_price = $this->get_meta( $id, 'total_price' );

			$months = [

				1 => "janvier",    //January

				2 => "février", //February

				3 => "mars",      //March

				4 => "avril", //April

				5 => "mai", //May

				6 => "juin",         //Jun

				7 => "juillet",      //July

				8 => "août", //August

				9 => "septembre",       //September

				10 => "octobre",    //October

				11 => "novembre", //November

				12 => "décembre"

			];      //December


			$invoice_address = trim( $this->get_meta( $id, 'invoice_address' ) );

			$invoice_address = str_replace( '<br />', '', $invoice_address );

			$invoice_address = explode( PHP_EOL, $invoice_address );

			$invoice_date = get_post( $id )->post_modified;

			$invoice_date = new DateTime( $invoice_date );

			$month = $months[ $invoice_date->format( 'n' ) ];


			$start_date = $this->get_meta( $id, 'Date' );

			$header_logo = get_field( 'field_594782633cf22', 'option' );

			$invoice_id = $this->get_meta($id, 'invoice_id');

			$details = empty( $this->get_meta( $id, 'product_details' ) ) ? get_field( 'field_5974910445ac5', 'option' ) : $this->get_meta( $id, 'product_details' );

			$details = explode( ',', $details );


			if ( $generate_id && empty( $invoice_id ) ) {

				$invoice_id = $this->generate_invoice_id( $id );

				update_post_meta( $id, 'invoice_id', $invoice_id );

			}

			$header_content = $this->format_variables( get_field( 'field_594a6ac58d219', 'option' ), $id );

			$footer = $this->format_variables( get_field( 'field_594782ad3cf23', 'option' ), $id );


			ob_start();

			?>

			<div>

				<table style="width: 100%; height: 400px">

					<tr>

						<td style="width: 70%;">

							<span class="header-own" style="display: inline-block; float: left">

								<span style="text-align:right; display: inline-block;">

									<img src="<?php echo $header_logo[ 'sizes' ][ 'medium' ] ?>" alt="EasyFlare">

								</span>

								<span style="display: inline-block; text-align: left;">

									<?php echo $header_content ?>

								</span>

							</span>

						</td>

						<td style="width: 30%">

							<div style="display: inline-block; width: 100%;">

								<br>

								<span class="header-customer">

									<br>



									<br>

									<?php foreach ( $invoice_address as $addres ) {

										?>

										<span><?php echo wordwrap( $addres, 20, '<br>', true ) ?></span>

										<br>

										<?php

									} ?>



								</span>

							</div>

						</td>

					</tr>

				</table>

				<div class="details wpk-invoice-list" style="position: relative; left:3px; font-weight: bold; list-style: none; margin: 0; padding: 0;">

					<span style="position: relative; right: 5px">

					<?php _e( 'Facture N° : ', $this->domain ) ?>

						<span style="font-weight: 200;"><?php echo $invoice_id ?></span>

					</span>

					<br>

					<br>

					<span><?php _e( 'Date de la facture: ', $this->domain ) ?>

						<span style="font-weight: 200; text-decoration: none">

							<?php echo $invoice_date->format( 'd' ) . ' ' . $month . ' ' . $invoice_date->format( 'Y' ) ?>

						</span>

					</span>

					<br>

					<br>

					<span style="text-decoration: none;"><?php _e( 'Numéro de TVA: ', $this->domain ) ?>

						<span style="font-weight: 200"><?php echo get_field( 'field_5959f2983df2d', 'option' ) ?></span></span>

					<br>

					<br>

					<span><?php _e( 'Date de la prestation: ', $this->domain ) ?>

						<span style="font-weight: 200; text-decoration: none"><?php echo $start_date ?> </span></span>

					<br>

					<br>

					<br>

					<br>

					<br>

					<span><span style="text-decoration: underline;"><?php _e( 'DETAILS PRESTATION:', $this->domain ) ?></span>

						<br>

						<br>

						<?php foreach ( $details as $detail ) : ?>

							<br>

							<span style="font-weight: 100; font-style: italic; text-decoration: none !important;"><?php echo $detail ?></span>

						<?php endforeach; ?>

						<br>

						<br>

					</span>


				</div>

				<br>

				<br>

				<br>

				<br>

				<div style="text-align: right" class="invoice-price">

					<span>

						<span><b>Montant HT:</b></span>

						<span><?php echo str_replace( [ '.', ',', '\'-CHF' ], [

								'\'',

								'.',

								'.-CHF'

							], $gross_price ) ?></span>

					</span>

					<br>

					<br>

					<span>

						<span><b>TVA <?php echo $vat ?>%: </b></span>

						<span><?php echo str_replace( [ '.', ',', '\'-CHF' ], [

								'\'',

								'.',

								'.-CHF'

							], $vat_price ) ?></span>

					</span>

					<br>

					<br>

					<span>

						<span><b>Montant TTC:</b></span>

						<span><?php echo str_replace( [ '.', ',', '\'-CHF' ], [

								'\'',

								'.',

								'.-CHF'

							], $total_price ) ?></span>

					</span>

				</div>


			</div>

			<?php

			$html = ob_get_clean();


			$args = [

				'html' => $html,

				'footer' => $footer

			];

			$invoices_html = empty( get_option( 'wpk_invoice_html' ) ) ? [] : get_option( 'wpk_invoice_html' );

			$invoices_html[ $id ] = $args;

			update_option( 'wpk_invoice_html', $invoices_html );


			return $args;

		}


		public function metabox_content( $post ) {

			/*$id     = $post->ID;

			$ref    = $this->get_meta( $id, 'ref' );

			$args   = $this->generate_invoice_html( $id );

			$pdf    = $this->generate_invoice_pdf( $args, 'F', $ref . '.pdf' );

			$date   = new DateTime();

			$events = $this->schedule->get_today_events();



			ob_start();

			*/ ?><!--

			<object width="100%" height="600px" data="<?php /*echo $pdf[ 'url' ] */ ?>" type="application/pdf">

				<embed src="<?php /*echo $pdf[ 'url' ] */ ?>" type="application/pdf"></embed>

			</object>

			--><?php

			/*			echo ob_get_clean();*/


		}


		public function pdf_remove( $id ) {

			$ref = get_post_meta( $id, 'ref', true );

			unlink( $this->dir . '/pdf/' . $ref . '.pdf' );


		}


		public function insert_invoice_meta( $id, $data ) {

			foreach ( $data as $key => $item ) {

				update_post_meta( $id, $key, $item );

			}

		}


		public static function register_schedule() {

			WPKSchedule::register_schedule();

		}


		public static function unregister_schedule() {

			WPKSchedule::unregister_schedule();

		}


	}


	$WPK_i698932 = WPK_i698932::get_instance();

	register_activation_hook( __FILE__, [ 'WPK_i698932', 'register_schedule' ] );

	register_deactivation_hook( __FILE__, [ 'WPK_i698932', 'unregister_schedule' ] );

}

