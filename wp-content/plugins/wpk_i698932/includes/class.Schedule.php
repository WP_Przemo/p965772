<?php

class WPKSchedule {

	/*
	 * Used for storing actual date.
	 * */
	public $date;


	/*
	 * Array for storing all schedules
	 * */
	public $schedules;


	/*
	 * Array for storing mail texts
	 * */
	public $mail_text;


	/*
	 * Number of emails that will be sent for quote form.
	 * */
	public $email_limit;


	/*
	 * Stores schedules that will be removed on init.
	 * */
	static $to_remove;

	public $admin_email;


	public function __construct() {

		$this->date        = $this->format_date();
		$this->email_limit = 1;
		$this->schedules   = empty( get_option( 'wpk_schedules' ) ) ? array() : get_option( 'wpk_schedules' );
		$this->admin_email = get_field( 'field_5976ec90bdd6d', 'option' );

		$this->mail_text = array(
			'en' => array(
				'before_event' => array(
					'title' => get_field( 'field_59748832320c8', 'option' ),
					'text'  => get_field( 'field_59748985320ca', 'option' )
				),
				'after_event'  => array(
					'title' => get_field( 'field_59748997320cb', 'option' ),
					'text'  => get_field( 'field_597489a0320cc', 'option' ),
				)
			),
			'fr' => array(
				'before_event' => array(
					'title' => get_field( 'field_595a4afaeb1c0', 'option' ),
					'text'  => get_field( 'field_594ed462c66e6', 'option' )
				),
				'after_event'  => array(
					'title' => get_field( 'field_595a4ba7af1cd', 'option' ),
					'text'  => get_field( 'field_597489a0320cc', 'option' )
				),
			)
		);

		add_action( 'wpk_schedule_hook', array( $this, 'start_schedule' ) );

		//add_action( 'init', array( $this, 'remove_schedule' ) );


	}


	/**
	 * Add schedule.
	 * If it's before event, the email will be send every week for 3 times.
	 * It it's after event, the email will be send after every 10 months.
	 *
	 *
	 * @param DateTime $date - DateTime object for which event will be scheduled
	 * @param string $mail - target mail
	 * @param string $ref - Reference number
	 * @param int $invoice_id - ID of the invoice (post)
	 * @param string $type - if it's schedule for quote, or for confirmation form, or just timeout for proffesional of events.
	 */

	public function add_schedule( $date, $mail, $ref, $invoice_id, $type = 'before_event' ) {

		if ( $type === 'after_event' ) {
			$date = $this->get_meta( $invoice_id, 'Date' );
			$date = WPK_i698932::translate_months( $date );
			$date = new DateTime( $date );
		}
		if ( $type === 'proffesional' ) {
			$date = new DateTime();
		}
		$date = $this->format_date( $date, $type );


		$day = $date[ 'day' ];

		$month = $date[ 'month' ];

		$month_day = $date[ 'month_day' ];

		$year = $date[ 'year' ];

		$id = wp_generate_uuid4();

		if ( $type === 'after_event' ) {

			$this->schedules[ 'after_event' ][ $month ][ $month_day ][ $ref ] = array(

				'mail' => $mail,

				'id' => $id,

				'invoice_id' => $invoice_id,

				'status' => 'active',

				'year' => $year

			);

		} else if ( $type === 'before_event' ) {

			$this->schedules[ 'before_event' ][ $day ][ $ref ] = array(

				'mail' => $mail,

				'count' => 0,

				'id' => $id,

				'invoice_id' => $invoice_id,

				'status'    => 'active',
				/*Ignore the day in which quote form was sent.*/
				'not_first' => $month_day,

			);

		} else if ( $type === 'proffesional' ) {

			$this->schedules[ 'proffesional' ][ $day ][ $ref ] = array(

				'mail' => $mail,

				'id' => $id,

				'invoice_id' => $invoice_id,

				'status' => 'active',

			);
		}

		update_option( 'wpk_schedules', $this->schedules );


	}


	public function send_email( $mail, $ref, $id, $event, $schedule_id ) {
		$day  = $event === 'before_event' ? $this->date[ 'day' ] : $this->date[ 'month' ];
		$lang = get_post_meta( $id, 'lang', true );
		if ( ! $lang || empty( $lang ) ) {
			$lang = 'fr';
		}
		$text    = $this->mail_text[ $lang ][ $event ][ 'text' ];
		$subject = $this->mail_text[ $lang ][ $event ][ 'title' ];
		if ( ! empty( $text ) && ! empty( $subject ) ) {
			$link    = get_site_url() . '/?wpk_cancel_email=' . $day . '&wpk_ref=' . $ref . '&wpk_mail_id=' . $schedule_id . '&wpk_event_type=' . $event . '';
			$text    = $this->format_variables( $text, $id, $link );
			$subject = $this->format_variables( $subject, $id );
			$result  = wp_mail( $mail, $subject, $text, $this->prepare_email_headers() );
			if ( ! empty( $this->admin_email ) ) {
				wp_mail( $this->admin_email, $subject, $text, $this->prepare_email_headers() );
			}

			return $result;
		}

		return false;

	}


	/**
	 * Prepare headers for email.
	 *
	 * @return array
	 */
	public function prepare_email_headers() {
		$headers = array(
			"MIME-Version: 1.0",
			"Content-Type: text/html; charset=UTF-8;",


		);

		return $headers;
	}


	public function remove_schedule() {
		$to_remove = empty( get_option( 'wpk_to_remove' ) ) ? array() : get_option( 'wpk_to_remove' );
		foreach ( $to_remove as $event_type => $data ) {
			foreach ( $data as $day => $array ) {
				if ( is_array( $array ) ) {
					foreach ( $array as $ref => $id ) {
						$schedule = $this->schedules[ $event_type ][ $day ][ $ref ];
						if ( $schedule[ 'id' ] === $id ) {
							unset( $this->schedules[ $event_type ][ $day ][ $ref ] );
							update_option( 'wpk_schedules', $this->schedules );
						}
						unset( $to_remove[ $event_type ][ $day ][ $ref ] );
						update_option( 'wpk_to_remove', $to_remove );

					}
				}

			}
		}


	}

	/**
	 * Get events for today's day.
	 *
	 * @return mixed
	 */

	public function get_today_events() {

		$date = $this->date;

		$day       = $date[ 'day' ];
		$month     = $date[ 'month' ];
		$month_day = $date[ 'month_day' ];


		return array(
			'before_event' => $this->schedules[ 'before_event' ][ $day ],
			'after_event'  => $this->schedules[ 'after_event' ][ $month ][ $month_day ],
			'proffesional' => $this->schedules[ 'proffesional' ][ $day ],
		);

	}


	/**
	 * @param DateTime|bool $dateTime
	 *
	 * @return array
	 */

	public function format_date( $dateTime = false, $type = null ) {


		if ( ! $dateTime ) {

			$dateTime = new DateTime();

		}

		/*You an change the date interval for schedules (the format is P|number|M/D| (month/day
			For example if you would like to add 7 days to date it would be P7D.
		  */
		if ( $type === 'before_event' ) {
			/*Interval*/
			$dateTime->add( new DateInterval( 'P3D' ) );

		}

		if ( $type === 'after_event' ) {
			/*You can change the interval for after events here (same as above)*/
			$dateTime->add( new DateInterval( 'P10M' ) );

		}

		if ( $type === 'proffesional' ) {
			/*You can change the interval for professional of events here (same as above)*/
			$dateTime->add( new DateInterval( 'P3D' ) );
		}

		$date = array(

			'day' => $dateTime->format( 'w' ),

			'month_day' => $dateTime->format( 'd' ),

			'month' => $dateTime->format( 'm' ),

			'year' => $dateTime->format( 'Y' ),

		);


		return $date;

	}


	public function start_schedule() {
		$events    = $this->get_today_events();
		$date      = $this->date;
		$day       = $date[ 'day' ];
		$month     = $date[ 'month' ];
		$month_day = $date[ 'month_day' ];
		$year      = $date[ 'year' ];
		foreach ( $events[ 'before_event' ] as $ref => $event ) {
			$count          = $event[ 'count' ];
			$id             = $event[ 'invoice_id' ];
			$mail           = get_post_meta( $id, 'Email', true );
			$quote_status   = get_post_meta( $id, 'quote_status', true );
			$invoice_status = get_post_meta( $id, 'invoice_status', true );
			$schedule_id    = $event[ 'id' ];
			$status         = $event[ 'status' ];
			$not_day        = $event[ 'not_first' ];
			/*Comment this to test the schedule on day it was sent.*/
			if ( $status === 'active' && $quote_status === 'Not confirmed' && $invoice_status !== 'refused' && get_post_status($id) ) {
				$result  = $this->send_email( $mail, $ref, $id, 'before_event', $schedule_id );
				if ( $result ) {
					$count ++;
					if ( $count >= $this->email_limit ) {
						unset( $this->schedules[ 'before_event' ][ $day ][ $ref ] );
					} else {
						$this->schedules[ 'before_event' ][ $day ][ $ref ][ 'count' ] = $count;
					}
					update_option( 'wpk_schedules', $this->schedules );
				}
			}
		}

		foreach ( $events[ 'after_event' ] as $ref => $event ) {

			$id          = $event[ 'invoice_id' ];
			$mail        = get_post_meta( $id, 'Email', true );
			$schedule_id = $event[ 'id' ];
			$status      = $event[ 'status' ];
			$event_year  = $event[ 'year' ];
			if ( $status === 'active' && $event_year == $year && get_post_status( $id ) ) {
				$result  = $this->send_email( $mail, $ref, $id, 'after_event', $schedule_id );
				if ( $result ) {
					unset( $this->schedules[ 'after_event' ][ $month ][ $month_day ][ $ref ] );
					$this->add_schedule( new DateTime(), $mail, $ref, $id, 'after_event' );
					update_option( 'wpk_schedules', $this->schedules );
				}
			}
		}


	}

	public function format_variables( $text, $id, $link = false ) {

		$ref          = $this->get_meta( $id, 'ref' );
		$invoice_id   = $this->get_meta( $id, 'invoice_id' ) ? $this->get_meta( $id, 'invoice_id' ) : '';
		$date         = $this->get_meta( $id, 'Date' );
		$mail         = $this->get_meta( $id, 'Email' );
		$name         = $this->get_meta( $id, 'Societe' );
		$nom          = $this->get_meta( $id, 'Nom' );
		$invoice_date = get_post( $id )->post_date;
		$invoice_date = new DateTime( $invoice_date );
		$months       = array(
			1  => "janvier",    //January
			2  => "février", //February
			3  => "mars",      //March
			4  => "avril", //April
			5  => "mai", //May
			6  => "juin",         //Jun
			7  => "juillet",      //July
			8  => "août", //August
			9  => "septembre",       //September
			10 => "octobre",    //October
			11 => "novembre", //November
			12 => "décembre"
		);      //December

		$month        = $months[ $invoice_date->format( 'n' ) ];
		$invoice_date = $invoice_date->format( 'd' ) . ' ' . $month . ' ' . $invoice_date->format( 'Y' );

		if ( $link ) {
			$text = str_replace( '{link}', $link, $text );
		}

		$text = str_replace( '{ref}', $ref, $text );
		$text = str_replace( '{invoice_id}', $invoice_id, $text );
		$text = str_replace( '{invoice_date}', $invoice_date, $text );
		$text = str_replace( '{event_date}', $date, $text );
		$text = str_replace( '{customer_email}', $mail, $text );
		$text = str_replace( '{customer_name}', $nom, $text );
		$text = str_replace( '{company_name}', $name, $text );

		return $text;

	}

	public function get_meta( $id, $meta ) {
		return get_post_meta( $id, $meta, true );
	}

	public static function register_schedule() {
		wp_schedule_event( strtotime( '07:00:00' ), 'daily', 'wpk_schedule_hook' );

	}

	public static function unregister_schedule() {
		wp_clear_scheduled_hook( 'wpk_schedule_hook' );
	}


}
