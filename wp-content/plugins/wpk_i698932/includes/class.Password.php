<?php





class WPKPassword {



	private static $password = '';



	private static $admin_emails = array(

		'Easyflash' => 'maxime.alexandre@easyflair.com',

		'EasyFlair' => 'maxime.alexandre@easyflair.com'

	);



	private static $id = '12525sf@ssg';



	public function __construct() {

		if ( session_status() === PHP_SESSION_NONE ) {

			session_start();

		}

		self::$password = ! get_option( 'wpk_password' ) ? null : get_option( 'wpk_password' );

		add_action( 'init', array( $this, 'passwordValidation' ) );

		add_action( 'admin_menu', array( $this, 'addMenuPage' ), 90 );

	}



	public function addMenuPage() {

		add_submenu_page( 'wpk_schedules', 'WPK Password', 'WPK Password', 'manage_options', 'wpk_password', array(

			$this,

			'passwordForm'

		) );

	}



	private function passwordReset() {

		if ( $_GET[ 'wpk_password_id' ] == self::$id ) {

			delete_option( 'wpk_password' );

		}

	}



	public function passwordValidation() {

		if ( isset( $_GET[ 'wpk_password_id' ] ) ) {

			$this->passwordReset();

		}

		if ( isset( $_POST[ 'wpk_password' ] ) ) {

			$password = $_POST[ 'wpk_password' ];

			if ( empty( self::$password ) ) {

				return true;

			}

			if ( empty( $password ) ) {

				return false;

			}

			$md5 = md5( $password );

			if ( $md5 === self::$password ) {

				$_SESSION[ 'wpk_password' ] = $md5;



				return true;

			}



			return false;

		}



		return false;

	}





	public static function isValidationRequired() {

		if ( empty( self::$password ) ) {

			return false;

		} else {

			return empty( $_SESSION[ 'wpk_password' ] ) || $_SESSION[ 'wpk_password' ] !== self::$password;

		}

	}



	/**

	 * Prepare headers for email.

	 *

	 * @return string

	 */

	public function prepare_email_headers() {

		$separator = md5( time() );

		$eol       = PHP_EOL;

// main header (multipart mandatory)

		$headers = 'From: Name <no-reply@test.in>' . "\r\n";

		$headers .= "MIME-Version: 1.0" . $eol;

		$headers .= "Content-Type: text/html; charset=UTF-8; boundary=\"" . $separator . "\"" . $eol; // see below

		$headers .= "Content-Transfer-Encoding: 7bit" . $eol;



		return $headers;

	}



	public function passwordForm() {

		if ( isset( $_POST[ 'send_reminder' ] ) ) {

			$this->sendReminder();

		} else {

			if ( ! empty( $_POST[ 'wpk_password_new' ] ) ) {

				if ( empty( self::$password ) ) {

					update_option( 'wpk_password', md5( $_POST[ 'wpk_password_new' ] ) );

				} else {

					if ( md5( $_POST[ 'wpk_password_repeat' ] ) === self::$password ) {

						update_option( 'wpk_password', md5( $_POST[ 'wpk_password_new' ] ) );

					} else {

						echo '<p>Old password must be the same as the current password</p>';

					}

				}

			}

		}

		?>

		<form action="#" method="POST" class="wpk-password-form">

			<div class="wpk-input">

				<input type="password" name="wpk_password_new" placeholder="New password">



			</div>

			<?php if ( ! empty( self::$password ) ) : ?>

				<div class="wpk-input">

					<input type="password" name="wpk_password_repeat" placeholder="Old password">



				</div>

				<!--<div class="wpk-input">

					<button class="button button-primary" value="send_reminder" type="submit">Send password reminder</button>

				</div>-->

			<?php endif; ?>

			<div class="wpk-input">

				<button class="button button-primary" type="submit">Submit</button>

			</div>

		</form>

		<?php

	}



	private function sendReminder() {

		$site_name = get_field( 'field_596df25a44900', 'option' );

		if ( isset( self::$admin_emails[ $site_name ] ) ) {

			$admin_email = self::$admin_emails[$site_name];

			$link        = '<a href="' . get_site_url() . 'wp-admin/admin.php?page=wpk_password?wpk_password_id=' . self::$id . '">here</a>';

			$message = "To reset invoice password, clisk $link";

			wp_mail($admin_email, 'WPK Invoices password reset', $message, $this->prepare_email_headers());

		}

	}



}