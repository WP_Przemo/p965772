(function( $ ) {

	var Wpk = function() {
		this.popup = $( '.wpk-remove-schedule' );
		this.form = $( '#estimation_popup' );
		this.formID = this.form.data( 'form' );
		this.formResult = [];
		this.ajaxAction = this.getFormType();
		this.formLang = this.getFormLang();
		this.dataSent = false;
		this.setEvents();

	};

	Wpk.prototype = {
		setEvents: function() {
			this.closePopup();
			var that = this;
			/*$( document ).on( 'click', '#wpe_btnOrder', function() {
				$( document ).ajaxComplete( function() {
					if ( !that.dataSent && !that.isCustomizationForm() ) {
						var values     = that.getFormValues(),
							additional = {
								price: $( '.progress-bar-price span' ).text(),
								lang:  that.formLang,
							};
						$.ajax( {
							url:      wpk_i698932.ajax_url,
							dataType: 'json',
							method:   'POST',
							data:     {
								action:  that.ajaxAction,
								values:  values,
								details: additional,
								form_id: that.formID,
							},
							success:  function( data ) {
								console.log( data );
								that.dataSent = true;
							}
						} );
					}
				} );
			} );*/
			if ( this.getFormType() === 'wpk_send' ) {
				this.checkReferenceEmail();
			}

		},

		isCustomizationForm: function() {
			var customizationIds = wpk_i698932.customization_forms,
				id;
			for ( var i in customizationIds ) {
				id = customizationIds[ i ].replace( ' ', '' );
				if ( id == this.formID ) {
					return true;
				}
			}
			return false;
		},

		sendAjax: function() {
			var that       = this,
				values     = this.getFormValues(),
				additional = {
					price: $( '.progress-bar-price span' ).text(),
					lang:  that.formLang,
				};

			$.ajax( {
				url:      wpk_i698932.ajax_url,
				dataType: 'json',
				method:   'POST',
				data:     {
					action:  that.ajaxAction,
					values:  values,
					details: additional,
					form_id: that.formID,
				},
				success:  function( data ) {
					console.log( data );
				}
			} );

		},

		closePopup: function() {
			$( document ).on( 'click', '.wpk-remove-schedule .close', function() {
				var $parent = $( this ).parents( '.wpk-remove-schedule' );
				$parent.fadeOut();
			} );
		},

		getFormLang: function() {
			var langs = wpk_i698932.english_forms,
				id;
			for ( var i = 0; i < langs.length; i++ ) {
				id = langs[ i ].replace( ' ', '' );
				if ( id == this.formID ) {
					return 'en';
				}
			}

			return 'fr';
		},

		getFormType: function() {
			var forms = wpk_i698932.confirmation_forms,
				id;

			for ( var i = 0; i < forms.length; i++ ) {
				id = forms[ i ].replace( ' ', '' );
				if ( id == this.formID ) {
					return 'wpk_send';
				}
			}
			return 'wpk_invoice';
		},

		checkReferenceEmail: function() {
			var that      = this,
				lang      = this.formLang,
				validated = false,
				response  = {
					fr: 'La référence du devis ou l\'email saisis ne correspondent pas à ceux de votre devis. Merci de vérifier vos informations.',
					en: 'Your reference or your email are wrong. Please check your information.'
				};
			$( document ).on( 'click', '.btn-next', function() {
				var values = that.getFormValues(),
					$this  = $( this );
				if ( $( '.has-error:visible' ).length === 0 && !validated ) {
					$.ajax( {
						type:     'POST',
						url:      wpk_i698932.ajax_url,
						data:     {
							action: 'wpk_validate_reference',
							values: values
						},
						async:    false,
						dataType: 'json',
						success:  function( data ) {
							var $target = $( 'input[data-title="wpk"]' );
							if ( data && $target.length ) {
								if ( !data.ignore && data.error ) {
									window.alert( response[ lang ] );
								} else if ( !data.ignore && !data.error ) {
									console.log( data );
									$target.val( 'correct' );
									validated = true;
									$this.click();
								}
							}

						},
						error:    function( xhr, ajaxOptions, thrownError ) {
							console.log( 'error...', xhr );
							//error logging
						},
						complete: function() {
							//afer ajax call is completed
						}
					} );
				}
			} );

		},

		formatTitle: function( value ) {
			if ( 'undefined' !== typeof value ) {
				value = value.replace( '(renseigné lors du devis)', '' );
				value = value.replace( '/', '' );
				value = value.replace( ' ', '_' );
				value = value.replace( '\'', '' );
				value = value.replace( 'é', 'e' );
			}
			if ( this.formResult.lang === 'en' ) {
				value = this.translateLabel( value );
			}
			return value;
		},

		translateLabel: function( label ) {
			return label;
		},

		getFormValues: function() {
			var $inputs     = this.form.find( 'input' ).not( '[type=hidden]' ),
				$textAreas  = this.form.find( 'textarea' ),
				$selects    = this.form.find( 'select' ),
				$selectable = this.form.find( '.selectable' ),
				that        = this;
			$inputs.each( function() {
				var $this = $( this ),
					value = $this.val(),
					title = $this.data( 'itemid' );

				if ( 'undefined' !== typeof title && value !== '' ) {
					if ( value.length ) {
						that.formResult[ title ] = value;
					}
				}
			} );

			$textAreas.each( function() {
				var $this = $( this ),
					value = $this.val(),
					title = $this.data( 'itemid' );

				if ( 'undefined' !== typeof title ) {

					if ( value !== '' ) {
						that.formResult[ title ] = value;
					}
				}
			} );

			$selects.each( function() {
				var $this = $( this ),
					value = $this.val(),
					title = $this.data( 'itemid' );

				if ( 'undefined' !== typeof title ) {

					if ( value !== '' ) {
						that.formResult[ title ] = value;
					}
				}
			} );

			$selectable.each( function() {
				var $this = $( this ),
					value = 'selected',
					title = $this.data( 'itemid' );


				if ( 'undefined' !== typeof title ) {

					if ( $this.hasClass( 'checked' ) ) {
						if ( $this.find( '.icon_quantity' ).length > 0 ) {
							value = $this.find( '.icon_quantity' ).text();
						}
						that.formResult[ title ] = value;
					}
				}
			} );
			this.formResult.lang = this.formLang;

			return this.removeEmpty();

		},
		removeEmpty:   function() {
			var result = this.formResult;

			for ( var i in result ) {
				if ( result[ i ] === null || result[ i ] === undefined ) {
					delete result[ i ];
				}
			}
			result._email = wpe_getContactInformations( this.formID ).email;
			result._phone = wpe_getContactInformations( this.formID ).phone;
			return result;
		}

	};


	/*var wpk = {
		init: function() {
			wpk.form.form_init();
			wpk.close_popup();
			console.log( 'WPK init' );
		},

		close_popup: function() {
			$( document ).on( 'click', '.wpk-remove-schedule .close', function() {
				var $parent = $( this ).parents( '.wpk-remove-schedule' );
				$parent.fadeOut();
			} );
		},

		form: {
			form_init: function() {
				//$( document ).on( 'click', '#estimation_popup .selectable *', wpk.form.get_clickable_values );
				$( document ).on( 'click', '#wpe_btnOrder', wpk.form.send_data );


			},

			result: {},

			price: 0,

			format_title: function( value ) {
				if ( 'undefined' !== typeof value ) {
					value = value.replace( '(renseigné lors du devis)', '' );
					value = value.replace( '/', '' );
					value = value.replace( ' ', '_' );
					value = value.replace( '\'', '' );
					value = value.replace( 'é', 'e' );
				}
				return value;

			},

			get_input_values: function() {
				var $inputs     = $( '#estimation_popup input' ).not( 'input[type=hidden]' ),
					$selects    = $( '#estimation_popup select' ),
					$selectable = $( '#estimation_popup .selectable' ),
					result      = {};
				$inputs.each( function() {
					var $this = $( this ),
						value = $this.val(),
						title = wpk.form.format_title( $this.data( 'originaltitle' ) );
					if ( 'undefined' === typeof title ) {
						title = wpk.form.format_title( $this.data( 'title' ) );
					}
					if ( 'undefined' !== typeof title ) {
						if ( title.toLowerCase() === 'non' || title.toLowerCase() === 'oui' ) {
							title = $this.parents( '.genSlide' ).find( '.stepTitle' ).text();
						}

						if ( value.length ) {
							result[ title ] = value;
						}
					}
				} );

				$selects.each( function() {
					var $this = $( this ),
						value = $this.val(),
						title = wpk.form.format_title( $this.data( 'originaltitle' ) );
					if ( 'undefined' === typeof title ) {
						title = wpk.form.format_title( $this.data( 'title' ) );
					}
					if ( 'undefined' !== typeof title ) {
						if ( value.length ) {
							result[ title ] = value;
						}
					}
				} );

				$selectable.each( function() {
					var $this = $( this ),
						value = 'selected',
						title = wpk.form.format_title( $this.data( 'originaltitle' ) );
					if ( 'undefined' === typeof title ) {
						title = wpk.form.format_title( $this.data( 'title' ) );
					}
					if ( 'undefined' !== typeof title ) {
						if ( $this.hasClass( 'checked' ) ) {
							if ( $this.find( '.icon_quantity' ).length > 0 ) {
								value = $this.find( '.icon_quantity' ).text();
							}
							result[ title ] = value;
						}
					}
				} );

				return result;

			},

			send_data:     function() {
				var form_id = $( '#estimation_popup' ).data( 'form' ),
					action  = wpk.form.get_form_type( form_id ),
					values  = wpk.form.get_input_values();
				values.price = $( '.progress-bar-price span' ).text();


				$.ajax( {
					url:      wpk_i698932.ajax_url,
					dataType: 'json',
					method:   'POST',
					data:     {
						action:  action,
						values:  values,
						form_id: form_id,
					},
					success:  function( data ) {
						console.log( data );
					}
				} );
			},
			get_form_type: function( form_id ) {
				var forms = wpk_i698932.confirmation_forms;
				for ( var i in forms ) {

					if ( forms[ i ] == form_id ) {
						return 'wpk_send';
					}
				}
				return 'wpk_invoice';
			}
		},


	};*/

	$( document ).ready( function() {
		window.wpk = new Wpk();
	} );


})( jQuery );