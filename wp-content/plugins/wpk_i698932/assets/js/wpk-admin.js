(function( $ ) {
	jQuery.extend( jQuery.fn.dataTableExt.oSort, {
		"date-eu-pre": function( date ) {
			date = date.replace( " ", "" );
			if ( !date ) {
				return 0;
			}
			var year;
			var eu_date = date.split( /[\.\-\/]/ );
			/*year (optional)*/
			if ( eu_date[ 2 ] ) {
				year = eu_date[ 2 ];
			}
			else {
				year = 0;
			}
			/*month*/
			var month = eu_date[ 1 ];
			if ( month.length == 1 ) {
				month = 0 + month;
			}

			/*day*/
			var day = eu_date[ 0 ];
			if ( day.length == 1 ) {
				day = 0 + day;
			}

			return (year + month + day) * 1;
		},

		"date-eu-asc": function( a, b ) {
			return ((a < b) ? -1 : ((a > b) ? 1 : 0));
		},

		"date-eu-desc": function( a, b ) {
			return ((a < b) ? 1 : ((a > b) ? -1 : 0));
		}
	} );

	function number_format( number, decimals, dec_point, thousands_sep ) {

		var n          = !isFinite( +number ) ? 0 : +number,
			prec       = !isFinite( +decimals ) ? 0 : Math.abs( decimals ),
			sep        = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
			dec        = (typeof dec_point === 'undefined') ? '.' : dec_point,
			toFixedFix = function( n, prec ) {
				// Fix for IE parseFloat(0.55).toFixed(0) = 0;
				var k = Math.pow( 10, prec );
				return Math.round( n * k ) / k;
			},
			s          = (prec ? toFixedFix( n, prec ) : Math.round( n )).toString().split( '.' );
		if ( s[ 0 ].length > 3 ) {
			s[ 0 ] = s[ 0 ].replace( /\B(?=(?:\d{3})+(?!\d))/g, sep );
		}
		if ( (s[ 1 ] || '').length < prec ) {
			s[ 1 ] = s[ 1 ] || '';
			s[ 1 ] += new Array( prec - s[ 1 ].length + 1 ).join( '0' );
		}
		return s.join( dec );
	}

	function format_date( date ) {

		return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`

	}

	$( document ).ready( function() {

		$.fn.dataTable.ext.search.push(
			function( settings, data, dataIndex ) {

				if ( $( '#show_all' ).is( ':checked' ) ) {
					return true;
				}

				var date  = data.slice( -1 ).pop(),
					today = new Date(),
					debug = new Date( parseInt( date ) );

				if ( parseInt( date ) <= today.getTime() && format_date( debug ) != format_date( today ) ) {
					return false;
				}

				return true;
			}
		);


		var schedules_table = $( '.wpk-table' ).not( '.wpk-invoice-table' ).not( '.wpk-all-events-table' ).DataTable( {
			responsive: true,
			lengthMenu: [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
			columnDefs: [
				{
					targets: [ $( '.wpk-table' ).find( 'th' ).length - 1 ],
					visible: false,
				}
			]
		} );


		var all_events_table = $( '.wpk-all-events-table' ).DataTable( {
			responsive:     true,
			columnDefs:     [
				{
					targets: [ $( '.wpk-all-events-table' ).find( 'th' ).length - 1 ],
					visible: false,
				},
				{
					type: 'date-eu', targets: 2,
				}
			],
			order:          [ [ 2, 'asc' ] ],
			lengthMenu:     [ [ 25, 50, 100, -1 ], [ 25, 50, 100, "All" ] ],
			iDisplayLength: -1
		} );
		all_events_table.columns().every( function( i ) {
			var that   = this,
				$input = $( 'input', this.footer() );
			if ( 'undefined' !== typeof $input ) {
				$input.on( 'keyup change', function() {
					if ( that.search() !== this.value ) {
						that.search( this.value ).draw();
					}
				} );
			}
		} );
		$( document ).on( 'change', '#show_all', function() {

			schedules_table.draw();
			all_events_table.draw();
		} );
		all_events_table.on( 'draw.dt', calculateSums );
		all_events_table.on( 'init.dt', calculateSums );

		var invoiceTable = $( '.wpk-invoice-table' ).not( '.wpk-all-events-table' );
		var _invoiceTable;
		try {
			_invoiceTable = invoiceTable.DataTable( {
				responsive: true,
				columnDefs: [ {
					type: 'date-eu', targets: 2,
				} ],
				order:      [ [ 2, 'asc' ] ],
				lengthMenu: [ [ 25, 50, 100, -1 ], [ 25, 50, 100, "All" ] ]
			} );
			_invoiceTable.columns().every( function( i ) {
				var that   = this,
					$input = $( 'input', this.footer() );
				if ( 'undefined' !== typeof $input ) {
					$input.on( 'keyup change', function() {
						if ( that.search() !== this.value ) {
							that.search( this.value ).draw();
						}
					} );
				}
			} );
		} catch ( e ) {
			console.log( e );
		}

		var formatPrice = function( price ) {

			price = price.replace( '.-CHF', '' );
			var after_dot = price.substr( price.indexOf( "." ) + 1 );
			if ( after_dot.replace( ' ', '' ).length > 2 ) {
				price = price.replace( '.', '' );
			}

			price = price.replace( ',', '.' );
			return price;

		};

		function calculateSums() {
			var total = 0,
				vat   = 0,
				gross = 0;
			$( 'td.gross-price:visible' ).each( function() {
				var text = $( this ).text();
				if ( text !== '' ) {
					gross = gross + parseFloat( formatPrice( text ) );
				}

			} );
			$( 'td.vat:visible' ).each( function() {
				var text = $( this ).text();
				if ( text !== '' ) {
					vat = vat + parseFloat( formatPrice( text ) );
				}
			} );
			$( 'td.total-price:visible' ).each( function() {
				var text = $( this ).text();
				if ( text !== '' ) {
					total = total + parseFloat( formatPrice( text ) );
				}
			} );


			$( '.total-sum' ).html( '<strong>Total TTC:</strong>' + number_format( total.toFixed( 2 ), 1, '\'', ',' ) + '.-CHF' );
			$( '.vat-sum' ).html( '<strong>Total TVA:</strong>' + number_format( total.toFixed( 2 ), 2, '\'', ',' ) + '.-CHF' );
			$( '.gross-sum' ).html( '<strong>Total HT:</strong>' + number_format( total.toFixed( 2 ), 2, '\'', ',' ) + '.-CHF' );

		};


		invoiceTable.on( 'draw.dt', calculateSums );
		invoiceTable.on( 'init.dt', calculateSums );

		calculateSums();


		$( document ).on( 'click', '.wpk-paid', function() {
			var $this = $( this );
			$.ajax( {
				url:        wpk_admin.ajax_url,
				method:     'POST',
				dataType:   'JSON',
				data:       {
					action: 'wpk_paid_status',
					id:     $this.data( 'id' ),
				},
				beforeSend: function( xhr ) {
					$this.attr( 'disabled', true );
				},
				success:    function( data ) {
					$this.attr( 'disabled', false );
					$this.text( data );

				}
			} );
		} );

		$( document ).on( 'click', '.wpk-cash', function( e ) {
			e.preventDefault();
			var $this = $( this );
			$.ajax( {
				url:        wpk_admin.ajax_url,
				method:     'POST',
				dataType:   'JSON',
				data:       {
					action: 'wpk_cash',
					id:     $this.data( 'id' ),
				},
				beforeSend: function( xhr ) {
					$this.attr( 'disabled', true );
				},
				success:    function( data ) {
					$this.attr( 'disabled', false );
					$this.text( data );

				}
			} );
		} );

		$( document ).on( 'click', '.wpk-send-invoice', function( e ) {
			var $this   = $( this ),
				defText = $this.text();
			e.preventDefault();
			$.ajax(
				{
					url:        wpk_admin.ajax_url,
					method:     'POST',
					dataType:   'JSON',
					data:       {
						action: 'wpk_send_invoice',
						id:     $this.data( 'id' ),
					},
					beforeSend: function( xhr ) {
						$this.attr( 'disabled', true );
						$this.text( 'Please wait...' );

					},
					success:    function( data ) {
						$this.attr( 'disabled', false );
						$this.text( defText );
						if ( data ) {
							if ( data === 'Sent' ) {
								$( '.wpk-invoice-status' ).text( data );
							}
							alert( data );
						}

					}

				}
			);

		} );

		$( document ).on( 'click', '.wpk-send-reminder', function( e ) {
			var $this = $( this );
			e.preventDefault();
			$.ajax(
				{
					url:        wpk_admin.ajax_url,
					method:     'POST',
					dataType:   'JSON',
					data:       {
						action: 'wpk_send_reminder',
						id:     $this.data( 'id' ),
					},
					beforeSend: function( xhr ) {
						$this.attr( 'disabled', true );

					},
					success:    function( data ) {
						console.log( data );
						$this.attr( 'disabled', false );
						if ( data ) {
							alert( data.alert );
							$this.text( data.message );
						}

					}

				}
			);

		} );
	} );
})( jQuery );