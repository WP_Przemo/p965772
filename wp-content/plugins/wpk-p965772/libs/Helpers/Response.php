<?php


namespace Wpk\p965772\Helpers;

use Wpk\p965772\Utility;

/**
 * Used to standarize request response.
 *
 * @author Przemysław Żydek
 */
class Response {

    /** @var int */
    const SUCCESS = 0;

    /** @var int */
    const NOTICE = 1;

    /** @var int */
    const MESSAGE = 2;

    /** @var int */
    const ERROR = 3;

    /** @var array */
    const TYPES = [ 'success', 'notice', 'message', 'error' ];

    /** @var bool True if there was error with submission (empty required fields etc.) */
    private $error = false;

    /** @var array Array of arrays with messages and their target (html element, popup or alert) */
    private $messages = [];

    /** @var array Contains additional data, useful for debuging. */
    private $additional = [];

    /** @var string Additional url that user will be redirected after ajax call */
    private $redirectUrl = '';

    /** @var mixed Result that can be used for something in frontend */
    private $result = false;

    /**
     * Add error to response
     *
     * @param string $message Error message
     * @param string $target Message target (html element ID, popup or alert)
     * @param bool $send Whenever send response after adding error.
     *
     * @return self
     */
    public function addError( string $message, string $target = 'alert', bool $send = false ): self {

        $this->error      = true;
        $this->messages[] = [
            'message' => $message,
            'target'  => $target,
            'type'    => self::ERROR,
        ];

        if ( $send ) {
            $this->sendJson();
        }

        return $this;

    }

    /**
     * @param mixed $result
     *
     * @return self
     */
    public function setResult( $result ): self {

        $this->result = $result;

        return $this;

    }

    /**
     * Add message to response
     *
     * @param string $message Error message
     * @param string $target Message target (html element ID, popup, alert or submit)
     * @param int $type Message type (error|notice|message|success)
     *
     * @return self
     */
    public function addMessage( $message, $target = 'alert', int $type = self::MESSAGE ) {

        $this->messages[] = [
            'message' => $message,
            'target'  => $target,
            'type'    => $type,
        ];

        return $this;

    }

    /**
     * Add additional data to response
     *
     * @param mixed $key
     * @param mixed $data
     *
     * @return self
     */
    public function addAdditionalData( $key, $data ): self {

        $this->additional[ $key ] = $data;

        return $this;

    }

    /**
     * Add messages from WP_Error to our response
     *
     * @param \WP_Error $error
     *
     * @return self
     */
    public function handleWpError( \WP_Error $error ): self {

        foreach ( $error->get_error_messages() as $error_message ) {

            if ( Utility::contains( $error_message, 'login' ) || Utility::contains( $error_message, 'sername' ) ) {
                $target = '#login';
            } elseif ( Utility::contains( $error_message, 'mail' ) ) {
                $target = '#email';
            } else {
                $target = 'popup-error';
            }
            $this->addError( $error_message, $target );
        }

        return $this;

    }

    /**
     * Checks if we have errors in our response
     *
     * @return bool
     */
    public function hasErrors(): bool {

        return $this->error;

    }

    /**
     * Set redirect url
     *
     * @param string $url
     *
     * @return self
     */
    public function setRedirectUrl( string $url ): self {

        $this->redirectUrl = $url;

        return $this;

    }

    /**
     * Send json with our response
     *
     * @return void
     */
    public function sendJson() {

        //Prase integers to actualy message type
        array_walk( $this->messages, function ( array &$message ) {
            $message[ 'type' ] = self::TYPES[ $message[ 'type' ] ];
        } );

        $result = [
            'messages'     => $this->messages,
            'error'        => $this->error,
            'additional'   => $this->additional,
            'redirect_url' => $this->redirectUrl,
            'result'       => $this->result,
        ];

        wp_send_json( $result );

    }

    /**
     * Helper function for checking ajax nonce
     *
     * @param string $action
     * @param string $queryArg
     * @param string $errorTarget
     *
     * @return void
     */
    public function checkNonce( $action, $queryArg, $errorTarget = 'alert' ) {

        if ( ! check_ajax_referer( $action, $queryArg, false ) ) {
            $this->addError( __( 'Security error', 'wpk' ), $errorTarget, true );
        }

    }


}