<?php


namespace Wpk\p965772\Controllers;

use Wpk\p965772\Helpers\Response;
use Wpk\p965772\Traits\Request;

/**
 * @author Przemysław Żydek
 */
abstract class Controller {

    use Request;

    /**
     * @var array Array with middleware classes to use
     */
    protected $middleware = [];

    /** @var Response */
    protected $response;

    /**
     * Controller constructor.
     */
    public function __construct() {
        $this->response = new Response();

        $this->loadMiddleware();
    }

    /**
     * Perform load of middleware modules
     *
     * @return void
     */
    protected function loadMiddleware() {

        foreach ( $this->middleware as $key => $middleware ) {
            $this->middleware[ $key ] = new $middleware();
        }

    }

    /**
     * Get controller middleware
     *
     * @param string $middleware
     *
     * @return bool|mixed
     */
    protected function middleware( $middleware ) {
        return isset( $this->middleware[ $middleware ] ) ? $this->middleware[ $middleware ] : false;
    }

}