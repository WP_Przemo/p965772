<?php


namespace Wpk\p965772\Controllers\Invoice;

use Dompdf\Dompdf;
use Dompdf\Options;
use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Helpers\Response;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Settings;
use Wpk\p965772\Utility;

use function Wpk\p965772\Core as CoreFc;

/**
 * Handles invoice PDF
 *
 * @author Przemysław Żydek
 */
class Pdf extends Controller {

    /**
     * Pdf constructor.
     */
    public function __construct() {

        parent::__construct();

        add_action( 'admin_init', [ $this, 'downloadPdf' ] );
        add_action( 'wpk/p965772/sendInvoice', [ $this, 'sendInvoice' ] );
        add_action( 'wp_ajax_wpk_send_invoice', [ $this, 'sendInvoiceAjax' ] );

    }

    /**
     * @return void
     */
    public function sendInvoiceAjax() {

        $invoiceID = (int) $this->getPostParam( 'invoice_id' );
        $response  = new Response();

        if ( empty( $invoiceID ) ) {
            $response->addError( __( 'No invoice provided', 'wpk' ), 'alert', true );
        }

        $response->checkNonce( 'wpk_send_invoice', 'wpk_nonce' );

        $invoice = Invoice::find( $invoiceID );
        $result  = $this->sendInvoice( $invoice );

        if ( $result ) {
            $response->addMessage( __( 'Invoice sent!', 'wpk' ), 'alert', Response::SUCCESS );
        }

        $response->setResult( $result )->sendJson();

    }

    /**
     * Send invoice to customer email
     *
     * @param Invoice $invoice
     *
     * @return bool
     */
    public function sendInvoice( Invoice $invoice ) {

        //Output PDF
        $pdf = $this->getPdf( $invoice, true );
        $pdf->render();
        $output = $pdf->output();

        //Store pdf in file
        $path = sprintf( '%s/pdfs/%s.pdf', CoreFc()->dir, $invoice->meta( 'ref' ) );
        @file_put_contents( $path, $output );

        $lang = $invoice->getLang();

        $message = $invoice->formatText( Settings::getSetting( "invoice_mail_text_{$lang}" ) );
        $subject = sprintf( __( 'Invoice for quote %s', 'wpk' ), $invoice->meta( 'ref' ) );

        $headers    = Utility::getMailHeaders();
        $adminEmail = Settings::getSetting( 'admin_e-mail' );

        if ( ! empty( $adminEmail ) ) {
            $headers[] = "cc: $adminEmail";
        }

        $result = wp_mail( $invoice->meta( 'email' ), $subject, $message, $headers, $path );

        if ( $result ) {
            //Delete pdf file after e-mail have been sent
            unlink( $path );

            $invoice->updateMeta( 'email_status', 'sent' );
        }

        /**
         * @param Invoice $invoice
         */
        do_action( 'wpk/p965772/invoiceSent', $invoice );

        return $result;

    }

    /**
     * @return void
     */
    public function downloadPdf() {

        if ( $this->getQueryParam( 'action' ) === 'wpk_download_invoice' ) {

            $invoiceID = (int) $this->getQueryParam( 'invoice' );

            if ( empty( $invoiceID ) ) {
                return;
            }

            $invoice = Invoice::find( $invoiceID );
            $pdf     = $this->getPdf( $invoice );
            $name    = "{$invoice->meta('ref')}.pdf";

            $pdf->render();
            $pdf->stream( $name, [ 'Attachment' => 0 ] );

        }

    }

    /**
     * Generate pdf for invoice
     *
     * @param Invoice $invoice
     * @param bool $generateID
     *
     * @return Dompdf
     */
    public function getPdf( Invoice $invoice, $generateID = false ) {

        $html = $this->getPdfHtml( $invoice, $generateID );

        $options = new Options();
        //Enable access to remote images
        $options->setIsRemoteEnabled( true );

        $pdf = new Dompdf( $options );

        $pdf->loadHtml( $html );

        return $pdf;

    }

    /**
     * Generate HTML content for pdf
     *
     * TODO Html adjustments
     *
     * @param Invoice $invoice
     * @param bool $generateID
     *
     * @return string
     */
    protected function getPdfHtml( Invoice $invoice, $generateID ) {

        $vat    = $invoice->meta( 'vat' );
        $ref    = $invoice->meta( 'ref' );
        $prices = $invoice->prices( true );

        //Parse invoice address
        $invoiceHeader = trim( $invoice->meta( 'invoice_header' ) );
        $invoiceHeader = wp_kses( $invoiceHeader, [] );
        $invoiceHeader = wpautop( $invoiceHeader );

        //Parse invoice date
        $invoiceDate = $invoice->date();

        $eventDate = $invoice->eventDate();

        $headerLogo = Settings::getSetting( 'header_logo' );

        $invoiceID = $invoice->invoiceID();

        $details = explode( ',', $invoice->meta( 'details' ) );

        $vatNumber = Settings::getSetting( 'vat_number' );

        if ( $generateID && empty( $invoiceID ) ) {
            $invoice->generateID();

            $invoiceID = $invoice->invoiceID();
        }

        $headerContent = $invoice->formatText( Settings::getSetting( 'header_text_content' ) );
        $footer        = $invoice->formatText( Settings::getSetting( 'footer_content' ) );

        $data = [
            'vat'           => $vat,
            'ref'           => $ref,
            'prices'        => $prices,
            'invoiceHeader' => $invoiceHeader,
            'invoiceID'     => $invoiceID,
            'invoiceDate'   => $invoiceDate,
            'headerLogo'    => isset( $headerLogo[ 'sizes' ][ 'medium' ] ) ? $headerLogo[ 'sizes' ][ 'medium' ] : '',
            'details'       => $details,
            'footer'        => $footer,
            'eventDate'     => $eventDate,
            'headerContent' => $headerContent,
            'vatNumber'     => $vatNumber,
        ];

        return CoreFc()->view->render( 'invoice.pdf', $data );

    }

}