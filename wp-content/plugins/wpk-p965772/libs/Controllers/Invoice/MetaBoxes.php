<?php


namespace Wpk\p965772\Controllers\Invoice;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Core;
use Wpk\p965772\Models\Invoice;

use function Wpk\p965772\Core as CoreFc;

/**
 * Adds new metaboxes for invoices
 *
 * @author Przemysław Żydek
 */
class MetaBoxes extends Controller {

    /**
     * MetaBoxes constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'add_meta_boxes', [ $this, 'add' ] );
    }

    /**
     * @return void
     */
    public function add() {

        add_meta_box( 'invoice_statuses', 'Invoice statuses', [ $this, 'invoiceStatuses' ], Core::INVOICE_SLUG );
        add_meta_box( 'summary', 'Summary', [ $this, 'summary' ], Core::INVOICE_SLUG );
        add_meta_box( 'actions', 'Invoice actions', [ $this, 'actions' ], Core::INVOICE_SLUG, 'side' );
        add_meta_box( 'confirmation_summary', 'Confirmation summary', [
            $this,
            'confirmationSummary',
        ], Core::INVOICE_SLUG );

    }

    /**
     * @param int $postID
     *
     * @return void
     */
    public function invoiceStatuses( $postID ) {

        $invoice = Invoice::find( $postID );

        $invoiceID = $invoice->invoiceID();
        $prices    = $invoice->prices( true );
        $company   = $invoice->meta( 'company' );


        $data = [
            'statuses' => [
                'quote_status'  => [
                    'label' => 'Quote status',
                    'value' => str_replace( '_', ' ', ucfirst( $invoice->meta( 'quote_status' ) ) ),
                ],
                'invoice_id'    => [
                    'label' => 'Invoice ID',
                    'value' => empty( $invoiceID ) ? __( 'Not generated yet', 'wpk' ) : $invoiceID,
                ],
                'proffessional' => [
                    'label' => 'Proffessional of events',
                    'value' => ucfirst( strtolower( $invoice->meta( 'event_proffessional' ) ) ),
                ],
                'company'       => [
                    'label' => 'Company',
                    'value' => empty( $company ) ? 'No' : $company,
                ],
                'net'           => [
                    'label' => 'Montant HT',
                    'value' => $prices[ 'price' ],
                ],
                'vat'           => [
                    'label' => 'Montant TVA',
                    'value' => $prices[ 'vat' ],
                ],
                'total'         => [
                    'label' => 'Montant TTC',
                    'value' => $prices[ 'total' ],
                ],
                'language'      => [
                    'label' => 'Lang',
                    'value' => ucfirst( $invoice->getLang() ),
                ],
            ],
        ];

        echo CoreFc()->view->render( 'invoice.admin.statuses', $data );

    }

    /**
     * @param int $postID
     *
     * @return void
     */
    public function summary( $postID ) {
        echo Invoice::find( $postID )->meta( 'summary' );
    }

    /**
     * @param int $postID
     *
     * @return void
     */
    public function confirmationSummary( $postID ) {
        echo Invoice::find( $postID )->meta( 'confirmation_summary' );
    }

    /**
     * @param int $postID
     *
     * @return void
     */
    public function actions( $postID ) {

        $invoice = Invoice::find( $postID );

        $data = [
            'id'     => $invoice->ID,
            'pdfUrl' => $invoice->pdfLink(),
        ];

        echo CoreFc()->view->render( 'invoice.admin.actions', $data );

    }

}