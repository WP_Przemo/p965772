<?php


namespace Wpk\p965772\Controllers\Invoice;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Core;
use Wpk\p965772\Helpers\Response;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Settings;
use Wpk\p965772\Utility;

/**
 * Handle invoice actions
 */
class Actions extends Controller {

    /**
     * Actions constructor.
     */
    public function __construct() {

        parent::__construct();

        add_action( 'wpk/p965772/invoiceConfirmed', [ $this, 'supressSameQuotes' ] );
        add_action( 'wpk/p965772/invoiceConfirmed', [ $this, 'sendMailToWorkers' ] );

        add_action( 'wpk/p965772/sendPaymentReminder', [ $this, 'sendPaymentReminder' ] );

        add_action( 'wp_ajax_wpk_invoice_action', [ $this, 'handleTableActions' ] );

        add_action( 'before_delete_post', [ $this, 'regenerateInvoiceIDs' ] );;

    }

    /**
     * On confirmation mark as standby invoices with the same event date and email
     *
     * @param Invoice $invoice
     *
     * @return void
     */
    public function supressSameQuotes( Invoice $invoice ) {

        //Find invoices with the same event date and email, and mark them as standby
        $sameInvoices = Invoice::init()
                               ->hasMetaRelation( [
                                   'event_date'   => $invoice->meta( 'event_date' ),
                                   'email'        => $invoice->meta( 'email' ),
                                   'quote_status' => 'not_confirmed',
                               ], 'AND' )
                               ->get();


        /** @var Invoice $item */
        foreach ( $sameInvoices->all() as $item ) {
            $item->updateMeta( 'quote_status', 'standby' );

            do_action( 'wpk/p965772/invoiceStandBy', $item );
        }

    }

    /**
     * @return void
     */
    public function handleTableActions() {

        $response = new Response();

        $action    = $this->getPostParam( 'invoice_action' );
        $invoiceID = (int) $this->getPostParam( 'invoice_id' );

        if ( empty( $action ) || empty( $invoiceID ) ) {
            $response->addError( __( 'Invalid params', 'wpk' ), 'alert', true );
        }

        $response->checkNonce( 'wpk_table', 'wpk_nonce' );

        $invoice = Invoice::find( $invoiceID );

        switch ( $action ) {

            case 'payment_status':

                $currentStatus = $invoice->meta( 'payment_status' ) === 'paid' ? 'not_paid' : 'paid';
                $invoice->updateMeta( 'payment_status', $currentStatus );

                $response->setResult( Invoice::formatMeta( $currentStatus ) );

                break;

            case 'payment_type':

                $paymentType = $invoice->meta( 'payment_type' ) === '---' ? 'cash' : '---';
                $invoice->updateMeta( 'payment_type', $paymentType );

                $response->setResult( Invoice::formatMeta( $paymentType ) );

                break;

            case 'payment_reminder':

                do_action( 'wpk/p965772/sendPaymentReminder', $invoice );

                $response->setResult(
                    sprintf( __( 'Reminder (%s)', 'wpk' ),
                        (int) $invoice->meta( 'payment_reminders_count' )
                    ) );

                break;


        }

        $response->sendJson();

    }

    /**
     * @param Invoice $invoice
     *
     * @return void
     */
    public function sendPaymentReminder( Invoice $invoice ) {

        $count = (int) $invoice->meta( 'payment_reminders_count' );

        $lang = $invoice->getLang();

        $subject = $invoice->formatText( Settings::getSetting( "payment_reminder_email_title_{$lang}" ) );
        $message = $invoice->formatText( Settings::getSetting( "payment_reminder_email_content_{$lang}" ) );

        $headers    = Utility::getMailHeaders();
        $adminEmail = Settings::getSetting( 'admin_email_payment' );

        if ( ! empty( $adminEmail ) ) {
            $headers[] = "cc: $adminEmail";
        }

        $result = wp_mail( $invoice->meta( 'email' ), $subject, $message, $headers );

        if ( $result ) {
            $count ++;

            $invoice->updateMeta( 'payment_reminders_count', $count );
        }

    }

    /**
     * After invoice removal regenerate IDs from invoices from the same day
     *
     * @param int $postID
     *
     * @return void
     */
    public function regenerateInvoiceIDs( $postID ) {

        if ( get_post_type( $postID ) !== Core::INVOICE_SLUG ) {
            return;
        }

        $invoice = Invoice::find( $postID );

        //Remove Easy Flash prefix from invoice ID
        $invoiceID = str_replace( 'F', '', $invoice->invoiceID() );

        if ( empty( $invoiceID ) ) {
            return;
        }

        $counters = get_option( 'wpk_invoice_references', [] );

        //Remove counter from ID, so that we get the date (for example 2018073106 -> 20180731)
        $counterDate              = str_split( $invoiceID, strlen( $invoiceID ) - 2 )[ 0 ];
        $counters[ $counterDate ] = 1;

        update_option( 'wpk_invoice_references', $counters );

        //Regenerate IDs
        $invoices = Invoice::init()
                           ->hasMeta( 'invoice_id' )
                           ->hasMetaValue( 'event_date', $invoice->meta( 'event_date' ) )
                           ->order( 'meta_value_num', 'ASC', 'invoice_id' )
                           ->get();

        /** @var Invoice $item */
        foreach ( $invoices->all() as $item ) {
            $item->deleteMeta( 'invoice_id' )->generateID();
        }

    }

    /**
     * On invoice confirmation send summary to workers without prices
     *
     * @param Invoice $invoice
     *
     * @return void
     */
    public function sendMailToWorkers( Invoice $invoice ) {

        $emails = Settings::getSetting( 'confirmation_emails', [] );

        if ( empty( $emails ) ) {
            return;
        }

        $summary = remove_accents( $invoice->meta( 'summary' ) );

        // Create a new DOMDocument and load the HTML
        $dom = new \DOMDocument( '1.0', 'UTF-8' );
        $dom->loadHTML( $summary );

        // Create a new XPath query
        $xpath = new \DOMXPath( $dom );

        $result = $xpath->query( '//span' );

        // Loop the results and remove them from the DOM
        /** @var \DOMElement $cell */
        foreach ( $result as $cell ) {

            $content = strtolower( $cell->nodeValue );
            if ( Utility::contains( $content, '.-chf' ) ) {
                $cell->nodeValue = ' ';
            }

        }

        // Save back to a string
        $newhtml = $dom->saveHTML();

        $subject = sprintf( __( 'Event %s', 'wpk' ), $invoice->meta( 'ref' ) );
        $headers = Utility::getMailHeaders();

        foreach ( $emails as $email ) {
            wp_mail( $email[ 'email' ], $subject, $newhtml, $headers );
        }


    }

}