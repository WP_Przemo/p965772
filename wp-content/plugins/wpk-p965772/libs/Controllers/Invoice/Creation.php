<?php


namespace Wpk\p965772\Controllers\Invoice;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Helpers\Response;
use Wpk\p965772\Settings;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Utility;

/**
 * Handles creation of new invoice
 *
 * @author Przemysław Żydek
 */
class Creation extends Controller {

    /** @var array Stores static field keys that will be used as keys for most important invoice values */
    const FIELDS = [
        'company'             => [ 'company', 'societe' ],
        'event_proffessional' => [ 'event_professional', 'professionnel_de_levenementiel' ],
        'event_date'          => [ 'date_of_event', 'date' ],
        'event_type'          => [ 'type_of_event', 'type_devenement' ],
    ];

    /** @var array Stores static field keys that will be used as keys for most important invoice confirmation values */
    const CONFIRMATION_FIELDS = [
        'name'           => [
            'companyname',
            'company__name',
            'societenom',
            'societe__nom',
            'nomsociete',
            'nom__societe',
        ],
        'invoice_header' => [ 'invoice_header', 'en_tete_de_facturation' ],
        'ref'            => [ 'quote_reference', 'reference_devis' ],
        'email'          => [ 'email', 'email_' ],
    ];

    /**
     * Creation constructor.
     */
    public function __construct() {

        parent::__construct();

        add_action( 'wpk/p965772/handleEstimationForm', [ $this, 'handleForm' ] );
        add_action( 'wp_ajax_wpk_validate_confirmation_form', [ $this, 'validateConfirmationForm' ] );

    }

    /**
     * Handle submission of WP_Cost_Estimation form
     *
     * @return void
     */
    public function handleForm() {

        $formID             = (int) $this->getPostParam( 'formID' );
        $confirmationForms  = Settings::getSetting( 'confirmation_forms' );
        $customizationForms = Settings::getSetting( 'customization_forms' );

        //No need to handle customzation form
        if ( in_array( $formID, $customizationForms ) ) {
            return;
        } else if ( in_array( $formID, $confirmationForms ) ) {

            $this->handleConfirmationForm();

        } else {

            $this->createInvoice( $formID );

        }

    }

    /**
     * @param int $formID
     *
     * @return void
     */
    protected function createInvoice( $formID ) {

        $invoice = new Invoice();
        $meta    = [];

        $ref        = $this->getRef( $formID );
        $formValues = $this->parseValues( $this->getPostParam( 'items', [] ) );

        $englishForms = explode( ',', Settings::getSetting( 'english_forms' ) );

        $meta[ 'total' ]          = (float) $this->getPostParam( 'total' );
        $meta[ 'phone' ]          = $this->getPostParam( 'phone' );
        $meta[ 'country' ]        = $this->getPostParam( 'country', '' );
        $meta[ 'city' ]           = $this->getPostParam( 'city', '' );
        $meta[ 'address' ]        = $this->getPostParam( 'address', '' );
        $meta[ 'zip' ]            = $this->getPostParam( 'zip', '' );
        $meta[ 'email' ]          = trim( $this->getPostParam( 'email', '' ) );
        $meta[ 'name' ]           = $this->getPostParam( 'firstName', '' );
        $meta[ 'summary' ]        = $this->getPostParam( 'summary' );
        $meta[ 'ref' ]            = $ref;
        $meta[ 'vat' ]            = (float) Settings::getSetting( 'vat_amount', 7.7 );
        $meta[ 'quote_status' ]   = 'not_confirmed';
        $meta[ 'email_status' ]   = 'not_sent';
        $meta[ 'payment_type' ]   = '---';
        $meta[ 'payment_status' ] = 'not_paid';
        $meta[ 'details' ]        = Settings::getSetting( 'default_invoice_details' );
        $meta[ 'lang' ]           = in_array( $formID, $englishForms ) ? 'en' : 'fr';

        $meta = array_merge( $meta, Utility::getAdditionalValues( self::FIELDS, $formValues ) );

        //Translate french month to english one (this will be useful later for DateTime :> )
        if ( isset( $meta[ 'event_date' ] ) ) {
            $meta[ 'event_date' ] = Invoice::translateMonths( $meta[ 'event_date' ] );
        }

        $meta[ 'event_date' ] = date( Invoice::META_DATE_FORMAT, strtotime( $meta[ 'event_date' ] ) );

        $invoice
            ->addMetas( $meta )
            ->title( sprintf( '%s invoice', $ref ) )
            ->status( 'pending' );

        do_action( 'wpk/p965772/beforeCreateInvoice', $invoice );

        $invoice = $invoice->create();

        /**
         * @param Invoice $invoice
         */
        do_action( 'wpk/p965772/newInvoice', $invoice );

    }

    /**
     * Get WP_Cost_Estimation field label from their IDs
     *
     * The values are stored in following associative array [label, itemid, value, step, stepid]
     *
     * @param array $values
     *
     * @return array
     */
    protected function parseValues( $values ) {

        $result = [];

        foreach ( $values as $value ) {

            if ( isset( $value[ 'value' ] ) ) {

                $label = self::formatLabel( $value[ 'label' ] );

                $result[ $label ] = $value[ 'value' ];
            }

        }

        return $result;

    }

    /**
     * @param string $label
     *
     * @return string
     */
    protected static function formatLabel( $label ) {

        $label = remove_accents( $label );
        $label = strtolower( $label );
        $label = str_replace( [ '\'', '/', ' ', '-', '’' ], [ '', '', '_', '_', '' ], $label );

        //Remove brackets ()
        $label = preg_replace( '/ *\([^)]*\) */m', '', $label );

        return $label;

    }

    /**
     * Get current reference number for provided form
     *
     * @param string $formID
     *
     * @return string
     */
    public function getRef( $formID ) {

        global $wpdb;

        $table = $wpdb->prefix . "wpefc_forms";
        $rows  = $wpdb->get_results( "SELECT * FROM $table WHERE id=$formID LIMIT 1" );
        $form  = $rows[ 0 ];
        $ref   = $form->current_ref;

        return $form->ref_root . $ref;

    }

    /**
     * @return void
     */
    public function validateConfirmationForm() {

        $response = new Response();

        $data = [];

        foreach ( $_POST as $key => $item ) {
            $key          = self::formatLabel( $key );
            $data[ $key ] = $item;
        }

        $values = Utility::getAdditionalValues( self::CONFIRMATION_FIELDS, $data );
        $values = filter_var_array( $values );

        /** @var Invoice $invoice */
        $invoice = Invoice::init()
                          ->hasMetaRelation( [
                              'ref'   => $values[ 'ref' ],
                              'email' => $values[ 'email' ],
                          ], 'AND' )
                          ->get()
                          ->first();

        if ( empty( $invoice ) ) {

            $message = Settings::getSetting( 'customization_form_error_message', esc_html__( 'Your reference or your email are wrong. Please check your information.', 'wpk' ) );

            $response->addError( $message, 'alert' );
        } else {

            if ( $invoice->meta( 'quote_status' ) === 'confirmed' ) {
                $response->addError( esc_html__( 'This quote is already confirmed.', 'wpk' ), 'alert' );
            } else {
                $response->setResult( true );
            }
        }

        $response->sendJson();

    }

    /**
     * @return void
     */
    protected function handleConfirmationForm() {

        $values = $this->parseValues( $this->getPostParam( 'items', [] ) );
        $values = Utility::getAdditionalValues( self::CONFIRMATION_FIELDS, $values );

        $email = $this->getPostParam( 'email' );

        $values = filter_var_array( $values );

        /** @var Invoice $invoice */
        $invoice = Invoice::init()
                          ->hasMetaRelation( [
                              'ref'   => $values[ 'ref' ],
                              'email' => $email,
                          ], 'AND' )
                          ->get()
                          ->first();

        if ( empty( $invoice ) ) {
            return;
        }

        $invoice
            ->updateMetas( [
                'quote_status'         => 'confirmed',
                'invoice_header'       => $values[ 'invoice_header' ],
                'name'                 => $values[ 'name' ],
                'confirmation_summary' => $this->getPostParam( 'summary' ),
            ] )
            ->status( 'publish' )
            ->update();

        /**
         * @param Invoice $invoice
         */
        do_action( 'wpk/p965772/invoiceConfirmed', $invoice );

    }

}

