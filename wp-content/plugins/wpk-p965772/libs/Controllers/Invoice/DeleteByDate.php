<?php


namespace Wpk\p965772\Controllers\Invoice;

use Wpk\p965772\Controllers\Controller;
use function Wpk\p965772\Core as CoreFc;
use Wpk\p965772\Core;
use Wpk\p965772\Helpers\Response;
use Wpk\p965772\Models\Invoice;

/**
 * @author Przemysław Żydek
 */
class DeleteByDate extends Controller {

    /** @var string */
    const MENU_SLUG = 'wpk_delete_by_date';

    /**
     * DeleteByDate constructor.
     */
    public function __construct() {

        parent::__construct();

        add_action( 'admin_menu', function () {
            add_submenu_page(
                Core::MENU_SLUG,
                __( 'Delete invoices by date', 'wpk' ),
                __( 'Delete invoices by date', 'wpk' ),
                'manage_options',
                self::MENU_SLUG,
                [ $this, 'menu' ]
            );
        }, 10000 );

        add_action( 'wp_ajax_wpk_delete_by_date', [ $this, 'handleForm' ] );

    }

    /**
     * @return void
     */
    public function handleForm() {

        $date     = $this->getPostParam( 'date' );
        $response = new Response();

        if ( empty( $date ) ) {
            $response->addError( __( 'No date provided', 'wpk' ), 'alert', true );
        }

        $response->checkNonce( 'wpk_delete_by_date', 'wpk_nonce' );


        $date     = ( new \DateTime( $date ) )->format( Invoice::ID_DATE_FORMAT );
        $counters = get_option( 'wpk_invoice_references', [] );
        $invoices = Invoice::init()->hasMetaValue( 'invoice_id', $date, 'LIKE' )->get();

        /** @var Invoice $invoice */
        foreach ( $invoices->all() as $invoice ) {
            $invoice->deleteMeta( 'invoice_id' );
        }

        unset( $counters[ $date ] );

        update_option( 'wpk_invoice_references', $counters );

        $response
            ->addMessage( sprintf( __( 'Reseted ID for %s invoices', 'wpk' ), $invoices->count() ), 'alert' )
            ->setResult( true )
            ->sendJson();

    }

    /**
     * @return void
     */
    public function menu() {
        echo CoreFc()->view->render( 'invoice.admin.delete-by-date-form' );
    }

}