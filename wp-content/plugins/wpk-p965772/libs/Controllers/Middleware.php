<?php

namespace Wpk\p965772\Controllers;

use Wpk\p965772\Traits\Request;

/**
 * @author Przemysław Żydek
 */
abstract class Middleware {

    use Request;

}