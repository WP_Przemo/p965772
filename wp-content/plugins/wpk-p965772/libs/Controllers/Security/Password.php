<?php


namespace Wpk\p965772\Controllers\Security;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Core;
use Wpk\p965772\Helpers\Response;
use Wpk\p965772\Models\User;

use function Wpk\p965772\Core as Corefc;

/**
 * @author Przemysław Żydek
 */
class Password extends Controller {

    /** @var string */
    const OPTION_SLUG = 'wpk_password_new';

    /** @var string Stores password */
    private static $password;

    /**
     * Password constructor.
     */
    public function __construct() {

        parent::__construct();

        add_action( 'init', function () {
            self::$password = get_option( self::OPTION_SLUG, wp_hash_password( '0007' ) );
        } );

        add_action( 'admin_menu', function () {
            add_submenu_page(
                Core::MENU_SLUG,
                esc_html__( 'WPK Password', 'wpk' ),
                esc_html__( 'WPK Password', 'wpk' ),
                'manage_options',
                'wpk_password',
                [ $this, 'menu' ]
            );
        }, 1000 );

        add_action( 'wp_ajax_wpk_check_password', [ $this, 'savePassword' ] );

    }

    /**
     * @param string $newPassword
     *
     * @return  void
     */
    private function changePassword( $newPassword ) {
        self::$password = wp_hash_password( $newPassword );

        update_option( self::OPTION_SLUG, self::$password );
    }

    /**
     * @return void
     */
    public function savePassword() {

        $password = $this->getPostParam( 'wpk_password' );
        $response = new Response();

        $response->checkNonce( 'wpk_security_nonce', 'wpk_nonce' );

        if ( self::isAuthenticated() ) {

            $newPassword = $this->getPostParam( 'wpk_new_password' );

            if ( ! empty( $newPassword ) ) {

                self::changePassword( $newPassword );

                $response->addMessage( __( 'Password changed!', 'wpk' ), 'alert' )->setResult( true );

            }

        } else {

            if ( self::isValid( $password ) ) {
                self::auth();

                $response->setResult( true )->setRedirectUrl( true );
            } else {
                $response->addError( __( 'Invalid password', 'wpk' ), 'alert', true );
            }
        }

        $response->sendJson();

    }

    /**
     * @return void
     */
    public function menu() {

        $data = [
            'auth' => Password::isAuthenticated(),
            'ref'  => htmlentities( $this->getQueryParam( 'ref' ) ),
        ];

        echo Corefc()->view->render( 'security.password-form', $data );
    }

    /**
     * @param string $password
     *
     * @return bool
     */
    private static function isValid( $password ) {
        return wp_check_password( $password, self::$password );
    }

    /**
     * @return bool
     */
    public static function isAuthenticated() {

        if ( ! isset( $_COOKIE[ 'wpk_token' ] ) ) {
            return false;
        }

        $cookieToken = $_COOKIE[ 'wpk_token' ];
        $userToken   = User::current()->meta( 'wpk_token' );

        return $cookieToken === $userToken;


    }

    /**
     * @return void
     */
    private static function auth() {

        $user  = User::current();
        $token = wp_generate_password( 20 );

        $user->updateMeta( 'wpk_token', $token );

        setcookie( 'wpk_token', $token, time() + 86400, ADMIN_COOKIE_PATH, COOKIE_DOMAIN );
    }

    /**
     * @return void
     */
    private static function clearToken() {
        setcookie( 'wpk_token', '', time() - 1, ADMIN_COOKIE_PATH, COOKIE_DOMAIN );
    }


}