<?php


namespace Wpk\p965772\Controllers\Security;

use Wpk\p965772\Controllers\Invoice\DeleteByDate;
use Wpk\p965772\Core;
use Wpk\p965772\Traits\Request;
use Wpk\p965772\Utility;

/**
 * Hides certain view elements based on security params
 *
 * @author Przemysław Żydek
 */
class View {

    use Request;

    /** @var array */
    const POST_TYPES = [ Core::INVOICE_SLUG, Core::SCHEDULE_SLUG ];

    /** @var array */
    const MENUS = [ Core::MENU_SLUG, DeleteByDate::MENU_SLUG ];

    /**
     * View constructor.
     */
    public function __construct() {
        add_action( 'pre_get_posts', [ $this, 'hidePosts' ] );

        add_action( 'admin_init', [ $this, 'hidePageContent' ] );
    }

    /**
     * @return string
     */
    protected function getRedirectUrl() {

        return add_query_arg( [
            'page' => 'wpk_password',
            'ref'  => Utility::getCurrentUrl(),
        ], admin_url( 'admin.php' ) );

    }

    /**
     * @param \WP_Query $query
     *
     * @return void
     */
    public function hidePosts( \WP_Query $query ) {

        if ( is_admin() && in_array( $query->get( 'post_type' ), self::POST_TYPES ) && ! Password::isAuthenticated() ) {

            wp_redirect( self::getRedirectUrl() );
            wp_die();

        }

    }

    /**
     * @return void
     */
    public function hidePageContent() {

        $postID = (int) $this->getQueryParam( 'post' );
        $page   = $this->getQueryParam( 'page' );

        if ( ( in_array( get_post_type( $postID ), self::POST_TYPES ) ||
               in_array( $page, self::MENUS ) ) &&
             ! Password::isAuthenticated() ) {
            wp_redirect( self::getRedirectUrl() );
        }

    }

}