<?php


namespace Wpk\p965772\Controllers\Migrate;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Core;

use function Wpk\p965772\Core as CoreFc;

/**
 * @author Przemysław Żydek
 */
class Menu extends Controller {

    /**
     * Menu constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'admin_menu', function () {
            add_submenu_page(
                Core::MENU_SLUG,
                esc_html__( 'Migrate', 'wpk' ),
                esc_html__( 'Migrate', 'wpk' ),
                'manage_options',
                'wpk_migrate',
                [ $this, 'render' ]
            );
        }, 100 );

    }

    /**
     * @return void
     */
    public function render() {

        $data = [];

        echo CoreFc()->view->render( 'migrate.menu', $data );

    }

}