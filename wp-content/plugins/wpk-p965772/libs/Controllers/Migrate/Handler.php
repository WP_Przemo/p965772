<?php


namespace Wpk\p965772\Controllers\Migrate;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Controllers\Invoice\Creation;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Models\Schedule;

/**
 * Handles migrating stuff to new format
 *
 * @author Przemysław Żydek
 */
class Handler extends Controller {

    /** @var array Result of migration */
    private $result = [
        'migratedInvoices' => 0,
        'finished'         => false,
        'nonce'            => '',
        'operation'        => 'migrate_invoices',
        'progress'         => '0%',
        'message'          => '',
    ];

    /** @var string Hardcoded date, all invoice created before it are considered old and will be migrated */
    const DATE = '28.07.2018';

    /**
     * Invoices constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wp_ajax_wpk_migrate', [ $this, 'migrate' ] );
    }

    /**
     * @return void
     */
    public function migrate() {

        @ini_set( 'max_execution_time', 700 );

        /*	if ( get_option( 'wpk_migrated', false ) ) {
                $this->result[ 'finished' ] = true;
                $this->result[ 'message' ]  = esc_html__( 'Migration has been already finished. Aborting', 'wpk' );

                $this->response->setResult( $this->result )->sendJson();
            }*/

        $operation = $this->getPostParam( 'operation' );

        $this->response->checkNonce( 'wpk_migrate', 'nonce' );

        //Store current nonce in result
        $this->result[ 'nonce' ] = $this->getPostParam( 'nonce' );

        switch ( $operation ) {

            case 'migrate_invoices':
                $this->migrateInvoices();
                break;

            case 'migrate_schedules':
                $this->migrateSchedules();
                break;

        }

    }

    /**
     * @param string $label
     *
     * @return string
     */
    private function formatLabel( $label ) {

        $label = remove_accents( $label );
        $label = strtolower( $label );
        $label = str_replace( [ '\'', '/', ' ', '-', '’' ], [ '', '', '_', '_', '' ], $label );

        //Remove brackets ()
        $label = preg_replace( '/ *\([^)]*\) */m', '', $label );

        return $label;

    }

    /**
     * @param string $key
     * @param mixed $meta
     *
     * @return mixed
     */
    private function formatMeta( $key, $meta ) {

        switch ( $key ) {

            case 'total':
                return str_replace( [ '.-CHF', ',', '\'' ], [ '', '.', '' ], $meta );

            case 'quote_status':
            case 'payment_status':
            case 'email_status':
                return str_replace( ' ', '_', strtolower( $meta ) );

            default:
                return $meta;

        }

    }

    /**
     * @return void
     */
    private function migrateInvoices() {

        $migratedInvoices = (int) $this->getPostParam( 'migrated_invoices', 0 );

        $totalInvoices = Invoice::init( [
            'date_query' => [
                'before' => Handler::DATE,
            ],
        ] )->perPage( - 1 )
                                ->hasNotMeta( 'migrated' )
                                ->get()->count();


        //This model contains all old invoices that needs to be migrated
        $model = Invoice::init( [
            'date_query' => [
                'before' => Handler::DATE,
            ],
        ] )->perPage( 10 )->hasNotMeta( 'migrated' )->get();

        if ( $model->empty() ) {

            $this->result[ 'migratedInvoices' ] = $migratedInvoices + $model->count();
            $this->result[ 'message' ]          = esc_html__( 'Migrating schedules...', 'wpk' );
            $this->result[ 'progress' ]         = '70%';
            $this->result[ 'operation' ]        = 'migrate_schedules';

            $this->response->setResult( $this->result )->sendJson();
        }

        //Map new meta fields
        $fields                              = Creation::CONFIRMATION_FIELDS + Creation::FIELDS;
        $fields[ 'total' ]                   = [ 'price' ];
        $fields[ 'name' ]                    = [ 'name', 'nom', 'first_name', 'invoice_societe', 'invoice_name' ];
        $fields[ 'address' ]                 = [ 'address', 'adresse' ];
        $fields[ 'city' ]                    = [ 'city', 'ville', 'ville_' ];
        $fields[ 'country' ]                 = [ 'country', 'pays' ];
        $fields[ 'phone' ]                   = [ 'telephone', 'telephone', 'phone', 'phone_number' ];
        $fields[ 'payment_status' ]          = [ 'paid' ];
        $fields[ 'email_status' ]            = [ 'invoice_status' ];
        $fields[ 'cash_status' ]             = [ 'payment_type' ];
        $fields[ 'invoice_header' ][]        = 'invoice_address';
        $fields[ 'payment_type' ]            = [ 'cash_status' ];
        $fields[ 'payment_reminders_count' ] = [ 'reminder_count' ];


        $findKey = function ( $key ) use ( $fields ) {

            foreach ( $fields as $metaKey => $field ) {

                if ( in_array( $key, $field ) ) {
                    return $metaKey;
                }

            }

            return $key;

        };

        /** @var Invoice $invoice */
        foreach ( $model->all() as $invoice ) {

            //Shift old meta
            $oldMeta = array_map( 'array_shift', $invoice->meta() );

            $newMeta = [];

            foreach ( $oldMeta as $key => $value ) {

                $key     = $this->formatLabel( $key );
                $metaKey = $findKey( $key );

                $value = $this->formatMeta( $metaKey, $value );

                if ( empty( $newMeta[ $metaKey ] ) ) {
                    $newMeta[ $metaKey ] = $value;
                }


            }

            $newInvoice = Invoice::init();

            $newMeta[ 'event_date' ] = Invoice::translateMonths( $newMeta[ 'event_date' ] );
            $newMeta[ 'event_date' ] = date( Invoice::META_DATE_FORMAT, strtotime( $newMeta[ 'event_date' ] ) );

            $newMeta[ 'post_modified' ]     = $invoice->post_modified;
            $newMeta[ 'post_modified_gmt' ] = $invoice->post_modified_gmt;

            if ( empty( $newMeta[ 'payment_type' ] ) ) {
                $newMeta[ 'payment_type' ] = '---';
            }

            $newInvoice = $newInvoice
                ->addMetas( $newMeta )
                ->title( $invoice->post_title )
                ->status( $invoice->post_status )
                ->author( $invoice->post_author )
                ->create();

            //Remove old invoice
            $invoice->delete();

        }

        $this->result[ 'migratedInvoices' ] = $migratedInvoices + $model->count();


        $percentProgress = round( ( $this->result[ 'migratedInvoices' ] / $totalInvoices ) * 100, 2 );

        //Remaining 30% are the schedules
        if ( $percentProgress > 70 ) {
            $percentProgress = 70;
        }

        $this->result[ 'message' ]  = esc_html__( 'Migrating invoices...', 'wpk' );
        $this->result[ 'progress' ] = "{$percentProgress}%";

        $this->response
            ->setResult( $this->result )
            ->sendJson();

    }

    /**
     * @param int $id
     * @param string $postModifiedDate
     * @param string $postModifiedGmt
     *
     * @return void
     */
    private function updateModifiedDate( $id, $postModifiedDate, $postModifiedGmt ) {

        global $wpdb;

        $wpdb->query( "UPDATE $wpdb->posts SET post_modified = '{$postModifiedDate}', post_modified_gmt = '{$postModifiedGmt}'  WHERE ID = {$id}" );

    }

    /**
     * @return void
     */
    private function migrateSchedules() {

        $this->result[ 'message' ]   = esc_html__( 'Migrating schedules...', 'wpk' );
        $this->result[ 'operation' ] = 'migrate_schedules';

        $schedules = get_option( 'wpk_schedules' );

        $afterEvent  = $schedules[ 'after_event' ];
        $beforeEvent = $schedules[ 'before_event' ];
        $invoice     = $schedules[ 'proffesional' ];

        if ( ! empty( $beforeEvent ) ) {
            $this->migrateBeforeEvent( $beforeEvent );

            //Each schedules adds 10% to progresss
            $this->result[ 'progress' ] = '80%';

            unset( $schedules[ 'before_event' ] );
            update_option( 'wpk_schedules', $schedules );

            $this->response->setResult( $this->result )->sendJson();
        }

        if ( ! empty( $afterEvent ) ) {
            $this->migrateAfterEvent( $afterEvent );

            //Each schedules adds 10% to progresss
            $this->result[ 'progress' ] = '90%';

            unset( $schedules[ 'after_event' ] );
            update_option( 'wpk_schedules', $schedules );

            $this->response->setResult( $this->result )->sendJson();
        }

        if ( ! empty( $invoice ) ) {
            $this->migrateInvoiceSchedules( $invoice );

            //Each schedules adds 10% to progresss
            $this->result[ 'progress' ] = '100%';

            unset( $schedules[ 'proffesional' ] );
            update_option( 'wpk_schedules', $schedules );

            $this->response->setResult( $this->result )->sendJson();
        }

        if ( empty( $beforeEvent ) && empty( $afterEvent ) && empty( $invoice ) ) {
            //Finished migration!
            $this->result[ 'finished' ] = true;

            $this->result[ 'message' ] = esc_html__( 'Migration finished!', 'wpk' );

            update_option( 'wpk_migrated', true );
        }

        $this->response->setResult( $this->result )->sendJson();

    }

    /**
     * @param array $schedules
     *
     * @return void
     */
    private function migrateBeforeEvent( $schedules = [] ) {

        foreach ( $schedules as $day => $refsArray ) {

            foreach ( $refsArray as $ref => $data ) {

                /** @var Invoice $invoice */
                $invoice = Invoice::init()->hasMetaValue( 'ref', $ref )->get()->first();

                if ( $invoice ) {
                    $schedule = Schedule::createForInvoice( $invoice, 'before_event' );

                    if ( $data[ 'status' ] === 'disabled' ) {
                        $schedule->updateMeta( 'status', 'disabled' );
                    }

                    $invoice = $schedule->invoice();

                    //Make sure that modified date stays untouched
                    $this->updateModifiedDate( $invoice->ID, $invoice->meta( 'post_modified' ), $invoice->meta( 'post_modified_gmt' ) );
                }

            }

        }

    }

    /**
     * @param array $schedules
     *
     * @return void
     */
    private function migrateAfterEvent( $schedules = [] ) {

        foreach ( $schedules as $months ) {

            foreach ( $months as $days ) {

                foreach ( $days as $ref => $data ) {


                    /** @var Invoice $invoice */
                    $invoice = Invoice::init()->hasMetaValue( 'ref', $ref )->get()->first();

                    if ( $invoice ) {
                        $schedule = Schedule::createForInvoice( $invoice, 'after_event' );

                        if ( $data[ 'status' ] === 'disabled' ) {
                            $schedule->updateMeta( 'status', 'disabled' );
                        }

                        $invoice = $schedule->invoice();

                        //Make sure that modified date stays untouched
                        $this->updateModifiedDate( $invoice->ID, $invoice->meta( 'post_modified' ), $invoice->meta( 'post_modified_gmt' ) );


                    }


                }

            }

        }

    }

    /**
     * @param array $schedulesArray
     *
     * @return void
     */
    private function migrateInvoiceSchedules( $schedulesArray = [] ) {

        foreach ( $schedulesArray as $days ) {

            foreach ( $days as $ref => $data ) {

                /** @var Invoice $invoice */
                $invoice = Invoice::init()->hasMetaValue( 'ref', $ref )->get()->first();

                if ( $invoice ) {
                    $schedule = Schedule::createForInvoice( $invoice, 'invoice' );

                    if ( $data[ 'status' ] === 'disabled' ) {
                        $schedule->updateMeta( 'status', 'disabled' );
                    }

                    $invoice = $schedule->invoice();

                    //Make sure that modified date stays untouched
                    $this->updateModifiedDate( $invoice->ID, $invoice->meta( 'post_modified' ), $invoice->meta( 'post_modified_gmt' ) );

                }

            }

        }

    }


}