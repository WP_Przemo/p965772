<?php


namespace Wpk\p965772\Controllers\Schedules\BeforeEvent;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Models\Schedule;

/**
 * Handles creation of before event schedule
 *
 * @author Przemysław Żydek
 */
class Creation extends Controller {

    /**
     * Creation constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wpk/p965772/newInvoice', [ $this, 'addSchedule' ] );
    }

    /**
     * On invoice creation add "before_event" schedule
     *
     * @param Invoice $invoice
     *
     * @return void
     */
    public function addSchedule( Invoice $invoice ) {

        Schedule::createForInvoice( $invoice, 'before_event' );

    }

}