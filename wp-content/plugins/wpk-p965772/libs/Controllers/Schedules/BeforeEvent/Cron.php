<?php


namespace Wpk\p965772\Controllers\Schedules\BeforeEvent;

use Wpk\p965772\Controllers\Schedules;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Models\Schedule;
use Wpk\p965772\Settings;
use Wpk\p965772\Utility;

/**
 * Handles cron task for this type of schedule
 *
 * @author Przemysław Żydek
 */
class Cron extends Schedules\Cron {

    const HOOK = 'wpk_before_event';

    /**
     * @param int $scheduleID
     *
     * @return void
     */
    public function schedule( $scheduleID ) {

        $schedule = Schedule::find( $scheduleID );

        if ( ! $schedule->enabled() ) {
            return;
        }

        $invoice   = $schedule->invoice();
        $eventDate = strtotime( $invoice->eventDate() );

        if ( time() >= $eventDate || $invoice->isStandby() ) {
            $schedule->delete();

            return;
        }

        $lang = $invoice->getLang();

        $message = $invoice->formatText( Settings::getSetting( "mail_text_{$lang}" ) );
        $subject = $invoice->formatText( Settings::getSetting( "mail_title_before_{$lang}" ) );

        $headers    = Utility::getMailHeaders();
        $adminEmail = Settings::getSetting( 'admin_e-mail_followup' );

        if ( ! empty( $adminEmail ) ) {
            $headers[] = "cc: $adminEmail";
        }

        $result = wp_mail( $invoice->meta( 'email' ), $subject, $message, $headers );

        if ( $result ) {

            /**
             * @param Schedule $schedule
             * @param Invoice $invoice
             */
            do_action( 'wpk/p965772/beforeEventSent', $schedule, $invoice );

            $schedule->delete();
        }

    }
}