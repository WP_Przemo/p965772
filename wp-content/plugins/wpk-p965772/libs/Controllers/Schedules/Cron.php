<?php


namespace Wpk\p965772\Controllers\Schedules;

/**
 * Abstract class for cron
 *
 * @author Przemysław Żydek
 */
abstract class Cron {

    /** @var string */
    const HOOK = '';

    public function __construct() {
        add_action( static::HOOK, [ $this, 'schedule' ] );
    }


    /**
     * @param int $scheduleID
     *
     * @return void
     */
    abstract public function schedule( $scheduleID );

}