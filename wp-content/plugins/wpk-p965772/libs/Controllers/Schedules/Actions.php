<?php


namespace Wpk\p965772\Controllers\Schedules;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Core;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Models\Schedule;

/**
 * Handles general schedule related actions
 */
class Actions extends Controller {

    /**
     * Actions constructor.
     */
    public function __construct() {

        parent::__construct();

        add_action( 'before_delete_post', [ $this, 'deleteInvoiceSchedules' ] );
        add_action( 'before_delete_post', [ $this, 'cancelCron' ] );
        add_action( 'wpk/p965772/invoiceConfirmed', [ $this, 'deleteBeforeEventSchedule' ] );
        add_action( 'wpk/p965772/invoiceSent', [ $this, 'deleteInvoiceSchedule' ] );
        add_action( 'pre_get_posts', [ $this, 'filter' ] );
        add_action( 'wpk/p965772/invoiceStandBy', [ $this, 'deleteInvoiceSchedule' ] );
        add_action( 'wpk/p965772/invoiceStandBy', [ $this, 'deleteAfterEventSchedyle' ] );

    }

    /**
     * On invoice removal delete all connected schedules
     *
     * @param int $invoiceID
     *
     * @return void
     */
    public function deleteInvoiceSchedules( $invoiceID ) {

        if ( get_post_type( $invoiceID ) !== Core::INVOICE_SLUG ) {
            return;
        }

        $schedules = Invoice::find( $invoiceID )->schedules()->all();

        /** @var Schedule $schedule */
        foreach ( $schedules as $schedule ) {
            $schedule->delete();
        }

    }

    /**
     * Cancel cron event on schedule removal
     *
     * @param int $scheduleID
     *
     * @return void
     */
    public function cancelCron( $scheduleID ) {

        if ( get_post_type( $scheduleID ) !== Core::SCHEDULE_SLUG ) {
            return;
        }

        Schedule::find( $scheduleID )->removeSchedule();

    }

    /**
     * Disable "before_event" schedule after invoice confirmation
     *
     * @param Invoice $invoice
     *
     * @return void
     */
    public function deleteBeforeEventSchedule( Invoice $invoice ) {
        $invoice->deleteSchedule( 'before_event' );
    }

    /**
     * Disable "invoice" schedule after invoice have been sent manually
     *
     * @param Invoice $invoice
     *
     * @return void
     */
    public function deleteInvoiceSchedule( Invoice $invoice ) {
        $invoice->deleteSchedule( 'invoice' );
    }

    /**
     * @param Invoice $invoice
     *
     * @return void
     */
    public function deleteAfterEventSchedyle( Invoice $invoice ) {
        $invoice->deleteSchedule( 'after_event' );
    }

    /**
     * Perform filtering on schedules post page
     *
     * @param \WP_Query $query
     *
     * @return void
     */
    public function filter( \WP_Query $query ) {

        if ( ! is_admin() || $query->get( 'post_type' ) !== Core::SCHEDULE_SLUG ) {
            return;
        }

        $metaKey   = $this->getQueryParam( 'meta_key' );
        $metaValue = $this->getQueryParam( 'meta_value' );

        if ( ! empty( $metaKey ) && ! empty( $metaValue ) ) {
            $query->set( 'meta_key', $metaKey );
            $query->set( 'meta_value', $metaValue );
        }

    }


}