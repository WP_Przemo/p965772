<?php

namespace Wpk\p965772\Controllers\Schedules\Invoice;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Models\Schedule;

/**
 * Handle creation of "invoice" schedule
 */
class Creation extends Controller {

    /**
     * Creation constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wpk/p965772/invoiceConfirmed', [ $this, 'addInvoiceSchedule' ] );
    }

    /**
     * On invoice creation add "invoice" schedule if conditions are met
     *
     * @param Invoice $invoice
     */
    public function addInvoiceSchedule( Invoice $invoice ) {

        if ( $invoice->proffessionalOfEvents() || ! empty( $invoice->meta( 'company' ) ) ) {

            Schedule::createForInvoice( $invoice, 'invoice' );

        }

    }

}