<?php


namespace Wpk\p965772\Controllers\Schedules\Invoice;

use Wpk\p965772\Controllers\Schedules;
use Wpk\p965772\Models\Schedule;

/**
 * Handles cron task for this type of schedule
 *
 * @author Przemysław Żydek
 */
class Cron extends Schedules\Cron {

    const HOOK = 'wpk_invoice';

    /**
     * @param int $scheduleID
     *
     * @return void
     */
    public function schedule( $scheduleID ) {

        $schedule = Schedule::find( $scheduleID );

        if ( ! $schedule->enabled() ) {
            return;
        }

        $invoice = $schedule->invoice();

        if ( ! $invoice->wasSent() ) {
            do_action( 'wpk/p965772/sendInvoice', $invoice );
        }

        $schedule->delete();


    }
}