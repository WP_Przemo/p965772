<?php


namespace Wpk\p965772\Controllers\Schedules\AfterEvent;

use Wpk\p965772\Controllers\Controller;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Models\Schedule;

/**
 * Handles creation of after event schedule
 *
 * @author Przemysław Żydek
 */
class Creation extends Controller {

    /**
     * Creation constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wpk/p965772/invoiceConfirmed', [ $this, 'addSchedule' ] );
    }

    /**
     * On invoice creation add "before_event" schedule
     *
     * @param Invoice $invoice
     *
     * @return void
     */
    public function addSchedule( Invoice $invoice ) {

        $eventType     = strtolower( $invoice->meta( 'event_type' ) );
        $disabledTypes = [ 'wedding', 'mariage', 'marriage' ];

        if ( ! in_array( $eventType, $disabledTypes ) ) {
            Schedule::createForInvoice( $invoice, 'after_event' );
        }

    }

}