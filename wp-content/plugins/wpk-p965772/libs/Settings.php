<?php


namespace Wpk\p965772;

/**
 * Manages settings using ACF plugin
 *
 * @author Przemysław Żydek
 */
class Settings {

    /** @var array Stores option args */
    protected $optionArgs = [];

    /** @var array Stores sub option menu args */
    protected $subOptionArgs = [];

    /** @var bool Determines if ACF plugin exists on site */
    public static $isAcf = false;

    public function __construct() {

        self::$isAcf = function_exists( 'get_field' );

        if ( self::$isAcf ) {
            $this->optionArgs = [
                [
                    'page_title' => 'WPK Invoices',
                    'menu_title' => 'WPK Invoices',
                    'menu_slug'  => Core::MENU_SLUG,
                    'capability' => 'manage_options',
                ],

            ];

            $this->subOptionArgs = [
                [
                    'page_title'  => '',
                    'menu_title'  => '',
                    'menu_slug'   => '',
                    'capability'  => 'manage_options',
                    'parent_slug' => '',
                ],
            ];
            add_action( 'init', [ $this, 'addOptionsPage' ] );

            $this->createFields();
        }

    }

    /**
     * Create ACF fields
     *
     * @return void
     */
    protected function createFields() {

        if ( function_exists( 'acf_add_local_field_group' ) ):

            acf_add_local_field_group( [
                'key'                   => 'group_5b45d197a9be3',
                'title'                 => 'Invoice',
                'fields'                => [
                    [
                        'key'               => 'field_5b45daad7a3e9',
                        'label'             => 'Reference number',
                        'name'              => 'ref',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b48a2b7f4d33',
                        'label'             => 'Company',
                        'name'              => 'company',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5b45daa67a3e8',
                        'label'             => 'Name',
                        'name'              => 'name',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5b45daa27a3e7',
                        'label'             => 'Email',
                        'name'              => 'email',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b45da8c7a3e3',
                        'label'             => 'Phone',
                        'name'              => 'phone',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b45da917a3e4',
                        'label'             => 'Country',
                        'name'              => 'country',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b45dac97a3ec',
                        'label'             => 'Event date',
                        'name'              => 'event_date',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b45da967a3e5',
                        'label'             => 'City',
                        'name'              => 'city',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b45da9d7a3e6',
                        'label'             => 'Address',
                        'name'              => 'address',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b45db88551e1',
                        'label'             => 'Details',
                        'name'              => 'details',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b45dab97a3ea',
                        'label'             => 'Vat',
                        'name'              => 'vat',
                        'type'              => 'number',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'min'               => '',
                        'max'               => '',
                        'step'              => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b45da807a3e2',
                        'label'             => 'Total',
                        'name'              => 'total',
                        'type'              => 'number',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'min'               => '',
                        'max'               => '',
                        'step'              => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b48a3312da5c',
                        'label'             => 'Invoice header',
                        'name'              => 'invoice_header',
                        'type'              => 'textarea',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'maxlength'         => '',
                        'rows'              => '',
                        'new_lines'         => 'wpautop',
                    ],
                ],
                'location'              => [
                    [
                        [
                            'param'    => 'post_type',
                            'operator' => '==',
                            'value'    => 'wpk_invoice',
                        ],
                    ],
                ],
                'menu_order'            => 0,
                'position'              => 'normal',
                'style'                 => 'default',
                'label_placement'       => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen'        => '',
                'active'                => 1,
                'description'           => '',
            ] );

            acf_add_local_field_group( [
                'key'                   => 'group_59478256b170a',
                'title'                 => 'Invoice settings',
                'fields'                => [
                    [
                        'key'               => 'field_5b478fa01eba8',
                        'label'             => 'Invoice',
                        'name'              => '',
                        'type'              => 'tab',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'placement'         => 'top',
                        'endpoint'          => 0,
                    ],
                    [
                        'key'               => 'field_594782633cf22',
                        'label'             => 'Invoice header logo',
                        'name'              => 'wpk_header_logo',
                        'type'              => 'image',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'return_format'     => 'array',
                        'preview_size'      => 'thumbnail',
                        'library'           => 'all',
                        'min_width'         => '',
                        'min_height'        => '',
                        'min_size'          => '',
                        'max_width'         => '',
                        'max_height'        => '',
                        'max_size'          => '',
                        'mime_types'        => '',
                    ],
                    [
                        'key'               => 'field_594a6ac58d219',
                        'label'             => 'Invoice header text content',
                        'name'              => 'wpk_header_text_content',
                        'type'              => 'wysiwyg',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'default_value'     => '',
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_594782ad3cf23',
                        'label'             => 'Invoice footer content',
                        'name'              => 'wpk_footer_content',
                        'type'              => 'wysiwyg',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'default_value'     => '',
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_5974910445ac5',
                        'label'             => 'Default invoice details',
                        'name'              => 'wpk_default_invoice_details',
                        'type'              => 'text',
                        'instructions'      => 'Please separate each value by comma.',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b478fb11eba9',
                        'label'             => 'Email messages',
                        'name'              => '',
                        'type'              => 'tab',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'placement'         => 'top',
                        'endpoint'          => 0,
                    ],
                    [
                        'key'               => 'field_5950c0c11c06a',
                        'label'             => 'Invoice mail text',
                        'name'              => 'wpk_invoice_mail_text_fr',
                        'type'              => 'wysiwyg',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{invoice_id} - invoice number,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_5b5f0d96d8934',
                        'label'             => 'Invoice mail text (english)',
                        'name'              => 'wpk_invoice_mail_text_en',
                        'type'              => 'wysiwyg',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{invoice_id} - invoice number,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_595a4afaeb1c0',
                        'label'             => 'Mail title (before_event)',
                        'name'              => 'wpk_mail_title_before_fr',
                        'type'              => 'text',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue

{company_name} - company name',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5b5f0daad8935',
                        'label'             => 'Mail title (before_event) (english)',
                        'name'              => 'wpk_mail_title_before_en',
                        'type'              => 'text',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue

{company_name} - company name',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_594ed462c66e6',
                        'label'             => 'Mail text (before event)',
                        'name'              => 'wpk_mail_text_fr',
                        'type'              => 'wysiwyg',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue
{company_name} - company name
{link} - Link that will disable this follow-up',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_5b5f0db6d8936',
                        'label'             => 'Mail text (before event) (english)',
                        'name'              => 'wpk_mail_text_en',
                        'type'              => 'wysiwyg',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue
{company_name} - company name
{link} - Link that will disable this follow-up',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_595a4ba7af1cd',
                        'label'             => 'Mail title (after_event)',
                        'name'              => 'wpk_mail_title_after_fr',
                        'type'              => 'text',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue
{company_name} - company name',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5b5f0dc3d8937',
                        'label'             => 'Mail title (after_event) (english)',
                        'name'              => 'wpk_mail_title_after_en',
                        'type'              => 'text',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue
{company_name} - company name',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5950afca6f7d7',
                        'label'             => 'Mail text (after_event)',
                        'name'              => 'wpk_mail_text_after_event_fr',
                        'type'              => 'wysiwyg',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{company_name} - company name
{link} - Link that will disable this follow-up
{invoice_date} - Date of the invoice issue',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_5b5f0dd2d8938',
                        'label'             => 'Mail text (after_event) (english)',
                        'name'              => 'wpk_mail_text_after_event_en',
                        'type'              => 'wysiwyg',
                        'instructions'      => 'Available values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{company_name} - company name
{link} - Link that will disable this follow-up
{invoice_date} - Date of the invoice issue',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_595d29a2c83c6',
                        'label'             => 'Payment reminder e-mail title',
                        'name'              => 'wpk_payment_reminder_email_title_fr',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5b5f0de0d8939',
                        'label'             => 'Payment reminder e-mail title (english)',
                        'name'              => 'wpk_payment_reminder_email_title_en',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_595d29b1c83c7',
                        'label'             => 'Payment reminder e-mail content',
                        'name'              => 'wpk_payment_reminder_email_content_fr',
                        'type'              => 'wysiwyg',
                        'instructions'      => 'Values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_5b5f0deed893a',
                        'label'             => 'Payment reminder e-mail content (english)',
                        'name'              => 'wpk_payment_reminder_email_content_en',
                        'type'              => 'wysiwyg',
                        'instructions'      => 'Values:
{ref} - form reference number,
{event_date} - date of the event,
{customer_email} - customer email address.
{customer_name} - customer name
{invoice_date} - Date of the invoice issue',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'tabs'              => 'all',
                        'toolbar'           => 'full',
                        'media_upload'      => 1,
                        'delay'             => 0,
                    ],
                    [
                        'key'               => 'field_5b478fd91ebab',
                        'label'             => 'Forms',
                        'name'              => '',
                        'type'              => 'tab',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'placement'         => 'top',
                        'endpoint'          => 0,
                    ],
                    [
                        'key'               => 'field_5950af466f7d6',
                        'label'             => 'Confirmation form IDs',
                        'name'              => 'wpk_confirmation_forms',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5979aff6fbe75',
                        'label'             => '"Customization" form IDS',
                        'name'              => 'wpk_customization_forms',
                        'type'              => 'text',
                        'instructions'      => 'IDs of customization forms. These forms will be ignored when generating invoice. Please separate each value by comma.',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b5f0e4f57528',
                        'label'             => 'English forms IDS',
                        'name'              => 'wpk_english_forms',
                        'type'              => 'text',
                        'instructions'      => 'Separate each value by comma',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5b478fed1ebac',
                        'label'             => 'Vat',
                        'name'              => '',
                        'type'              => 'tab',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'placement'         => 'top',
                        'endpoint'          => 0,
                    ],
                    [
                        'key'               => 'field_5959ea8c1d337',
                        'label'             => 'Vat amount (%)',
                        'name'              => 'wpk_vat_amount',
                        'type'              => 'number',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'min'               => '',
                        'max'               => '',
                        'step'              => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5959f2983df2d',
                        'label'             => 'Vat number',
                        'name'              => 'wpk_vat_number',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b478ff41ebad',
                        'label'             => 'Emails',
                        'name'              => '',
                        'type'              => 'tab',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'placement'         => 'top',
                        'endpoint'          => 0,
                    ],
                    [
                        'key'               => 'field_596df24a448ff',
                        'label'             => 'Admin e-mail',
                        'name'              => 'wpk_admin_e-mail',
                        'type'              => 'email',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5976ec90bdd6d',
                        'label'             => 'Admin e-mail (for follow-up)',
                        'name'              => 'wpk_admin_e-mail_followup',
                        'type'              => 'email',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_599c6deb2bec3',
                        'label'             => 'Admin e-mail (for payment reminders)',
                        'name'              => 'wpk_admin_email_payment',
                        'type'              => 'email',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5a952f8c688d2',
                        'label'             => 'Quote confirmation e-mail list',
                        'name'              => 'wpk_confirmation_emails',
                        'type'              => 'repeater',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'min'               => 0,
                        'max'               => 0,
                        'layout'            => 'table',
                        'button_label'      => '',
                        'collapsed'         => '',
                        'sub_fields'        => [
                            [
                                'key'               => 'field_5a953051688d3',
                                'label'             => 'Email',
                                'name'              => 'email',
                                'type'              => 'email',
                                'instructions'      => '',
                                'required'          => 0,
                                'conditional_logic' => 0,
                                'wrapper'           => [
                                    'width' => '',
                                    'class' => '',
                                    'id'    => '',
                                ],
                                'default_value'     => '',
                                'placeholder'       => '',
                                'prepend'           => '',
                                'append'            => '',
                            ],
                        ],
                    ],
                    [
                        'key'               => 'field_5b478ffe1ebae',
                        'label'             => 'Other',
                        'name'              => '',
                        'type'              => 'tab',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'placement'         => 'top',
                        'endpoint'          => 0,
                    ],
                    [
                        'key'               => 'field_596df25a44900',
                        'label'             => 'Site name',
                        'name'              => 'wpk_site_name',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'maxlength'         => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                    ],
                    [
                        'key'               => 'field_5b4c92a551fac',
                        'label'             => 'Customization form error message',
                        'name'              => 'wpk_customization_form_error_message',
                        'type'              => 'text',
                        'instructions'      => 'Message displayed if reference number or e-mail won\'t match in confirmation form',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                ],
                'location'              => [
                    [
                        [
                            'param'    => 'options_page',
                            'operator' => '==',
                            'value'    => 'wpk_invoices',
                        ],
                    ],
                ],
                'menu_order'            => 0,
                'position'              => 'normal',
                'style'                 => 'default',
                'label_placement'       => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen'        => '',
                'active'                => 1,
                'description'           => '',
            ] );

            acf_add_local_field_group( [
                'key'                   => 'group_5b4c368de5006',
                'title'                 => 'Schedule',
                'fields'                => [
                    [
                        'key'               => 'field_5b4c369b3c980',
                        'label'             => 'Status',
                        'name'              => 'status',
                        'type'              => 'select',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'choices'           => [
                            'enabled'  => 'Enabled',
                            'disabled' => 'Disabled',
                        ],
                        'default_value'     => [
                        ],
                        'allow_null'        => 0,
                        'multiple'          => 0,
                        'ui'                => 0,
                        'ajax'              => 0,
                        'return_format'     => 'value',
                        'placeholder'       => '',
                    ],
                ],
                'location'              => [
                    [
                        [
                            'param'    => 'post_type',
                            'operator' => '==',
                            'value'    => 'wpk_schedule',
                        ],
                    ],
                ],
                'menu_order'            => 0,
                'position'              => 'normal',
                'style'                 => 'default',
                'label_placement'       => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen'        => '',
                'active'                => 1,
                'description'           => '',
            ] );

        endif;

    }


    /**
     * Add options pages
     */
    public function addOptionsPage() {

        foreach ( $this->optionArgs as $option_arg ) {
            acf_add_options_page( $option_arg );
        }

        foreach ( $this->subOptionArgs as $sub_option_arg ) {
            acf_add_options_sub_page( $sub_option_arg );
        }


    }

    /**
     * Perform conditional formatting for setting
     *
     * @param string $key
     * @param mixed $value
     *
     * @return mixed
     */
    protected static function formatSetting( $key, $value ) {

        switch ( $key ) {

            case 'confirmation_forms':
            case 'customization_forms':

                $array = explode( ',', $value );

                $array = array_map( 'intval', $array );

                return $array;

                break;

            default:
                return $value;

        }

    }

    /**
     * Get setting via its key
     *
     * @param string|int $key
     * @param mixed $default Default value for option
     *
     * @return bool|mixed
     */
    public static function getSetting( $key, $default = false ) {

        $value = self::getField( $key );
        $value = empty( $value ) ? $default : $value;


        return self::formatSetting( $key, $value );

    }

    /**
     * Helper function for accessing acf fields in options
     *
     * @param mixed $key
     * @param string $prefix Option prefix
     *
     * @return mixed|false
     */
    public static function getField( $key, $prefix = 'wpk' ) {

        if ( ! self::$isAcf ) {
            return false;
        }

        return get_field( "{$prefix}_{$key}", 'option' );

    }


}