<?php

namespace Wpk\p965772;

use Wpk\p965772\Helpers\PostType;
use Wpk\p965772\Controllers;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Models\Schedule;

/**
 * Core class
 *
 * @author Przemysław Żydek
 */
final class Core {

    /** @var Core Stores class instance */
    private static $instance;

    /** @var string Slug used for translations */
    const SLUG = 'wpk-p965772';

    /** @var string */
    const MENU_SLUG = 'wpk_invoices';

    /** @var string */
    const INVOICE_SLUG = 'wpk_invoice';

    /** @var string */
    const SCHEDULE_SLUG = 'wpk_schedule';

    /** @var string Stores path to this plugin directory */
    public $dir;

    /** @var string Stores url to this plugin directory */
    public $url;

    /** @var string Main plugin file */
    public $file;

    /** @var Loader Stores loader instance */
    public $loader;

    /** @var Utility */
    public $utility;

    /** @var Enqueue */
    public $enqueue;

    /** @var View */
    public $view;

    /** @var Settings */
    public $settings;

    /** @var array */
    public $controllers = [
        Controllers\Invoice\Actions::class,
        Controllers\Invoice\Creation::class,
        Controllers\Invoice\DeleteByDate::class,
        Controllers\Invoice\MetaBoxes::class,
        Controllers\Invoice\Pdf::class,
        Controllers\Schedules\AfterEvent\Creation::class,
        Controllers\Schedules\AfterEvent\Cron::class,
        Controllers\Schedules\BeforeEvent\Creation::class,
        Controllers\Schedules\BeforeEvent\Cron::class,
        Controllers\Schedules\Invoice\Creation::class,
        Controllers\Schedules\Invoice\Cron::class,
        Controllers\Schedules\Actions::class,
        Controllers\Security\Password::class,
        Controllers\Security\View::class,
        Controllers\Migrate\Menu::class,
        Controllers\Migrate\Handler::class,
    ];

    /** @var bool Determines whenever init have been performed */
    private $loaded = false;

    private function __construct() {
    }

    private function __sleep() {
    }

    private function __wakeup() {
    }

    private function __clone() {
    }

    /**
     * Core init
     *
     * @param string $file Main plugin file
     * @param string $namespace Namespace of core class
     *
     * @return $this
     */
    public function init( $file = null, $namespace = null ) {

        if ( ! $this->loaded ) {

            $this->file = empty( $file ) ? __FILE__ : $file;
            $this->url  = plugin_dir_url( $this->file );
            $this->dir  = plugin_dir_path( $this->file );

            $namespace = empty( $namespace ) ? __NAMESPACE__ : $namespace;

            $this->loader   = new Loader( $this->dir . '/libs', $namespace );
            $this->enqueue  = new Enqueue();
            $this->utility  = new Utility();
            $this->settings = new Settings();
            $this->view     = new View( "{$this->dir}/views" );

            $this
                ->registerInvoices()
                ->registerSchedules()
                ->loadControllers();

            $this->loaded = true;

        }

        return $this;

    }

    /**
     * @return $this
     */
    private function loadControllers() {

        foreach ( $this->controllers as $controller ) {
            new $controller();
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function registerInvoices() {

        $postType = new PostType( self::INVOICE_SLUG, [

            'labels'            => [
                'name'          => 'Invoices',
                'singular_name' => 'Invoice',
            ],
            'public'            => false,
            'show_ui'           => true,
            'show_in_menu'      => true,
            'show_in_admin_bar' => true,
            'supports'          => [
                'custom-fields',
            ],
        ] );

        $postType
            ->addSortableColumn(
                'ref',
                __( 'Reference', 'wpk' ),
                [
                    'meta_key' => 'ref',
                    'orderby'  => 'meta_value',
                ],
                function ( $postID ) {
                    echo Invoice::find( $postID )->meta( 'ref' );
                } )
            ->addSortableColumn(
                'event_date',
                __( 'Event date', 'wpk' ),
                [
                    'meta_key' => 'event_date',
                    'orderby'  => 'meta_value',
                ],
                function ( $postID ) {
                    echo Invoice::find( $postID )->eventDate();
                } )
            ->addAdminColumn( 'company', esc_html__( 'Company', 'wpk' ), function ( $postID ) {
                echo Invoice::find( $postID )->meta( 'company' );
            } )
            ->addAdminColumn( 'name', esc_html__( 'Name', 'wpk' ), function ( $postID ) {
                echo Invoice::find( $postID )->meta( 'name' );
            } )
            ->addSortableColumn( 'net_price', esc_html__( 'Net price', 'wpk' ), [ 'meta_key' => 'total' ], function ( $postID ) {
                echo Invoice::find( $postID )->prices( true )[ 'price' ];
            } )
            ->addAdminColumn( 'vat_price', esc_html__( 'Vat', 'wpk' ), function ( $postID ) {
                echo Invoice::find( $postID )->prices( true )[ 'vat' ];
            } )
            ->addAdminColumn( 'gross_price', esc_html__( 'Gross price', 'wpk' ), function ( $postID ) {
                echo Invoice::find( $postID )->prices( true )[ 'total' ];
            } )
            ->addSortableColumn(
                'quote_status',
                __( 'Quote status', 'wpk' ),
                [ 'meta_key' => 'quote_status' ],
                function ( $postID ) {

                    $status = Invoice::find( $postID )->meta( 'quote_status' );

                    echo Invoice::formatMeta( $status );
                } )
            ->addSortableColumn(
                'invoice_id',
                __( 'Invoice ID', 'wpk' ),
                [ 'meta_key' => 'invoice_id', 'orderby' => 'meta_value' ],
                function ( $postID ) {

                    $id = Invoice::find( $postID )->invoiceID();

                    echo empty( $id ) ? __( 'Not generated yet', 'wpk' ) : $id;
                } )
            ->addAdminColumn( 'invoice_actions', esc_html__( 'Actions', 'wpk' ), function ( $postID ) {

                $invoice = Invoice::find( $postID );

                $data = [
                    'invoice'         => $invoice,
                    'paymentStatus'   => Invoice::formatMeta( $invoice->meta( 'payment_status' ) ),
                    'paymentType'     => Invoice::formatMeta( $invoice->meta( 'payment_type' ) ),
                    'paymentReminder' => sprintf( __( 'Reminder (%s)', 'wpk' ),
                        (int) $invoice->meta( 'payment_reminders_count' )
                    ),
                ];

                echo $this->view->render( 'invoice.admin.table-actions', $data );

            } )
            //Send invoice bulk action
            ->addBulkAction( 'send_invoice', esc_html__( 'Send invoices', 'wpk' ), function ( $redirectTo, $postIDS ) {

                foreach ( $postIDS as $postID ) {
                    do_action( 'wpk/p965772/sendInvoice', Invoice::find( $postID ) );
                }

                return $redirectTo;

            } )
            //Invoice confirm bulk action
            ->addBulkAction( 'confirm_invoice', esc_html__( 'Confirm invoices', 'wpk' ), function ( $redirectTo, $postIDS ) {

                foreach ( $postIDS as $postID ) {

                    $invoice = Invoice::find( $postID );

                    //Don't re-confirm
                    if ( $invoice->isConfirmed() ) {
                        continue;
                    }

                    $invoice->updateMeta( 'quote_status', 'confirmed' )->status( 'publish' )->update();

                    /**
                     * @param Invoice $invoice
                     */
                    do_action( 'wpk/p965772/invoiceConfirmed', $invoice );

                }

                return $redirectTo;
            } )
            //Bulk mark as standby
            ->addBulkAction( 'standby_invoice', esc_html__( 'Mark as standby', 'wpk' ), function ( $redirectTo, $postIDS ) {

                foreach ( $postIDS as $postID ) {

                    $invoice = Invoice::find( $postID );
                    $invoice->updateMeta( 'quote_status', 'standby' );

                    do_action( 'wpk/p965772/invoiceStandBy', $invoice );
                }

                return $redirectTo;

            } );


        return $this;

    }

    /**
     * @return $this
     */
    private function registerSchedules() {

        $postType = new PostType( self::SCHEDULE_SLUG, [

            'labels'            => [
                'name'          => 'Schedules',
                'singular_name' => 'Schedule',
            ],
            'public'            => false,
            'show_ui'           => true,
            'show_in_menu'      => true,
            'show_in_admin_bar' => true,
            'supports'          => [
                'custom-fields',
            ],
        ] );

        $postType
            ->addAdminColumn( 'type', esc_html__( 'Type', 'wpk' ), function ( $postID ) {

                $type = Schedule::find( $postID )->meta( 'type' );
                $type = str_replace( '_', ' ', $type );

                echo ucfirst( $type );
            } )
            ->addAdminColumn( 'status', esc_html__( 'Status', 'wpk' ), function ( $postID ) {

                echo ucfirst( Schedule::find( $postID )->meta( 'status' ) );

            } )
            ->addSortableColumn( 'schedule_date', __( 'Schedule date', 'wpk' ), [ 'meta_key' => 'time' ], function ( $postID ) {
                echo date( Invoice::DATE_FORMAT, Schedule::find( $postID )->meta( 'time' ) );
            } )
            ->addAdminColumn( 'invoice_url', esc_html__( 'Invoice', 'wpk' ), function ( $postID ) {

                $schedule = Schedule::find( $postID );
                $invoice  = $schedule->invoice();

                printf( '<a target="_blank" href="%s">%s</a>', get_edit_post_link( $invoice->ID ), $invoice->post_title );

            } )
            ->addBulkAction( 'disable', esc_html__( 'Disable schedules', 'wpk' ), function ( $redirectTo, $postIDS ) {

                foreach ( $postIDS as $postID ) {

                    $schedule = Schedule::find( $postID );

                    $schedule->updateMeta( 'status', 'disabled' );
                }

                return $redirectTo;

            } )
            ->addBulkAction( 'enable', esc_html__( 'Enable schedules', 'wpk' ), function ( $redirectTo, $postIDS ) {

                foreach ( $postIDS as $postID ) {

                    $schedule = Schedule::find( $postID );

                    $schedule->updateMeta( 'status', 'enabled' );
                }

                return $redirectTo;

            } )
            //Submenus for filtering
            ->addSubmenu(
                esc_html__( 'Before event', 'wpk' ),
                esc_html__( 'Before event', 'wpk' ),
                sprintf( 'edit.php?post_type=%s&meta_key=type&meta_value=before_event', self::SCHEDULE_SLUG ) )
            ->addSubmenu(
                esc_html__( 'After event', 'wpk' ),
                esc_html__( 'After event', 'wpk' ),
                sprintf( 'edit.php?post_type=%s&meta_key=type&meta_value=after_event', self::SCHEDULE_SLUG ) )
            ->addSubmenu(
                esc_html__( 'Invoice schedules', 'wpk' ),
                esc_html__( 'Invoice schedules', 'wpk' ),
                sprintf( 'edit.php?post_type=%s&meta_key=type&meta_value=invoice', self::SCHEDULE_SLUG ) );


        return $this;

    }

    /**
     * Get Core instance.
     *
     * @return Core
     */
    public static function getInstance() {

        if ( empty( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;

    }
}
