<?php

namespace Wpk\p965772;

use Wpk\p965772\Controllers\Invoice\Creation;
use Wpk\p965772\Controllers\Migrate\Handler;
use Wpk\p965772\Controllers\Schedules\BeforeEvent\Cron;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Models\Schedule;
use Wpk\p965772\Traits;

/**
 * Utility class.
 *
 * @author Przemysław Żydek
 */
class Utility {

    use Traits\Request;

    /**@var string Debug option slug. */
    private static $name = 'wpk_debug';

    /**@var mixed Stores debug option. */
    private static $debug;


    /**
     * Utility constructor
     */
    public function __construct() {

        self::$debug = get_option( self::$name, [] );
        add_action( 'admin_menu', [ $this, 'addMenu' ] );

    }

    /**
     * Update debug option
     *
     * @param mixed $value
     */
    public static function updateDebug( $value ) {

        self::$debug = $value;
        update_option( self::$name, self::$debug );

    }

    /**
     * Helper function for logging stuff
     *
     * @param mixed $log
     * @param string $title
     */
    public static function log( $log, $title = null ) {

        if ( ! empty( $title ) ) {
            error_log( $title );
        }
        if ( is_array( $log ) || is_object( $log ) ) {
            error_log( print_r( $log, true ) );
        } else {
            error_log( $log );
        }


    }

    /**
     * Get debug option value
     *
     * @return mixed
     */
    public static function getDebug() {

        return self::$debug;

    }

    /**
     * Add debug menu.
     */
    public function addMenu() {

        add_menu_page( 'WPK Debug', 'WPK Debug', 'manage_options', 'wpk_debug', [ $this, 'debugMenu' ] );

    }

    /**
     * Debug menu callback function.
     */
    public function debugMenu() {

        $schedules = get_option( 'wpk_schedules' );

        $test = null;

    }

    /**
     * Display user first and last name, or login
     *
     * @param \WP_User $user
     *
     * @return string User name
     */
    public static function getUserName( \WP_User $user ) {

        if ( empty( $user->first_name ) || empty( $user->last_name ) ) {
            return $user->user_login;
        }

        return $user->first_name . ' ' . $user->last_name;

    }

    /**
     * Get date range from provided dates and return them in selected format.
     *
     * @param \DateTime $from
     * @param \DateTime $to
     * @param \DateInterval $interval
     * @param string $format
     *
     * @return array
     */
    public static function getDateRange( \DateTime $from, \DateTime $to, \DateInterval $interval, $format = 'Y-m-d' ) {

        $range  = new \DatePeriod( $from, $interval, $to );
        $result = [];


        foreach ( $range as $item ) {
            $result[] = $item->format( $format );
        }
        try {
            $last = new \DateTime( end( $result ) );
            $last->modify( 'tomorrow' );
        } catch ( \Exception $e ) {
            $current_year = date( 'Y' );
            $last         = new \DateTime( $current_year . '-' . end( $result ) );
            $last->modify( 'tomorrow' );
        }
        $result[] = $last->format( $format );


        return $result;

    }

    /**
     * Check if element is iframe.
     *
     * @param $string
     *
     * @return bool
     */
    public static function isIframe( $string ) {

        return strpos( $string, '<iframe' ) !== false;

    }

    /**
     *
     *
     * @param int $value
     *
     * @return float|string
     */
    public static function round( $value ) {

        $value    = floatval( $value );
        $exploded = str_split( (string) $value );
        if ( $value >= 1000000 ) {
            return $exploded[ 0 ] . 'm';
        } else if ( $value >= 100000 ) {
            return $exploded[ 0 ] . $exploded[ 1 ] . $exploded[ 2 ] . 'k';
        } else if ( $value >= 10000 ) {
            return $exploded[ 0 ] . $exploded[ 1 ] . 'k';
        } else if ( $value > 999 ) {
            return $exploded[ 0 ] . 'k';
        }

        return $value;

    }

    /**
     * Validate e-mail address
     *
     * @param string $email
     *
     * @return bool
     */
    public static function validateEmail( $email ) {

        $isValid = true;
        $atIndex = strrpos( $email, "@" );

        if ( is_bool( $atIndex ) && ! $atIndex ) {
            $isValid = false;
        } else {
            $domain    = substr( $email, $atIndex + 1 );
            $local     = substr( $email, 0, $atIndex );
            $localLen  = strlen( $local );
            $domainLen = strlen( $domain );

            if ( $localLen < 1 || $localLen > 64 ) {
                // local part length exceeded
                $isValid = false;
            } else if ( $domainLen < 1 || $domainLen > 255 ) {
                // domain part length exceeded
                $isValid = false;
            } else if ( $local[ 0 ] == '.' || $local[ $localLen - 1 ] == '.' ) {
                // local part starts or ends with '.'
                $isValid = false;
            } else if ( preg_match( '/\\.\\./', $local ) ) {
                // local part has two consecutive dots
                $isValid = false;
            } else if ( ! preg_match( '/^[A-Za-z0-9\\-\\.]+$/', $domain ) ) {
                // character not valid in domain part
                $isValid = false;
            } else if ( preg_match( '/\\.\\./', $domain ) ) {
                // domain part has two consecutive dots
                $isValid = false;
            } else if ( ! preg_match( '/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace( "\\\\", "", $local ) ) ) {
                // character not valid in local part unless
                // local part is quoted
                if ( ! preg_match( '/^"(\\\\"|[^"])+"$/',
                    str_replace( "\\\\", "", $local ) ) ) {
                    $isValid = false;
                }
            }

            if ( $isValid && function_exists( "checkdnsrr" ) && ! ( checkdnsrr( $domain, "MX" ) || checkdnsrr( $domain, "A" ) ) ) {
                // domain not found in DNS
                $isValid = false;
            }
        }

        return $isValid;

    }

    /**
     * Check if string contains provided substring
     *
     * @param string $string
     * @param string|array $substring
     *
     * @return bool
     */
    public static function contains( $string, $substring ) {

        if ( is_array( $substring ) ) {
            foreach ( $substring as $item ) {
                if ( strpos( $string, $item ) === false ) {
                    return false;
                }
            }

            return true;
        } else {
            return strpos( $string, $substring ) !== false;
        }

    }

    /**
     * Helper function for getting templates.
     *
     * @param string $path Path to template (relative to /templates)
     * @param bool $return Whenever return template content or just load it
     *
     * @return bool|string
     */
    public static function getTemplate( $path, $return = false ) {

        $core         = Core();
        $templatePath = $core->dir . '/templates/';
        $path         = $templatePath . $path;

        if ( ! file_exists( $path ) ) {
            return false;
        }

        if ( $return ) {
            ob_start();
            require_once $path;

            return ob_get_clean();
        } else {
            require_once $path;

            return true;
        }

    }

    /**
     * Perform console log with provided value
     *
     * @param mixed $value
     */
    public static function consoleLog( $value ) {

        $debug = debug_backtrace();
        $debug = $debug[ 0 ][ 'file' ] . ' Line ' . $debug[ 0 ][ 'line' ];

        $hook = is_admin() ? 'admin_footer' : 'wp_footer';

        add_action( $hook, function () use ( $debug, $value ) {
            ?>
			<script>
                'use strict';
                console.log(<?php echo json_encode( $debug ) ?>);
                console.log(<?php echo json_encode( $value ) ?>);
			</script>
            <?php
        } );

    }

    /**
     * Create attachment from uploaded file
     *
     * @param array $file
     *
     * @return bool|int
     */
    public static function createAttachment( $file = [] ) {

        $upload = wp_handle_upload( $file, [ 'test_form' => false ] );

        if ( ! isset( $upload[ 'error' ] ) ) {

            $attachment = [
                'post_mime_type' => $upload[ 'type' ],
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $upload[ 'url' ] ) ),
                'post_content'   => '',
                'post_status'    => 'inherit',
                'guid'           => $upload[ 'url' ],
            ];

            $attachID = wp_insert_attachment( $attachment, $upload[ 'file' ] );

            if ( is_wp_error( $attachID ) ) {
                return false;
            }

            require_once( ABSPATH . 'wp-admin/includes/image.php' );

            $attachmentData = wp_generate_attachment_metadata( $attachID, $upload[ 'file' ] );

            wp_update_attachment_metadata( $attachID, $attachmentData );

            return $attachID;

        }

        return false;

    }

    /**
     * Get additional values from form basing on static fields
     *
     * @param array $fields
     * @param array $values
     *
     * @return array
     */
    public static function getAdditionalValues( $fields, $values ) {

        $result = [];

        foreach ( $values as $key => $value ) {

            //Use static field values to find the desired ones
            foreach ( $fields as $fieldKey => $names ) {

                if ( in_array( $key, $names ) ) {
                    $result[ $fieldKey ] = $value;
                }

            }

        }

        return $result;

    }

    /**
     * @return array
     */
    public static function getMailHeaders() {

        return [
            "MIME-Version: 1.0",
            "Content-Type: text/html; charset=UTF-8;",
        ];

    }

    /**
     * @return string
     */
    public static function getCurrentUrl() {
        global $wp;

        $queryString = isset( $_SERVER[ 'QUERY_STRING' ] ) ? $_SERVER[ 'QUERY_STRING' ] : '';

        return add_query_arg( $queryString, '', home_url( $wp->request ) );
    }

}