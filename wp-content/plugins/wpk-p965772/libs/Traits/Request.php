<?php


namespace Wpk\p965772\Traits;

/**
 * Trait with helper request methods
 */
trait Request {

    /**
     * Helper function for getting post param
     *
     * @param      $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function getPostParam( $key, $default = null ) {
        return isset( $_POST[ $key ] ) ? stripslashes_deep( $_POST[ $key ] ) : $default;
    }

    /**
     * Get file from request
     *
     * @param string $key
     *
     * @return bool|array
     */
    public function getFile( $key ) {

        $file = false;

        if ( isset( $_FILES[ $key ] ) ) {

            $file = $_FILES[ $key ];

            if ( $file[ 'error' ] ) {
                $file = false;
            }

        }

        return $file;
    }

    /**
     * Helper function for getting query param
     *
     * @param mixed $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function getQueryParam( $key, $default = null ) {
        return isset( $_GET[ $key ] ) ? stripslashes_deep( $_GET[ $key ] ) : $default;
    }

}