<?php


namespace Wpk\p965772\Templates;

use function Wpk\p965772\Core;

/**
 * Abstract class for creating new cron schedules.
 *
 * @author Przemysław Żydek
 */
abstract class Schedule {

    /** @var string Name of hook used for this schedule */
    protected $hook = '';

    /** @var array Array of methods uses as callbacks for this schedule */
    protected $callbacks = [];

    /** @var string Determines how often schedule should run (hourly, twicedaily, daily) */
    protected $recurrence = 'hourly';

    /** @var array Args for schedule callbacks */
    protected $args = [];


    /**
     * Schedule constructor.
     *
     * @throws \Exception
     *
     */
    public function __construct() {

        if ( empty( $this->hook ) ) {
            throw new \Exception( __( 'You need do provide hook name for schedule.', 'wpk' ) );
        }

        $this->setupActivationHooks();
        $this->setupHooks();

    }

    /**
     * Setups schedule activation hooks
     */
    protected function setupActivationHooks() {

        register_activation_hook( Core()->file, [ $this, 'createSchedule' ] );
        register_deactivation_hook( Core()->file, [ $this, 'removeSchedule' ] );

    }

    /**
     * Setups schedule hooks
     */
    protected function setupHooks() {

        foreach ( $this->callbacks as $callback ) {
            add_action( $this->hook, [ $this, $callback ] );
        }

    }

    /**
     * Creates schedule after theme activation
     */
    public function createSchedule() {

        /* TODO Configurable timestamp? */
        wp_schedule_event( time(), $this->recurrence, $this->hook, $this->args );

    }

    /**
     * Clears schedule after changing theme
     */
    public function removeSchedule() {

        wp_clear_scheduled_hook( $this->hook, $this->args );

    }

}