<?php


namespace Wpk\p965772\Models;

use Wpk\p965772\Core;
use Wpk\p965772\Settings;
use Wpk\p965772\Utility;

use function Wpk\p965772\Core as CoreFc;

/**
 * @author Przemysław Żydek
 */
class Invoice extends Post {

    /** @var array Array of months in french */
    const FRENCH_MONTHS = [
        1  => "janvier",
        2  => "février",
        3  => "mars",
        4  => "avril",
        5  => "mai",
        6  => "juin",
        7  => "juillet",
        8  => "août",
        9  => "septembre",
        10 => "octobre",
        11 => "novembre",
        12 => "décembre",
    ];

    /** @var string */
    const DATE_FORMAT = 'd F y';

    /** @var string Date format that is used for saving in meta */
    const META_DATE_FORMAT = 'Y-m-d';

    /** @var string Date format used for invoice ID */
    const ID_DATE_FORMAT = 'Ymd';

    /**
     * @return Collection
     */
    public function get() {
        $this->attributes[ 'post_type' ] = Core::INVOICE_SLUG;

        return parent::get();
    }

    /**
     * @return bool|static
     */
    public function create() {
        $this->attributes[ 'post_type' ] = Core::INVOICE_SLUG;

        return parent::create();
    }

    /**
     * @param string $ref
     *
     * @return $this
     */
    public function ref( $ref ) {

        return $this->addMeta( 'ref', $ref );

    }

    /**
     * @param string $ref
     *
     * @return $this
     */
    public function hasRef( $ref ) {

        return $this->hasMetaValue( 'ref', $ref );

    }

    /**
     * @return bool
     */
    public function wasSent() {
        return $this->meta( 'email_status' ) === 'sent';
    }

    /**
     * @return bool
     */
    public function wasPaid() {
        return $this->meta( 'payment_status' ) === 'paid';
    }

    /**
     * @return bool
     */
    public function isConfirmed() {
        return $this->meta( 'quote_status' ) === 'confirmed';
    }

    /**
     * @return bool
     */
    public function isStandby() {
        return $this->meta( 'quote_status' ) === 'standby';
    }

    /**
     * Translates month from french to english
     *
     * @param string $date
     *
     * @return mixed
     */
    public static function translateMonths( $date ) {

        $months = [
            'janvier'   => "January",    //January
            'février'   => "February", //February
            'mars'      => "March",      //March
            'avril'     => "April", //April
            'mai'       => "May", //May
            'juin'      => "June",         //Jun
            'juillet'   => "July",      //July
            'août'      => "August", //August
            'septembre' => "September",       //September
            'octobre'   => "October",    //October
            'novembre'  => "November", //November
            'décembre'  => "December" //December
        ];

        foreach ( $months as $frenchMonth => $month ) {
            $date = str_replace( $frenchMonth, $month, $date );
        }

        return $date;

    }

    /**
     * Format price to french format
     *
     * @param int|float $price
     *
     * @return string
     */
    protected static function formatPrice( $price = 0 ) {

        setlocale( LC_MONETARY, 'fr_FR' );

        $price = number_format( $price, 2, '.', '\'' );
        $price = trim( $price );

        $price = str_replace( 'EUR', '', $price );
        $price = "$price.-CHF";

        return $price;

    }

    /**
     * @param bool $format Whenever format price or return it in float format
     *
     * @return array Array with total, vat and regular price
     */
    public function prices( $format = false ) {

        $total           = (float) $this->meta( 'total' );
        $vat             = (float) $this->meta( 'vat' );
        $calc            = ( $vat / 100 );
        $priceCalculated = (float) $total / ( 1 + $calc );

        $vat = $total - $priceCalculated;

        $result = [
            'total' => $total,
            'vat'   => round( $vat, 2 ),
            'price' => round( $priceCalculated, 2 ),
        ];

        if ( $format ) {
            array_walk( $result, function ( &$item ) {
                $item = self::formatPrice( $item );
            } );
        }

        return $result;


    }

    /**
     * @return string
     */
    public function pdfLink() {

        return add_query_arg(
            [
                'action'  => 'wpk_download_invoice',
                'invoice' => $this->ID,
            ],
            admin_url()
        );

    }

    /**
     * @return false|string
     */
    public function eventDate() {
        return self::formatDate( $this->meta( 'event_date' ) );
    }

    /**
     * @return false|string
     */
    public function date() {
        return self::formatDate( $this->post_modified );
    }

    /**
     * @return bool
     */
    public function proffessionalOfEvents() {

        $proffessional = strtolower( $this->meta( 'event_proffessional' ) );

        $options = [ 'yes', 'oui' ];

        return in_array( $proffessional, $options );

    }

    /**
     * Format text with invoice variables
     *
     * @param string $text
     *
     * @return string
     */
    public function formatText( $text ) {

        $ref       = $this->meta( 'ref' );
        $invoiceID = $this->meta( 'invoice_id' ) ? $this->meta( 'invoice_id' ) : '';
        $eventDate = $this->eventDate();

        $mail = $this->meta( 'email' );

        $company = $this->meta( 'company' );
        $name    = $this->meta( 'name' );

        $invoiceDate = $this->date();

        $text = str_replace( '{ref}', $ref, $text );
        $text = str_replace( '{invoice_id}', $invoiceID, $text );
        $text = str_replace( '{invoice_date}', $invoiceDate, $text );
        $text = str_replace( '{event_date}', $eventDate, $text );
        $text = str_replace( '{customer_email}', $mail, $text );
        $text = str_replace( '{customer_name}', $name, $text );
        $text = str_replace( '{company_name}', $company, $text );

        return $text;

    }

    /**
     * @return Collection
     */
    public function schedules() {
        return Schedule::init()->parent( $this->ID )->all()->get();
    }

    /**
     * @param string $type Schedule type
     *
     * @return $this
     */
    public function deleteSchedule( $type ) {

        /** @var Schedule $schedule */
        foreach ( $this->schedules()->all() as $schedule ) {
            if ( $schedule->scheduleType() === $type ) {
                $schedule->delete();
            }
        }

        return $this;

    }

    /**
     * @param string $date
     *
     * @return false|string
     */
    protected static function formatDate( $date ) {

        return date( self::DATE_FORMAT, strtotime( $date ) );

    }

    /**
     * Get invoice ID (not post ID)
     *
     * @return int
     */
    public function invoiceID() {
        return $this->meta( 'invoice_id' );
    }

    /**
     * Generate unique invoice ID based on event date
     *
     * @return $this
     */
    public function generateID() {

        $date = date( self::ID_DATE_FORMAT, strtotime( $this->eventDate() ) );

        //This option stores dates with their counters, so that they increment on each invoice
        $counters = get_option( 'wpk_invoice_references', [] );
        $counter  = isset( $counters[ $date ] ) ? (int) $counters[ $date ] : 1;

        $ID = $counter < 10 ? "0$counter" : $counter;
        $ID = $date . $ID;

        if ( strtolower( Settings::getSetting( 'site_name' ) ) === 'easyflash' ) {
            $ID = "F$ID";
        }

        $ID = apply_filters( 'wpk/p965772/invoiceID', $ID, $this );

        $this->updateMeta( 'invoice_id', $ID );

        $counter ++;
        $counters[ $date ] = $counter;

        //Update counters
        update_option( 'wpk_invoice_references', $counters );

        return $this;

    }

    /**
     * Format meta for display
     *
     * @param string $meta
     *
     * @return string
     */
    public static function formatMeta( $meta ) {
        return ucfirst( str_replace( '_', ' ', $meta ) );
    }

    /**
     * Get invoice language
     *
     * @return string
     */
    public function getLang() {

        $lang = $this->meta( 'lang' );

        return empty( $lang ) ? 'en' : $lang;

    }

}