<?php

namespace Wpk\p965772\Models;

use Wpk\p965772\Models\Traits;
use Wpk\p965772\Utility;

/**
 * Model class for WordPress users
 *
 * @author Przemysław Żydek
 */
class User extends Entity {

    use Traits\HandleMetaFields;

    /** @var \WP_User */
    public $entity;

    protected $entityClass = \WP_User::class;

    protected $entityInstanceMethod = 'get_data_by';

    protected $getMetaFunction = 'get_user_meta';

    protected $updateMetaFunction = 'update_user_meta';

    protected $addMetaFunction = 'add_user_meta';

    protected $deleteMetaFunction = 'delete_user_meta';

    /**
     * User constructor.
     *
     * @param mixed $attributes
     */
    public function __construct( $attributes = null ) {

        if ( is_numeric( $attributes ) ) {
            $this->entity = get_userdata( $attributes );
        } else {
            parent::__construct( $attributes );
        }

    }

    /**
     * @param string $login
     *
     * @return $this
     */
    public function login( $login ) {

        $this->attributes[ 'login' ] = $login;

        return $this;

    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function email( $email ) {

        $this->attributes[ 'email' ] = $email;

        return $this;

    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function password( $password ) {

        $this->attributes[ 'password' ] = $password;

        return $this;

    }

    /**
     * @param string|array $cap
     *
     * @return $this
     */
    public function cap( $cap ) {

        if ( is_array( $cap ) ) {
            $this->attributes[ 'cap' ] = array_merge( $this->attributes[ 'cap' ], $cap );
        } else {

            $this->attributes[ 'cap' ][] = $cap;

        }

        return $this;

    }

    /**
     * @param string|array $role
     *
     * @return $this
     */
    public function role( $role ) {

        if ( is_array( $role ) ) {
            $this->attributes[ 'role' ] = array_merge( $this->attributes[ 'role' ], $role );
        } else {

            $this->attributes[ 'role' ][] = $role;

        }

        return $this;

    }

    /**
     * @return bool|static
     */
    public function create() {

        $attributes = $this->attributes;

        $user = wp_create_user( $attributes[ 'login' ], $attributes[ 'password' ], $attributes[ 'email' ] );

        if ( is_wp_error( $user ) ) {
            Utility::log( $user->get_error_message() );

            return false;
        }

        $user = new static( $user );

        foreach ( $this->attributes[ 'role' ] as $role ) {
            $user->entity->add_role( $role );
        }

        foreach ( $this->attributes[ 'cap' ] as $cap ) {
            $user->entity->add_cap( $cap );
        }

        foreach ( $this->attributes[ 'meta_input' ] as $key => $value ) {
            $user->addMeta( $key, $value );
        }


        return $user;

    }

    /**
     * Execute query
     *
     * @return Collection
     */
    public function get() {

        $result = [];

        $query = new \WP_User_Query( $this->attributes );

        foreach ( $query->get_results() as $post ) {
            $result[] = new static( $post );
        }

        return Collection::make( $result );

    }

    /**
     * @return static
     */
    public function update() {

        $this->attributes[ 'ID' ] = $this->ID;

        $result = wp_update_user( (object) $this->attributes );

        if ( is_wp_error( $result ) ) {
            Utility::log( $result->get_error_messages(), 'USER_UPDATE_ERROR' );

            return $this;
        }

        //Recreate instance with updated attributes
        return new static( $this->ID );

    }

    /**
     * @return bool
     */
    public function delete() {
        return (bool) wp_delete_user( $this->ID );
    }

    /**
     * @return self
     */
    public static function current() {
        return new static( get_current_user_id() );
    }

}