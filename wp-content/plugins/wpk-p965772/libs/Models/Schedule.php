<?php


namespace Wpk\p965772\Models;

use Wpk\p965772\Controllers\Schedules;
use Wpk\p965772\Core;

class Schedule extends Post {

    /** @var array */
    const TIMES = [
        'before_event' => '+3 days',
        'invoice'      => '+72 hours',
        'after_event'  => '+10 months',
    ];

    /** @var array */
    const CRON_HOOKS = [
        'before_event' => Schedules\BeforeEvent\Cron::HOOK,
        'invoice'      => Schedules\Invoice\Cron::HOOK,
        'after_event'  => Schedules\AfterEvent\Cron::HOOK,
    ];

    /**
     * @return Collection
     */
    public function get() {
        $this->attributes[ 'post_type' ] = Core::SCHEDULE_SLUG;

        return parent::get();
    }

    /**
     * @return bool|static
     */
    public function create() {
        $this->attributes[ 'post_type' ] = Core::SCHEDULE_SLUG;

        return parent::create();
    }

    /**
     * @return string
     */
    public function scheduleType() {
        return $this->meta( 'type' );
    }

    /**
     * @return int
     */
    public function time() {
        return (int) $this->meta( 'time' );
    }

    /**
     * @return $this
     */
    public function disable() {
        $this->updateMeta( 'status', 'disabled' );

        return $this;
    }

    /**
     * @return $this
     */
    public function enable() {
        $this->updateMeta( 'status', 'enabled' );

        return $this;
    }

    /**
     * @return bool
     */
    public function enabled() {
        return $this->meta( 'status' ) === 'enabled';
    }

    /**
     * Set schedule time. On this time the schedule will be executed
     *
     * @return $this
     */
    public function setTime() {

        $type = $this->scheduleType();

        if ( ! empty( $type ) && isset( self::TIMES[ $type ] ) ) {

            //Remove current schedule before setting time
            $this->removeSchedule();

            $time = self::TIMES[ $type ];

            //For "after_event" we are adding 10 months to event date
            if ( $type === 'after_event' ) {
                $timestamp = ( new \DateTime( $this->invoice()->meta( 'event_date' ) ) )
                    ->modify( $time )
                    ->getTimestamp();
            } else {
                $timestamp = strtotime( $time );
            }

            $this->updateMeta( 'time', $timestamp );

        }

        return $this;

    }

    /**
     * Create cron schedule
     *
     * @return $this
     */
    public function schedule() {

        //Can't enable schedule if it's post doesn't exists or time is not set
        if ( ! $this->hasEntity() || empty( $this->entity->ID ) || empty( $this->time() ) ) {
            return $this;
        }

        $hook = $this->getCronHook();

        if ( $hook ) {

            wp_schedule_single_event( $this->time(), $hook, [ $this->ID ] );

            //Enable the schedule
            $this->enable();

        }

        return $this;

    }

    /**
     * @return bool|string
     */
    protected function getCronHook() {

        $type = $this->scheduleType();

        return isset( self::CRON_HOOKS[ $type ] ) ? self::CRON_HOOKS[ $type ] : false;
    }

    /**
     * @return $this
     */
    public function removeSchedule() {
        wp_unschedule_event( $this->time(), $this->getCronHook(), [ $this->ID ] );

        return $this;
    }

    /**
     * @return Invoice
     */
    public function invoice() {
        return new Invoice( $this->post_parent );
    }

    /**
     * @param Invoice $invoice
     * @param string $type Event type
     *
     * @return self
     */
    public static function createForInvoice( Invoice $invoice, $type ) {

        return self::init()
                   ->addMeta( 'type', $type )
                   ->parent( $invoice->ID )
                   ->status( 'publish' )
                   ->title( sprintf( 'Schedule for %s', $invoice->post_title ) )
                   ->create()
                   ->setTime()
                   ->schedule();

    }

}