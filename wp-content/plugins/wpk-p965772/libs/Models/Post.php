<?php

namespace Wpk\p965772\Models;


use Wpk\p965772\Models\Traits;
use Wpk\p965772\Utility;

/**
 * Model class for WordPress posts
 *
 * @author Przemysław Żydek
 */
class Post extends Entity {

    use Traits\Post\HandleTaxonomy,
        Traits\HandleMetaFields;

    /** @var \WP_Post */
    public $entity;

    protected $entityClass = \WP_Post::class;

    protected $entityInstanceMethod = 'get_instance';

    protected $getMetaFunction = 'get_post_meta';

    protected $updateMetaFunction = 'update_post_meta';

    protected $addMetaFunction = 'add_post_meta';

    protected $deleteMetaFunction = 'delete_post_meta';


    /**
     * @return string
     */
    public function __toString() {
        return $this->hasEntity() ? $this->post_content : '';
    }

    /**
     * Rename certain attributes so that they will fit in @see wp_insert_post()
     *
     * @return $this
     */
    protected function parseCreationAttributes() {

        if ( ! empty( $this->attributes[ 'author' ] ) ) {
            $this->attributes[ 'post_author' ] = $this->attributes[ 'author' ];
        }

        return $this;

    }

    /**
     * @return bool|static
     */
    public function create() {

        if ( empty( $this->attributes ) ) {
            return false;
        }

        $taxInput = [];

        if ( ! empty( $this->attributes[ 'tax_input' ] ) ) {
            $taxInput = $this->attributes[ 'tax_input' ];
            unset( $this->attributes[ 'tax_input' ] );
        }

        $this->parseCreationAttributes();

        do_action( 'wpk/p965772/post/beforeCreation', $this );

        $post = wp_insert_post( $this->attributes, true );

        /*
         * Manually add new taxonomies.
         * The reason for that is "tax_input" in wp_insert_post must be passed as terms id, and sometimes we want to use slug or name instead :>
         * */
        if ( ! is_wp_error( $post ) ) {

            $post = new static( $post );

            if ( ! empty( $taxInput ) ) {
                foreach ( $taxInput as $taxonomy => $terms ) {
                    $post->setTerms( $terms, $taxonomy );
                }
            }

        }

        return $post;

    }

    /**
     * Add post status to query
     *
     * @param string|array $status
     *
     * @return $this
     */
    public function status( $status ) {

        $this->attributes[ 'post_status' ] = $status;

        return $this;

    }

    /**
     * @param string|array $type
     *
     * @return $this
     */
    public function type( $type ) {

        $this->attributes[ 'post_type' ] = $type;

        return $this;

    }

    /**
     *
     * @param string $title
     *
     * @return $this
     */
    public function title( $title ) {

        $this->attributes[ 'post_title' ] = $title;

        return $this;

    }

    /**
     * @param int $parentID
     *
     * @return $this
     */
    public function parent( $parentID ) {

        $this->attributes[ 'post_parent' ] = $parentID;

        return $this;

    }

    /**
     * @param string $fields
     *
     * @return $this
     */
    public function fields( $fields ) {

        $this->attributes[ 'fields' ] = $fields;

        return $this;

    }

    /**
     * @param int $amount
     *
     * @return $this
     */
    public function perPage( $amount ) {

        $this->attributes[ 'posts_per_page' ] = $amount;

        return $this;

    }

    /**
     * @return $this
     */
    public function all() {

        $this->perPage( - 1 );

        return $this;

    }

    /**
     * @param int $page
     *
     * @return $this
     */
    public function paged( $page ) {

        $this->attributes[ 'paged' ] = $page;

        return $this;

    }

    /**
     * Set order of posts. Must be called before @see Entity::get()
     *
     * @param array|string $by
     * @param string $how
     * @param string|null $metaKey Optional, if order should be set by meta key
     *
     * @return $this
     */
    public function order( $by, $how = 'ASC', $metaKey = null ) {

        $this->attributes[ 'order' ]   = $how;
        $this->attributes[ 'orderby' ] = $by;

        if ( ! empty( $metaKey ) ) {
            $this->attributes[ 'meta_key' ] = $metaKey;
        }

        return $this;

    }

    /**
     * @param int $authorID
     *
     * @return $this
     */
    public function author( $authorID ) {

        $this->attributes[ 'author' ] = $authorID;

        return $this;

    }

    /**
     * Execute query
     *
     * @return Collection
     */
    public function get() {

        $result = [];

        $query = new \WP_Query( $this->attributes );

        foreach ( $query->get_posts() as $post ) {
            $result[] = new static( $post );
        }

        return Collection::make( $result );

    }

    /**
     * @return static
     */
    public function update() {

        $this->parseCreationAttributes();

        $this->attributes[ 'ID' ] = $this->ID;

        $result = wp_update_post( $this->attributes );

        if ( is_wp_error( $result ) ) {
            Utility::log( $result->get_error_messages(), 'POST_UPDATE_ERROR' );

            return $this;
        }

        //Recreate instance with updated attributes
        return new static( $this->ID );

    }

    /**
     * @return bool
     */
    public function delete() {

        return (bool) wp_delete_post( $this->ID, true );

    }

}