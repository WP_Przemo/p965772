<?php

/**
 * View for invoice table actions
 *
 * @var \Wpk\p965772\Models\Invoice $invoice
 */

?>

<div class="wpk-table-actions">
    <div class="wpk-action">
        <button data-action="payment_status" title="{{ __('Click to change payment status.', 'wpk') }}" class="button button-primary" data-id="{{ $invoice->ID }}">{{ $paymentStatus }}</button>
    </div>
    <div class="wpk-action">
        <button data-action="payment_type" title="{{ __('Click to change payment type.', 'wpk') }}" class="button button-primary" data-id="{{ $invoice->ID }}">{{ $paymentType }}</button>
    </div>
    <div class="wpk-action">
        <button data-action="payment_reminder" title="{{ __('Click to send payment reminder.', 'wpk') }}" class="button button-primary" data-id="{{ $invoice->ID }}">{{ $paymentReminder }}</button>
    </div>
    <div class="wpk-action">
        <a target="_blank" class="button button-primary" href="{{  $invoice->pdfLink() }}">{{ __('Download PDF', 'wpk') }}</a>
    </div>
    <input type="hidden" name="wpk_nonce" class="wpk-nonce" value="{{ wp_create_nonce('wpk_table') }}">
</div>