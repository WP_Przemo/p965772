<?php
/**
 * View for invoice statuses
 *
 * @var \Wpk\p965772\Models\Invoice $invoice
 */
?>


<div class="wpk-invoice-statuses">
    @foreach($statuses as $key => $values)
        <div class="wpk-status">
            <div class="wpk-label">
                <strong>{{ $values['label'] }}</strong>
            </div>
            <div class="wpk-status">{{ $values['value'] }}</div>
        </div>
    @endforeach
</div>