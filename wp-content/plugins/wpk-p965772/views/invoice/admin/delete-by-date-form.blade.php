<div class="wpk-form-wrap">
    <form action="#" class="wpk-form" id="wpk_delete_by_date_form">
        <div class="wpk-form-section">
            <label for="date">{{ __( 'Enter date', 'wpk' )  }}</label>
            <input type="text" name="date" id="date" class="wpk-date-picker" data-format="D MMM YYYY">
        </div>
        <div class="wpk-form-section wpk-submit-section">
            <input type="hidden" name="wpk_nonce" id="wpk_nonce" value="{{ wp_create_nonce('wpk_delete_by_date') }}">
            <button class="button button-primary">{{ __('Submit', 'wpk') }}</button>
        </div>
    </form>
</div>