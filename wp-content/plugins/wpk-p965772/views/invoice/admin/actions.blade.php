<?php
/**
 * View for invoice actions
 */
?>
<div class="wpk-invoice-actions">

    <div class="wpk-action">
        <a target="_blank" href="{{ $pdfUrl }}">Download PDF</a>
    </div>

    <div class="wpk-action">
        <button class="button-primary button wpk-send-invoice" data-id="{{ $id }}">Send invoice</button>
        <input type="hidden" name="wpk_invoice" id="wpk_invoice" value="{{ wp_create_nonce('wpk_send_invoice') }}">
    </div>

</div>