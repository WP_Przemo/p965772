<?php
/**
 * View for invoice PDF
 */
?>

<div style="height: 75%">
    <table style="width: 100%; height: 200px">
        <tr>
            <td style="width: 65%; vertical-align: top">
                <div>
                    <div style="text-align:left; margin-right: 20px">
                        <img src="{{ $headerLogo }}" alt="EasyFlare">
                    </div>
                    <div style="text-align: left;">
                        {!! $headerContent !!}
                    </div>
                </div>
            </td>
            <td style="width: 35%; vertical-align: top;">
                <div style="display: inline-block; width: auto; float: right;">
                    <div>
                        {!! $invoiceHeader !!}
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div style="position: relative; left:3px; font-weight: bold; list-style: none; margin-bottom: 80px; padding: 0; margin-top: 20px;">
        @if(!empty($invoiceID))

            <div style="position: relative; right: 5px; margin-bottom: 10px">
                Facture N° :
                <span style="font-weight: 200;">{{ $invoiceID }}</span>
            </div>

        @endif
        <div style="margin-bottom: 10px;">
            Date de la facture:
            <span style="font-weight: 200; text-decoration: none">
                {{ $invoiceDate }}
            </span>
        </div>
        <div style="text-decoration: none; margin-bottom: 10px;">Numéro de TVA:
            <span style="font-weight: 200">{{ $vatNumber }}</span></div>
        <div style="margin-bottom: 50px">
            Date de la prestation:
            <span style="font-weight: 200; text-decoration: none">{{ $eventDate }}</span>
        </div>
        <div>
            <div style="text-decoration: underline; margin-bottom: 20px">DETAILS PRESTATION:</div>
            @foreach ( $details as $detail )
                <div style="font-weight: 100; font-style: italic; text-decoration: none !important;">{{ $detail }}</div>
            @endforeach
        </div>
    </div>
    <div style="text-align: right; margin-top: 90px;">
        <div style="margin-bottom: 10px">
            <span><b>Montant HT:</b></span>
            <span>{{ $prices['price'] }}</span>
        </div>
        <div style="margin-bottom: 10px">
            <span><b> TVA {{ $vat }}%: </b></span>
            <span>{{ $prices['vat'] }}</span>
        </div>
        <div style="margin-bottom: 10px">
            <span><b>Montant TTC:</b></span>
            <span>{{ $prices['total'] }}</span>
        </div>
    </div>
</div>
<footer>
    {!! $footer !!}
</footer>