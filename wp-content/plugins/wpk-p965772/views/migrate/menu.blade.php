<section class="wpk-migrate-wrap wpk-wrap">

    <h3> {{ __('The purpose of this menu is to migrate all old invoices and schedules into new format. This is one-time operation') }}</h3>

    <div class="wpk-progress-bar">
        <div class="wpk-bar"></div>
        <div class="wpk-progress"></div>

        <div class="wpk-bar-message">
        </div>
    </div>

    <div class="wpk-button-container">
        <button class="wpk-migrate button button-prmiary button-large"> {{ __('Start migration', 'wpk') }} </button>
        <input type="hidden" name="wpk_nonce" id="wpk_nonce" value="{{ wp_create_nonce('wpk_migrate') }}">
    </div>

</section>