<div class="wpk-form-wrap">
    <form action="#" class="wpk-form" id="wpk_password_form" data-ref="{{ $ref }}">

        @if($auth)
            <div class="wpk-form-section">
                <label for="wpk_password">{{ __( 'Enter new password', 'wpk' )  }}</label>
                <input type="password" name="wpk_new_password" id="wpk_new_password">
            </div>
        @else
            <div class="wpk-form-section">
                <label for="wpk_password">{{ __( 'Enter password to proceed', 'wpk' )  }}</label>
                <input type="password" name="wpk_password" id="wpk_password">
            </div>
        @endif
        <div class="wpk-form-section wpk-submit-section">
            <input type="hidden" name="wpk_nonce" id="wpk_nonce" value="{{ wp_create_nonce('wpk_security_nonce') }}">
            <button class="button button-primary">{{ __('Submit', 'wpk') }}</button>
        </div>
    </form>
</div>