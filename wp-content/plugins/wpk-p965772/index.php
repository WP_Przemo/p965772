<?php
/*
Plugin Name: WP Kraken [#p965772]
Plugin URI: https://wpkraken.io/job/p965772
Description: Rewriting plugin #p607489 #w586400
Author: WP Kraken
Author URI: https://wpkraken.io/
Version: 1.0
Text Domain: wpk-p965772
*/

namespace Wpk\p965772;

//Require necessary files
require_once 'includes/wpk_functions.php';
require_once 'libs/Loader.php';
require_once 'libs/Core.php';
require_once 'vendor/autoload.php';

//ACF Plugin
if ( ! function_exists( 'get_field' ) ) {
	require_once 'vendor/advanced-custom-fields-pro/acf.php';
}

/**
 * Helper function for accessing Core.
 *
 * @return Core
 *
 * @author Przemysław Żydek
 */
function Core() {

	return Core::getInstance();

}

$core = Core();

/* Release the Kraken! */
$core->init( __FILE__ );
