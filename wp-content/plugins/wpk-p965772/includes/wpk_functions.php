<?php
if ( ! function_exists( 'dump' ) ):
	function dump( $var ) {

		$style = is_admin() ? '' : 'style="display:none"';
		echo '<pre class="wpk-dump" ' . $style . '>';
		var_dump( $var );
		echo '</pre>';

	}
endif;

if ( ! function_exists( 'dump_hidden' ) ):
	function dump_hidden( $var ) {

		echo '<pre style="display: none" class="wpk-dump">';
		var_dump( $var );
		echo '</pre>';

	}
endif;

