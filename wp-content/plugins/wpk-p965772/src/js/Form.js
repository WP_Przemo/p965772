import Utils from './Utils';

let $ = jQuery.noConflict();

/**
 * Handles form related operations
 *
 * @author Przemysław Żydek
 * */
class Form {

    constructor( $form ) {

        this.$form = $form;
        this.formID = $form.data( 'form' );

        this.validateReference();

    }

    /**
     * @return {FormData}
     * */
    getValues() {

        let $inputs     = this.$form.find( 'input, textarea' ).not( ':hidden' ),
            result      = new FormData(),
            formatTitle = title => {

                title = title
                    .toLowerCase()
                    .trim()
                    //Remove slashes
                    .replace( / \/ /, '' )
                    //Replace spaces with underscore
                    .replace( / /g, '_' )
                    //Remove brackets
                    .replace( / *\([^)]*\) */g, '' );

                return title;
            };

        $inputs.each( function() {

            let $this = $( this ),
                title = $this.data( 'title' );

            title = formatTitle( title );

            result.append( title, $this.val() );

        } );

        return result;

    }

    /**
     * Validate values from confirmation form
     *
     * @return {Form}
     *
     * */
    validateReference() {

        const Instance = this;

        let confirmationForms = Utils.vars.forms.confirmation;

        if ( confirmationForms ) {

            confirmationForms = confirmationForms.split( ',' );

            //Check if we are on confirmation form
            if ( confirmationForms.find( item => parseInt( item ) === this.formID ) ) {

                /**
                 * This is hidden required input, it's value will be set once we validate the form.
                 * So, user can't proceed further unless he have provided valid referece and e-mail
                 * */
                let $input    = this.$form.find( 'input[data-title="wpk"]' ),
                    validated = false;

                $( document ).on( 'click', '.btn-next', function() {

                    let $this = $( this );

                    if ( !$( '.has-error:visible' ).length && !validated ) {

                        let values = Instance.getValues();

                        values.append( 'action', 'wpk_validate_confirmation_form' );

                        Utils.ajax( {
                            data: values,
                            beforeSend() {
                                //Always reset value
                                $input.val( '' );
                            },
                            success( data ) {

                                Utils.handleResponse( data );

                                if ( data.result ) {

                                    validated = true;
                                    $input.val( 'validated' );
                                    $this.click();

                                }

                            }
                        } );

                    }

                } );

            }

        }

        return this;

    }

}

export default Form;