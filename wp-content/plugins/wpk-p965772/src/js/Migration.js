import Utils from './Utils';

const $ = jQuery.noConflict();

/**
 * @author Przemysław Żydek
 * */
class Migration {

    constructor( $container ) {

        this.$container = $container;
        this.$progress = $container.find( '.wpk-progress' );
        this.$message = $container.find( '.wpk-bar-message' );

        this.handleClick();
    }

    /**
     * @return Migration
     * */
    handleClick() {

        let $button = $( '.wpk-migrate' ),
            nonce   = $button.next().val();

        $button.on( 'click', () => {

            $button.attr( 'disabled', true );
            this.process( { nonce } );

        } );

        return this;

    }

    /**
     * @param {String} nonce Security nonce
     * @param {Number} migratedInvoices Amount of total invoices. We resend it after first request to increase performance
     * @param {String} operation Current migrate operation
     * @param {Boolean} finished Determines if we have finished migration
     *
     * @return Migration
     * */
    process( { nonce = '', migratedInvoices = 0, operation = 'migrate_invoices', finished = false } ) {

        if ( !finished ) {

            const Fd = new FormData();

            Fd.append( 'action', 'wpk_migrate' );
            Fd.append( 'nonce', nonce );
            Fd.append( 'migrated_invoices', migratedInvoices );
            Fd.append( 'operation', operation );

            Utils.ajax( {
                data:    Fd,
                success: data => this.handleResponse( data )
            } );

        }

        return this;

    }

    /**
     * Handle migration response
     *
     * @param {Object} data
     *
     * @return void
     *
     * */
    handleResponse( data ) {

        if ( data ) {

            let result = data.result;

            this.$progress.css( 'width', result.progress );
            this.$message.text( result.message );

            if ( !result.finished ) {
                this.process( result );
            } else {
                window.alert( 'Migration finished!' );
            }

        }

    }


}

export default Migration;