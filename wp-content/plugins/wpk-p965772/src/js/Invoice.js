import Utils from './Utils';
import Form from './Form';

let $ = jQuery.noConflict();

/**
 * Handles invoice related actions
 *
 * @author Przemysław Żydek
 * */
class Invoice {

    constructor() {
        this.send().actions().calculateTotals().deleteByDate();
    }

    /**
     * Send invoice via ajax
     *
     * @return {Invoice}
     * */
    send() {

        $( '.wpk-send-invoice' ).on( 'click', function() {

            let fd        = new FormData(),
                $this     = $( this ),
                invoiceID = $this.data( 'id' ),
                nonce     = $this.next().val();

            fd.append( 'invoice_id', invoiceID );
            fd.append( 'action', 'wpk_send_invoice' );
            fd.append( 'wpk_nonce', nonce );

            Utils.ajax( {
                data: fd,
                beforeSend() {
                    $this.attr( 'disabled', true );
                },
                success( data ) {
                    Utils.handleResponse( data );
                },
                complete() {
                    $this.attr( 'disabled', false );
                }
            } );

        } );

        return this;

    }

    /**
     * Handles invoice actions
     *
     * @return {Invoice}
     * */
    actions() {

        let $button = $( '.wpk-table-actions .wpk-action button' );

        $button.on( 'click', function( e ) {

            e.preventDefault();

            let $this  = $( this ),
                $table = $this.parents( '.wpk-table-actions' ),
                nonce  = $table.find( '.wpk-nonce' ).val(),
                action = $this.data( 'action' ),
                ID     = $this.data( 'id' ),
                fd     = new FormData();

            fd.append( 'invoice_action', action );
            fd.append( 'action', 'wpk_invoice_action' );
            fd.append( 'invoice_id', ID );
            fd.append( 'wpk_nonce', nonce );

            Utils.ajax( {
                data: fd,
                beforeSend() {
                    $this.attr( 'disabled', true );
                },
                success( data ) {

                    Utils.handleResponse( data );

                    if ( data && data.result ) {
                        $this.text( data.result );
                    }

                },
                complete() {
                    $this.attr( 'disabled', false );
                }
            } );

        } );

        return this;
    }

    /**
     * @return {Invoice}
     * */
    deleteByDate() {

        let $form = $( '#wpk_delete_by_date_form' );

        $form.on( 'submit', function( e ) {

            e.preventDefault();

            let fd       = new FormData( this ),
                $targets = $( this ).find( 'button, input' );

            fd.append( 'action', 'wpk_delete_by_date' );

            Utils.ajax( {
                data:    fd,
                beforeSend() {
                    $targets.attr( 'disabled', true );
                },
                success: Utils.handleResponse,
                complete() {
                    $targets.attr( 'disabled', false );
                }
            } );

        } );

        return this;

    }

    /**
     * Calculate total prices and display them below invoices table
     *
     * @return {Invoice}
     * */
    calculateTotals() {

        let gatherPrices = ( $el ) => {

            let total = 0;

            $el.each( function() {
                let text = $( this ).text();

                total += parseFloat( text.replace( /'/, '' ).replace( /.-CH/g, '' ) );
            } );


            return total.toLocaleString( 'fr-FR' ).replace( /\s/g, '\'' ).replace( /,/g, '.' ).concat( '.-CHF' );

        };

        if ( $( '.post-type-wpk_invoice' ).length ) {

            let net        = gatherPrices( $( '.net_price' ) ),
                vat        = gatherPrices( $( '.vat_price' ) ),
                gross      = gatherPrices( $( '.gross_price' ) ),
                $container = $( '<div class="wpk-total-prices"></div>' );

            $container.append( `<div class="wpk-price wpk-net"><span class="wpk-label">${Utils.getMessage( 'net_price' )}: </span><span class="wpk-price">${net}</span></div>` );
            $container.append( `<div class="wpk-price wpk-vat"><span class="wpk-label">${Utils.getMessage( 'vat' )}: </span><span class="wpk-price">${vat}</span></div>` );
            $container.append( `<div class="wpk-price wpk-net"><span class="wpk-label">${Utils.getMessage( 'gross_price' )}: </span><span class="wpk-price">${gross}</span></div>` );

            $( '#posts-filter' ).after( $container );


        }

        return this;

    }

}

export default Invoice;