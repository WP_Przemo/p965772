import Pikaday from 'pikaday';

let $ = jQuery.noConflict();

$( '.wpk-date-picker' ).each( function() {

    let $this  = $( this ),
        format = $this.data( 'format' ) || 'D MMM YYYY',
        picker = new Pikaday( {
            field: $this.get( 0 ),
            toString( date, format ) {

                let day   = date.getDate(),
                    month = date.getMonth() + 1,
                    year  = date.getFullYear();

                month = month < 10 ? `0${month}` : month;
                day = day < 10 ? `0${day}` : day;

                return `${day}-${month}-${year}`;
            },
        } );

} );