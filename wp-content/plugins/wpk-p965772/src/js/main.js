import Utils from './Utils';
import Form from './Form';
import Invoice from './Invoice';
import Migration from './Migration';

import './password-form';
import './date-picker';

import '../scss/main.scss';

let $ = jQuery.noConflict();


$( document ).on( 'ready', () => {

    const $Form = $( '#estimation_popup' );

    if ( $Form.length ) {
        Utils.addInstance( 'form', new Form( $Form ) );
    }

    Utils.addInstance( 'invoice', new Invoice() );

    const $Migration = $( '.wpk-migrate-wrap' );

    if ( $Migration.length ) {
        Utils.addInstance( 'migration', new Migration( $Migration ) );
    }


} );