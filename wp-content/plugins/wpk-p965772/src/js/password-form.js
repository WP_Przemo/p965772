import Utils from './Utils';

let $ = jQuery.noConflict();

const $Form = $( '#wpk_password_form' );

$Form.on( 'submit', function( e ) {

    e.preventDefault();

    let fd       = new FormData( this ),
        $this    = $( this ),
        $targets = $this.find( 'button, input' ),
        ref      = $this.data( 'ref' );

    fd.append( 'action', 'wpk_check_password' );

    Utils.ajax( {
        data: fd,
        beforeSend() {
            $targets.attr( 'disabled', true );
        },
        success( data ) {

            Utils.handleResponse( data );

            if ( data.result ) {

                if ( ref !== '' && data.redirect_url !== '' ) {
                    window.location = ref;
                } else {
                    window.location.reload();
                }

            }

        },
        complete() {
            $targets.attr( 'disabled', false );
        }
    } );

} );