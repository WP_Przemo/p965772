<?php

namespace Wpk\p965772;

use Wpk\p965722\Tests\Helpers\Plugins\WpkP965772;

if ( function_exists( 'xdebug_disable' ) ) {
    xdebug_disable();
}

define( 'TESTS_NAMESPACE', 'Wpk\\p965722\\Tests' );
define( 'ROOT_DIR', 'C:/wamp64/www/jobs/p965772/' );
define( 'WP_PLUGIN_DIR', ROOT_DIR . 'wp-content/plugins/' );
define( 'CORE_PLUGIN_DIR', ROOT_DIR . 'wp-content/plugins/wpk-p965772/' );
define( 'WPK_IGNORE_SECURITY', true );


if ( false !== getenv( 'WP_DEVELOP_DIR' ) ) {
    require getenv( 'WP_DEVELOP_DIR' ) . 'tests/phpunit/includes/bootstrap.php';
}

spl_autoload_register( function ( $class ) {

    if ( strpos( $class, TESTS_NAMESPACE ) === false ) {
        return;
    }

    $class = str_replace( TESTS_NAMESPACE . '\\', '', $class );

    require_once "$class.php";

} );

WpkP965772::activate();

