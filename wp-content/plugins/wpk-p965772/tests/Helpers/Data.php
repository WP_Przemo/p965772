<?php

namespace Wpk\p965722\Tests\Helpers;

/**
 * @author Przemysław Żydek
 */
class Data {

    /**
     * @return array
     */
    public static function invoiceInEnglish() {
        return [
            'action'         => 'send_email',
            'formID'         => '19',
            'informations'   => '',
            'email'          => 'przemyslaw.z@mpcreation.net',
            'lastName'       => '',
            'firstName'      => 'Przemek',
            'phone'          => '12345678',
            'country'        => 'Switzerland',
            'zip'            => '',
            'state'          => '',
            'city'           => 'City',
            'address'        => 'Address',
            'summary'        => '<div id=\\"lfb_summaryCt\\" style=\\"padding-top: 24px;padding-bottom: 24px; text-align: center;\\">
                        
                        <table class=\\"table table-bordered\\" width=\\"90%\\" border=\\"1\\" style=\\"width:90%;margin:0 auto;;color:#ffffff;color:#ffffff\\" bordercolor=\\"#dddddd\\" cellspacing=\\"0\\" cellpadding=\\"8\\" bgcolor=\\"#FFFFFF\\">
                            <thead style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\">
                                <tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th bgcolor=\\"#51d8f0\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; background: rgb(81, 216, 240); width: 0%; min-width: 500px;;color:#000000;color:#000000\\" color=\\"#000000\\" align=\\"left\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Description</span></th>
                                <th class=\\"\\" style=\\"display: table-cell; font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; background: rgb(81, 216, 240); width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#51d8f0\\" color=\\"#000000\\" align=\\"left\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Information</span></th>
                                <th class=\\"\\" bgcolor=\\"#51d8f0\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; background: rgb(81, 216, 240); width: 0%;;color:#000000;color:#000000\\" color=\\"#000000\\" align=\\"right\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Quantity</span></th>
                                <th class=\\"\\" bgcolor=\\"#51d8f0\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; background: rgb(81, 216, 240); width: 0%;;color:#000000;color:#000000\\" color=\\"#000000\\" align=\\"right\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Price</span></th>
                            </tr></thead>
                            <tbody style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\">
                                <tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"175\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">RENTAL OF PHOTOBOOTH</strong></span></th></tr><tr data-itemstep=\\"175\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">EasyBox</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">380,00 .-CHF</span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"173\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">DETAILS</strong></span></th></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Company</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">WPK</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Event Professional</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">NO</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Name</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Przemek</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">City of Event</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">City</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Address</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Address</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Country</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Switzerland</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Phone Number</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">12345678</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Email</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">przemyslaw.z@mpcreation.net</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Date of Event</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">31 August 2018</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Booking on consecutive days</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">NO</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Start Time of Event</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">11h00</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">EndTime of Event</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">01h00</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Type of Event</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Bar Mitsvah</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"173\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Where did you hear about it?</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Social media</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"176\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">PHOTO DEVELOPMENT SERVICE</strong></span></th></tr><tr data-itemstep=\\"176\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">No photo development</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"177\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">PICTURES CUSTOMIZATION</strong></span></th></tr><tr data-itemstep=\\"177\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">NO</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"178\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">ACCESSORIES</strong></span></th></tr><tr data-itemstep=\\"178\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">No, thank you</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"179\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">WIFI SMARTPHONE</strong></span></th></tr><tr data-itemstep=\\"179\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">No, thank you</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"180\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">CUSTOMIZE YOUR EASYBOX</strong></span></th></tr><tr data-itemstep=\\"180\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">NO</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"181\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">BACKDROP</strong></span></th></tr><tr data-itemstep=\\"181\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">White backdrop</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">90,00 .-CHF</span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"184\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">TRANSPORT</strong></span></th></tr><tr data-itemstep=\\"184\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">EASYFLASH DELIVERY</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">50,00 .-CHF</span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"189\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">DISCOUNT CODE</strong></span></th></tr><tr data-itemstep=\\"189\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Discount :</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"187\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">VAT</strong></span></th></tr><tr data-itemstep=\\"187\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">VAT 7.7%</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">40,04 .-CHF</span></td></tr>
                                <tr id=\\"sfb_summaryTotalTr\\" class=\\"lfb_static \\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"3\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Total :</span></th><th id=\\"lfb_summaryTotal\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"><span style=\\"font-size: 16px; padding: 0px; text-align: right; line-height: 22.8571px;;color:#000000;color:#000000\\">560,04 .-CHF</span></span></th></tr>
                            </tbody>
                        </table>
                    </div>',
            'stripeToken'    => '',
            'stripeTokenB'   => '',
            'totalTxt'       => '560.04 .-CHF',
            'email_toUser'   => '0',
            'usePaypalIpn'   => '0',
            'discountCode'   => '',
            'formSession'    => '5b851530986e2',
            'total'          => '560.04',
            'totalSub'       => '0',
            'subFrequency'   => '',
            'formTitle'      => 'Easyflash, Quote Photobooth',
            'contactSent'    => '1',
            'contentTxt'     => '[n]<p><u><b>RENTAL OF PHOTOBOOTH :</b></u></p>[n]    - EasyBox : 380 .-CHF[n][n]<p><u><b>DETAILS :</b></u></p>[n]    - Company : <b>WPK</b>[n]    - Event Professional (NO) : 0 .-CHF[n]    - Name : <b>Przemek</b>[n]    - City of Event : <b>City</b>[n]    - Address : <b>Address</b>[n]    - Country : <b>Switzerland</b>[n]    - Phone Number : <b>12345678</b>[n]    - Email : <b>przemyslaw.z@mpcreation.net</b>[n]    - Date of Event : <b>31 August 2018</b>[n]    - Booking on consecutive days (NO) : 0 .-CHF[n]    - Start Time of Event (11h00) : 0 .-CHF[n]    - EndTime of Event (01h00) : 0 .-CHF[n]    - Type of Event (Bar Mitsvah) : 0 .-CHF[n]    - Where did you hear about it? (Social media) : 0 .-CHF[n][n]<p><u><b>PHOTO DEVELOPMENT SERVICE :</b></u></p>[n]    - No photo development : 0 .-CHF[n][n]<p><u><b>PICTURES CUSTOMIZATION :</b></u></p>[n]    - NO : 0 .-CHF[n][n]<p><u><b>ACCESSORIES :</b></u></p>[n]    - No, thank you : 0 .-CHF[n][n]<p><u><b>WIFI SMARTPHONE :</b></u></p>[n]    - No, thank you : 0 .-CHF[n][n]<p><u><b>CUSTOMIZE YOUR EASYBOX :</b></u></p>[n]    - NO : 0 .-CHF[n][n]<p><u><b>BACKDROP :</b></u></p>[n]    - White backdrop : 90 .-CHF[n][n]<p><u><b>STAFF :</b></u></p>[n][n]<p><u><b>TRANSPORT FEE :</b></u></p>[n][n]<p><u><b>TRANSPORT :</b></u></p>[n]    - EASYFLASH DELIVERY : 50 .-CHF[n][n]<p><u><b>DISCOUNT CODE :</b></u></p>[n]    - Discount : : 0- .-CHF[n][n]<p><u><b>VAT :</b></u></p>[n]    - VAT 7.7% : 40.04 .-CHF[n][n]<p><u><b>FINAL STEP :</b></u></p>[n]',
            'items'          =>
                [
                    0  =>
                        [
                            'label'         => 'EasyBox',
                            'itemid'        => '915',
                            'price'         => '380',
                            'quantity'      => '1',
                            'step'          => 'RENTAL OF PHOTOBOOTH',
                            'stepid'        => '175',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    1  =>
                        [
                            'label'         => 'Company',
                            'itemid'        => '901',
                            'value'         => 'WPK',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                        ],
                    2  =>
                        [
                            'label'         => 'Event Professional',
                            'itemid'        => '902',
                            'price'         => '0',
                            'value'         => 'NO',
                            'quantity'      => '1',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    3  =>
                        [
                            'label'         => 'Name',
                            'itemid'        => '903',
                            'value'         => 'Przemek',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                        ],
                    4  =>
                        [
                            'label'         => 'City of Event',
                            'itemid'        => '904',
                            'value'         => 'City',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                        ],
                    5  =>
                        [
                            'label'         => 'Address',
                            'itemid'        => '905',
                            'value'         => 'Address',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                        ],
                    6  =>
                        [
                            'label'         => 'Country',
                            'itemid'        => '906',
                            'value'         => 'Switzerland',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                        ],
                    7  =>
                        [
                            'label'         => 'Phone Number',
                            'itemid'        => '910',
                            'value'         => '12345678',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                        ],
                    8  =>
                        [
                            'label'         => 'Email',
                            'itemid'        => '911',
                            'value'         => 'przemyslaw.z@mpcreation.net',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                        ],
                    9  =>
                        [
                            'label'         => 'Date of Event',
                            'itemid'        => '900',
                            'value'         => '31 August 2018',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                        ],
                    10 =>
                        [
                            'label'         => 'Booking on consecutive days',
                            'itemid'        => '908',
                            'price'         => '0',
                            'value'         => 'NO',
                            'quantity'      => '1',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    11 =>
                        [
                            'label'         => 'Start Time of Event',
                            'itemid'        => '909',
                            'price'         => '0',
                            'value'         => '11h00',
                            'quantity'      => '1',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    12 =>
                        [
                            'label'         => 'EndTime of Event',
                            'itemid'        => '907',
                            'price'         => '0',
                            'value'         => '01h00',
                            'quantity'      => '1',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    13 =>
                        [
                            'label'         => 'Type of Event',
                            'itemid'        => '912',
                            'price'         => '0',
                            'value'         => 'Bar Mitsvah',
                            'quantity'      => '1',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    14 =>
                        [
                            'label'         => 'Where did you hear about it?',
                            'itemid'        => '913',
                            'price'         => '0',
                            'value'         => 'Social media',
                            'quantity'      => '1',
                            'step'          => 'DETAILS',
                            'stepid'        => '173',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    15 =>
                        [
                            'label'         => 'No photo development',
                            'itemid'        => '925',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'PHOTO DEVELOPMENT SERVICE',
                            'stepid'        => '176',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    16 =>
                        [
                            'label'         => 'NO',
                            'itemid'        => '927',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'PICTURES CUSTOMIZATION',
                            'stepid'        => '177',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    17 =>
                        [
                            'label'         => 'No, thank you',
                            'itemid'        => '932',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'ACCESSORIES',
                            'stepid'        => '178',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    18 =>
                        [
                            'label'         => 'No, thank you',
                            'itemid'        => '936',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'WIFI SMARTPHONE',
                            'stepid'        => '179',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    19 =>
                        [
                            'label'         => 'NO',
                            'itemid'        => '938',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'CUSTOMIZE YOUR EASYBOX',
                            'stepid'        => '180',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    20 =>
                        [
                            'label'         => 'White backdrop',
                            'itemid'        => '941',
                            'price'         => '90',
                            'quantity'      => '1',
                            'step'          => 'BACKDROP',
                            'stepid'        => '181',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    21 =>
                        [
                            'label'         => 'NO',
                            'itemid'        => '949',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'STAFF',
                            'stepid'        => '182',
                            'showInSummary' => 'false',
                            'isSinglePrice' => 'false',
                        ],
                    22 =>
                        [
                            'label'         => 'City',
                            'itemid'        => '951',
                            'value'         => 'geneva',
                            'step'          => 'TRANSPORT FEE',
                            'stepid'        => '183',
                            'showInSummary' => 'false',
                        ],
                    23 =>
                        [
                            'label'         => 'Country',
                            'itemid'        => '952',
                            'value'         => 'Switzerland',
                            'step'          => 'TRANSPORT FEE',
                            'stepid'        => '183',
                            'showInSummary' => 'false',
                        ],
                    24 =>
                        [
                            'label'         => 'EASYFLASH DELIVERY',
                            'itemid'        => '954',
                            'price'         => '1',
                            'quantity'      => '1',
                            'step'          => 'TRANSPORT',
                            'stepid'        => '184',
                            'showInSummary' => 'false',
                            'isSinglePrice' => 'false',
                        ],
                    25 =>
                        [
                            'label'         => 'EASYFLASH DELIVERY',
                            'itemid'        => '1111',
                            'price'         => '50',
                            'quantity'      => '1',
                            'step'          => 'TRANSPORT',
                            'stepid'        => '184',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    26 =>
                        [
                            'label'         => 'NO',
                            'itemid'        => '962',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'DISCOUNT CODE',
                            'stepid'        => '189',
                            'showInSummary' => 'false',
                            'isSinglePrice' => 'false',
                        ],
                    27 =>
                        [
                            'label'         => 'Discount :',
                            'itemid'        => '963',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'DISCOUNT CODE',
                            'stepid'        => '189',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    28 =>
                        [
                            'label'         => 'VAT 7.7%',
                            'itemid'        => '958',
                            'price'         => '40.04',
                            'quantity'      => '1',
                            'step'          => 'VAT',
                            'stepid'        => '187',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                ],
            'fieldsLast'     =>
                [
                    0 =>
                        [
                            'fieldID' => '',
                            'value'   => '',
                        ],
                ],
            'activatePaypal' => '1',
            'captcha'        => '',
            'useRtl'         => 'false',
            'finalUrl'       => 'http://localhost/jobs/p965772/en/thank-you-discount-easyflair/',
        ];
    }

    /**
     * @return array
     */
    public static function invoiceInFrench() {
        return [
            'action'         => 'send_email',
            'formID'         => '19',
            'informations'   => '',
            'email'          => 'przemyslaw.z@mpcreation.net',
            'lastName'       => '',
            'firstName'      => 'Nom',
            'phone'          => '25225',
            'country'        => 'Suisse',
            'zip'            => '',
            'state'          => '',
            'city'           => 'Genève',
            'address'        => 'Adresse',
            'summary'        => '<div id=\\"lfb_summaryCt\\" style=\\"padding-top: 24px;padding-bottom: 24px; text-align: center;\\">
                        
                        <table class=\\"table table-bordered\\" width=\\"90%\\" border=\\"1\\" style=\\"width:90%;margin:0 auto;;color:#ffffff;color:#ffffff\\" bordercolor=\\"#dddddd\\" cellspacing=\\"0\\" cellpadding=\\"8\\" bgcolor=\\"#FFFFFF\\">
                            <thead style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\">
                                <tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th bgcolor=\\"#51d8f0\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; background: rgb(81, 216, 240); width: 0%; min-width: 500px;;color:#000000;color:#000000\\" color=\\"#000000\\" align=\\"left\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Description</span></th>
                                <th class=\\"\\" style=\\"display: table-cell; font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; background: rgb(81, 216, 240); width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#51d8f0\\" color=\\"#000000\\" align=\\"left\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Information</span></th>
                                <th class=\\"\\" bgcolor=\\"#51d8f0\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; background: rgb(81, 216, 240); width: 0%;;color:#000000;color:#000000\\" color=\\"#000000\\" align=\\"right\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Quantité</span></th>
                                <th class=\\"\\" bgcolor=\\"#51d8f0\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; background: rgb(81, 216, 240); width: 0%;;color:#000000;color:#000000\\" color=\\"#000000\\" align=\\"right\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Prix</span></th>
                            </tr></thead>
                            <tbody style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\">
                                <tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"15\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">LOCATION PHOTOBOOTH</strong></span></th></tr><tr data-itemstep=\\"15\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">EasyBox</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">380,00 .-CHF</span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"14\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">INFORMATIONS</strong></span></th></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Société</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Societe</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Professionnel de l\\\'évènementiel</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">NON</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Nom</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Nom</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Ville (où se déroule l\\\'évènement)</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Ville</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Adresse</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Adresse</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Pays</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Suisse</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Téléphone</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">25225</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Email</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">przemyslaw.z@mpcreation.net</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Date</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">30 août 2018</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Réservation sur plusieurs jours consécutifs</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">NON</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Horaire de début l’événement</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">23h00</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Horaire de fin l\\\'évenement</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">23h00</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Type d\\\'événement</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Mariage</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr data-itemstep=\\"14\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Comment avez-vous entendu parler de nous?</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Réseaux sociaux</span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"16\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">DEVELOPPEMENT PHOTOS</strong></span></th></tr><tr data-itemstep=\\"16\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">200 photos</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">169,00 .-CHF</span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"225\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">PERSONNALISATION PHOTOS</strong></span></th></tr><tr data-itemstep=\\"225\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Oui, je fourni mon visuel à Easyflash  (Insertion offerte)</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"227\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">ACCESSOIRES</strong></span></th></tr><tr data-itemstep=\\"227\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Non merci</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"226\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">WIFI SMARTPHONE</strong></span></th></tr><tr data-itemstep=\\"226\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Non, merci</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"20\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">PERSONNALISEZ VOTRE EASYBOX</strong></span></th></tr><tr data-itemstep=\\"20\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">NON</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"21\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">TOILE DE FOND</strong></span></th></tr><tr data-itemstep=\\"21\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Non, merci. J\\\'utiliserai mon propre fond, un mur ou un rideau de 2m/2m</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"22\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">PERSONNEL</strong></span></th></tr><tr data-itemstep=\\"22\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">NON</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"24\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">DEPLACEMENT</strong></span></th></tr><tr data-itemstep=\\"24\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Livraison Easyflash</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">50,00 .-CHF</span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"131\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">CODE DE REDUCTION</strong></span></th></tr><tr data-itemstep=\\"131\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Montant de la réduction :</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td></tr><tr style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"4\\" class=\\"sfb_summaryStep\\" data-step=\\"38\\" align=\\"center\\" style=\\"font-size: 22px; padding: 8px; text-align: center; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" bgcolor=\\"#bdc3c7\\" color=\\"#000000\\" width=\\"332\\"><span style=\\"font-size: 22px;;color:#000000;color:#000000\\"><strong style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 22.8571px;;color:#000000;color:#000000\\">TVA</strong></span></th></tr><tr data-itemstep=\\"38\\" class=\\"\\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><td color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">TVA 7.7%</span></td><td class=\\"lfb_valueTd \\" color=\\"#000000\\" align=\\"left\\" style=\\"font-size: 16px; padding: 8px; text-align: left; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"></span></td><td class=\\"\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">46,12 .-CHF</span></td></tr>
                                <tr id=\\"sfb_summaryTotalTr\\" class=\\"lfb_static \\" style=\\"font-size: 16px; padding: 0px; text-align: center; line-height: 24px;;color:#ffffff;color:#ffffff\\"><th colspan=\\"3\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"332\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\">Total :</span></th><th id=\\"lfb_summaryTotal\\" color=\\"#000000\\" align=\\"right\\" style=\\"font-size: 16px; padding: 8px; text-align: right; line-height: 22.8571px; width: 0%;;color:#000000;color:#000000\\" width=\\"103\\"><span style=\\"font-size: 16px;;color:#000000;color:#000000\\"><span style=\\"font-size: 16px; padding: 0px; text-align: right; line-height: 22.8571px;;color:#000000;color:#000000\\">645,12 .-CHF</span></span></th></tr>
                            </tbody>
                        </table>
                    </div>',
            'stripeToken'    => '',
            'stripeTokenB'   => '',
            'totalTxt'       => '645.12 .-CHF',
            'email_toUser'   => '1',
            'usePaypalIpn'   => '0',
            'discountCode'   => '',
            'formSession'    => '5b84fea592261',
            'total'          => '645.12',
            'totalSub'       => '0',
            'subFrequency'   => '',
            'formTitle'      => 'Easyflash, Devis Photobooth',
            'contactSent'    => '1',
            'contentTxt'     => '[n]<p><u><b>LOCATION PHOTOBOOTH :</b></u></p>[n]    - EasyBox : 380 .-CHF[n][n]<p><u><b>INFORMATIONS :</b></u></p>[n]    - Société : <b>Societe</b>[n]    - Professionnel de l\\\'évènementiel (NON) : 0 .-CHF[n]    - Nom : <b>Nom</b>[n]    - Ville (où se déroule l\\\'évènement) : <b>Ville</b>[n]    - Adresse : <b>Adresse</b>[n]    - Pays : <b>Suisse</b>[n]    - Téléphone : <b>25225</b>[n]    - Email : <b>przemyslaw.z@mpcreation.net</b>[n]    - Date : <b>30 août 2018</b>[n]    - Réservation sur plusieurs jours consécutifs (NON) : 0 .-CHF[n]    - Horaire de début l’événement (23h00) : 0 .-CHF[n]    - Horaire de fin l\\\'évenement (23h00) : 0 .-CHF[n]    - Type d\\\'événement (Mariage) : 0 .-CHF[n]    - Comment avez-vous entendu parler de nous? (Réseaux sociaux) : 0 .-CHF[n][n]<p><u><b>DEVELOPPEMENT PHOTOS :</b></u></p>[n]    - 200 photos : 169 .-CHF[n][n]<p><u><b>PERSONNALISATION PHOTOS :</b></u></p>[n]    - Oui, je fourni mon visuel à Easyflash  (Insertion offerte) : 0 .-CHF[n][n]<p><u><b>ACCESSOIRES :</b></u></p>[n]    - Non merci : 0 .-CHF[n][n]<p><u><b>WIFI SMARTPHONE :</b></u></p>[n]    - Non, merci : 0 .-CHF[n][n]<p><u><b>PERSONNALISEZ VOTRE EASYBOX :</b></u></p>[n]    - NON : 0 .-CHF[n][n]<p><u><b>TOILE DE FOND :</b></u></p>[n]    - Non, merci. J\\\'utiliserai mon propre fond, un mur ou un rideau de 2m/2m : 0 .-CHF[n][n]<p><u><b>PERSONNEL :</b></u></p>[n]    - NON : 0 .-CHF[n][n]<p><u><b>FRAIS DE DEPLACEMENT :</b></u></p>[n][n]<p><u><b>DEPLACEMENT :</b></u></p>[n]    - Livraison Easyflash : 50 .-CHF[n][n]<p><u><b>CODE DE REDUCTION :</b></u></p>[n]    - Montant de la réduction : : 0- .-CHF[n][n]<p><u><b>TVA :</b></u></p>[n]    - TVA 7.7% : 46.123 .-CHF[n][n]<p><u><b>FINALISATION :</b></u></p>[n]',
            'items'          =>
                [
                    0  =>
                        [
                            'label'         => 'EasyBox',
                            'itemid'        => '42',
                            'price'         => '380',
                            'quantity'      => '1',
                            'step'          => 'LOCATION PHOTOBOOTH',
                            'stepid'        => '15',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    1  =>
                        [
                            'label'         => 'Société',
                            'itemid'        => '33',
                            'value'         => 'Societe',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                        ],
                    2  =>
                        [
                            'label'         => 'Professionnel de l\\\'évènementiel',
                            'itemid'        => '34',
                            'price'         => '0',
                            'value'         => 'NON',
                            'quantity'      => '1',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    3  =>
                        [
                            'label'         => 'Nom',
                            'itemid'        => '35',
                            'value'         => 'Nom',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                        ],
                    4  =>
                        [
                            'label'         => 'Ville (où se déroule l\\\'évènement)',
                            'itemid'        => '36',
                            'value'         => 'Ville',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                        ],
                    5  =>
                        [
                            'label'         => 'Adresse',
                            'itemid'        => '37',
                            'value'         => 'Adresse',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                        ],
                    6  =>
                        [
                            'label'         => 'Pays',
                            'itemid'        => '38',
                            'value'         => 'Suisse',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                        ],
                    7  =>
                        [
                            'label'         => 'Téléphone',
                            'itemid'        => '41',
                            'value'         => '25225',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                        ],
                    8  =>
                        [
                            'label'         => 'Email',
                            'itemid'        => '134',
                            'value'         => 'przemyslaw.z@mpcreation.net',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                        ],
                    9  =>
                        [
                            'label'         => 'Date',
                            'itemid'        => '32',
                            'value'         => '30 août 2018',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                        ],
                    10 =>
                        [
                            'label'         => 'Réservation sur plusieurs jours consécutifs',
                            'itemid'        => '406',
                            'price'         => '0',
                            'value'         => 'NON',
                            'quantity'      => '1',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    11 =>
                        [
                            'label'         => 'Horaire de début l’événement',
                            'itemid'        => '135',
                            'price'         => '0',
                            'value'         => '23h00',
                            'quantity'      => '1',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    12 =>
                        [
                            'label'         => 'Horaire de fin l\\\'évenement',
                            'itemid'        => '39',
                            'price'         => '0',
                            'value'         => '23h00',
                            'quantity'      => '1',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    13 =>
                        [
                            'label'         => 'Type d\\\'événement',
                            'itemid'        => '136',
                            'price'         => '0',
                            'value'         => 'Mariage',
                            'quantity'      => '1',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    14 =>
                        [
                            'label'         => 'Comment avez-vous entendu parler de nous?',
                            'itemid'        => '401',
                            'price'         => '0',
                            'value'         => 'Réseaux sociaux',
                            'quantity'      => '1',
                            'step'          => 'INFORMATIONS',
                            'stepid'        => '14',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    15 =>
                        [
                            'label'         => '200 photos',
                            'itemid'        => '107',
                            'price'         => '169',
                            'quantity'      => '1',
                            'step'          => 'DEVELOPPEMENT PHOTOS',
                            'stepid'        => '16',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    16 =>
                        [
                            'label'         => 'Oui, je fourni mon visuel à Easyflash  (Insertion offerte)',
                            'itemid'        => '1150',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'PERSONNALISATION PHOTOS',
                            'stepid'        => '225',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    17 =>
                        [
                            'label'         => 'Non merci',
                            'itemid'        => '1160',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'ACCESSOIRES',
                            'stepid'        => '227',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    18 =>
                        [
                            'label'         => 'Non, merci',
                            'itemid'        => '1155',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'WIFI SMARTPHONE',
                            'stepid'        => '226',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    19 =>
                        [
                            'label'         => 'NON',
                            'itemid'        => '55',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'PERSONNALISEZ VOTRE EASYBOX',
                            'stepid'        => '20',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    20 =>
                        [
                            'label'         => 'Non, merci. J\\\'utiliserai mon propre fond, un mur ou un rideau de 2m/2m',
                            'itemid'        => '124',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'TOILE DE FOND',
                            'stepid'        => '21',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    21 =>
                        [
                            'label'         => 'NON',
                            'itemid'        => '403',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'PERSONNEL',
                            'stepid'        => '22',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    22 =>
                        [
                            'label'         => 'Ville',
                            'itemid'        => '61',
                            'value'         => 'geneve',
                            'step'          => 'FRAIS DE DEPLACEMENT',
                            'stepid'        => '23',
                            'showInSummary' => 'false',
                        ],
                    23 =>
                        [
                            'label'         => 'Pays',
                            'itemid'        => '144',
                            'value'         => 'suisse',
                            'step'          => 'FRAIS DE DEPLACEMENT',
                            'stepid'        => '23',
                            'showInSummary' => 'false',
                        ],
                    24 =>
                        [
                            'label'         => 'LIVRAISON EASYFLASH',
                            'itemid'        => '1213',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'DEPLACEMENT',
                            'stepid'        => '24',
                            'showInSummary' => 'false',
                            'isSinglePrice' => 'false',
                        ],
                    25 =>
                        [
                            'label'         => 'Livraison Easyflash',
                            'itemid'        => '1221',
                            'price'         => '50',
                            'quantity'      => '1',
                            'step'          => 'DEPLACEMENT',
                            'stepid'        => '24',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    26 =>
                        [
                            'label'         => 'Non',
                            'itemid'        => '714',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'CODE DE REDUCTION',
                            'stepid'        => '131',
                            'showInSummary' => 'false',
                            'isSinglePrice' => 'false',
                        ],
                    27 =>
                        [
                            'label'         => 'Montant de la réduction :',
                            'itemid'        => '715',
                            'price'         => '0',
                            'quantity'      => '1',
                            'step'          => 'CODE DE REDUCTION',
                            'stepid'        => '131',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                    28 =>
                        [
                            'label'         => 'TVA 7.7%',
                            'itemid'        => '125',
                            'price'         => '46.123',
                            'quantity'      => '1',
                            'step'          => 'TVA',
                            'stepid'        => '38',
                            'showInSummary' => 'true',
                            'isSinglePrice' => 'false',
                        ],
                ],
            'fieldsLast'     =>
                [
                    0 =>
                        [
                            'fieldID' => '',
                            'value'   => '',
                        ],
                ],
            'activatePaypal' => '1',
            'captcha'        => '',
            'useRtl'         => 'false',
            'finalUrl'       => 'http://localhost/jobs/p965772/totem-photo-photobooth-photomaton/',
        ];
    }

}