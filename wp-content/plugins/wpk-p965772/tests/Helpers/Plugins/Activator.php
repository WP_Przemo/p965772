<?php

namespace Wpk\p965722\Tests\Helpers\Plugins;

/**
 * Helper class for activating dependency plugins
 *
 * @author Przemysław Żydek
 */
abstract class Activator {

    /** @var string Path to plugin (relative to PLUGINS_DIR constant path) */
    protected static $path = '';

    /**
     * Activates provided plugin
     *
     * @return void
     */
    final public static function activate() {

        $result = activate_plugin( static::$path );

        if ( is_wp_error( $result ) ) {
            var_dump( $result );
        }

    }

    /**
     * Deactivates provided plugin
     *
     * @return void
     */
    final public static function deactivate() {
        deactivate_plugins( static::$path );
    }

}