<?php

namespace Wpk\p965722\Tests\Invoice;

use Wpk\p965722\Tests\Helpers\Data;
use Wpk\p965722\Tests\Helpers\Plugins\WPCostEstimation;
use Wpk\p965722\Tests\TestCase;
use Wpk\p965772\Controllers\Invoice\Creation;
use Wpk\p965772\Models\Invoice;
use Wpk\p965772\Models\Schedule;

final class CreationTest extends TestCase {

    public static function wpSetUpBeforeClass( \WP_UnitTest_Factory $factory ) {
        WPCostEstimation::activate();
        WPCostEstimation::createForms();

        do_action( 'init' );
    }

    public static function wpTearDownAfterClass() {
        WPCostEstimation::deactivate();
    }

    public function setUp() {
        self::login();

        parent::setUp();
    }

    /**
     * @covers Creation::createInvoice()
     *
     * @return Invoice
     */
    public function testIsBeingCreatedProperly() {

        $_POST  = Data::invoiceInEnglish();
        $formID = $_POST[ 'formID' ];

        try {
            $this->_handleAjax( 'send_email' );
        } catch ( \WPAjaxDieContinueException $e ) {

        }

        $ref = $this->getRef( $formID );
        $this->assertNotEmpty( $ref );

        $model = Invoice::init()->hasMetaValue( 'ref', $ref )->get();
        $this->assertFalse( $model->empty() );

        /** @var Invoice $invoice */
        $invoice = $model->first();

        $this->assertInstanceOf( Invoice::class, $invoice );
        $this->assertEquals( (float) $_POST[ 'total' ], $invoice->prices()[ 'total' ] );

        return $invoice;

    }

    /**
     * @depends testIsBeingCreatedProperly
     *
     * @param Invoice $invoice
     */
    public function testIsBeforeEventScheduleCreated( Invoice $invoice ) {

        do_action( 'wpk/p965772/newInvoice', $invoice );

        $schedules = $invoice->schedules();

        $this->assertFalse( $schedules->empty() );
        $this->assertEquals( 1, $schedules->count() );

        /** @var Schedule $schedule */
        $schedule = $schedules->first();
        $this->assertInstanceOf( Schedule::class, $schedule );

        $timestamp = $schedule->time();
        $this->assertEquals( strtotime( Schedule::TIMES[ 'before_event' ] ), $timestamp );

        $this->assertEquals( $schedule->post_parent, $invoice->ID );

    }

    /**
     * @param int $formID
     *
     * @return string|false
     */
    private function getRef( $formID ) {

        global $wpdb;

        $table = $wpdb->prefix . 'wpefc_forms';
        $rows  = $wpdb->get_results( "SELECT * FROM $table WHERE id=$formID LIMIT 1" );

        if ( empty( $rows ) ) {
            return false;
        }

        $form = $rows[ 0 ];
        $ref  = $form->current_ref;

        return $form->ref_root . $ref;

    }

}