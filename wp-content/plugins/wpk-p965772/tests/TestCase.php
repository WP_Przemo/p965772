<?php

namespace Wpk\p965722\Tests;

use Wpk\p965722\Tests\Helpers\Data;
use Wpk\p965772\Controllers\Security\Password;
use Wpk\p965772\Models\Invoice;

abstract class TestCase extends \WP_Ajax_UnitTestCase {

    /**
     * @return array|null
     */
    protected function getLastResponse() {
        return json_decode( $this->_last_response, true );
    }

    /**
     * @param bool $passSecurity Whenever bypass our security
     *
     * @see Password
     *
     * @return void
     */
    protected static function login( $passSecurity = true ) {

        $userID = self::factory()->user->create( [
            'role' => 'administrator',
        ] );

        wp_set_current_user( $userID );

        if ( $passSecurity ) {

            $token = 'test';

            $_COOKIE[ 'wpk_token' ] = $token;
            update_user_meta( $userID, 'wpk_token', $token );

        }

    }

    /**
     * @param array $data Optional WP Form Estimation $_POST data
     *
     * @return bool|Invoice
     */
    protected static function createInvoice( $data = [] ) {

        $_POST = wp_parse_args( $data, Data::invoiceInEnglish() );

        do_action( 'wpk/p965772/handleEstimationForm' );

        return Invoice::init()->get()->first();

    }

}
